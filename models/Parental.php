<?php

namespace app\models;

use Yii;

class Parental extends \yii\db\ActiveRecord
{

  const UPDATE_TYPE_CREATE = 'create';
  const UPDATE_TYPE_UPDATE = 'update';
  const UPDATE_TYPE_DELETE = 'delete';

  const SCENARIO_BATCH_UPDATE = 'batchUpdate';

  private $_updateType;

  public function getUpdateType()
  {
      if (empty($this->_updateType)) {
          if ($this->isNewRecord) {
              $this->_updateType = self::UPDATE_TYPE_CREATE;
          } else {
              $this->_updateType = self::UPDATE_TYPE_UPDATE;
          }
      }

      return $this->_updateType;
  }

  public function setUpdateType($value)
  {
      $this->_updateType = $value;
  }

    public static function tableName()
    {
        return 'parental';
    }

    public function rules()
    {
        return [
          ['updateType', 'required', 'on' => self::SCENARIO_BATCH_UPDATE],
          ['updateType',
              'in',
              'range' => [self::UPDATE_TYPE_CREATE, self::UPDATE_TYPE_UPDATE, self::UPDATE_TYPE_DELETE],
              'on' => self::SCENARIO_BATCH_UPDATE]
          ,
          ['patient_id', 'required', 'except' => self::SCENARIO_BATCH_UPDATE],

            [['patient_id'], 'integer'],
            [['desease_case', 'parental_relation'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'No. Paciente',
            'desease_case' => 'Enfermedad',
            'parental_relation' => 'Parentesco',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
