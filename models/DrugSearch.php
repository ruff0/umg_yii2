<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Drug;

class DrugSearch extends Drug
{
    public function rules()
    {
        return [
            [['id', 'cbarras', 'linea', 'activo'], 'integer'],
            [['nombre', 'descripcion', 'tipo', 'anotaciones'], 'safe'],
            [['precio', 'costo'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Drug::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cbarras' => $this->cbarras,
            'linea' => $this->linea,
            'precio' => $this->precio,
            'costo' => $this->costo,
            'activo' => $this->activo,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'anotaciones', $this->anotaciones]);

        return $dataProvider;
    }
}
