<?php

namespace app\models;

use Yii;

class Allergic extends \yii\db\ActiveRecord
{
  const UPDATE_TYPE_ALLERGIC_CREATE = 'create';
  const UPDATE_TYPE_ALLERGIC_UPDATE = 'update';
  const UPDATE_TYPE_ALLERGIC_DELETE = 'delete';
  const SCENARIO_ALLERGIC_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypeAllergic;
    public function getUpdateTypeAllergic()
    {
        if (empty($this->_updateTypeAllergic)) {
            if ($this->isNewRecord) {
                $this->_updateTypeAllergic = self::UPDATE_TYPE_ALLERGIC_CREATE;
            } else {
                $this->_updateTypeAllergic = self::UPDATE_TYPE_ALLERGIC_UPDATE;
            }
        }

        return $this->_updateTypeAllergic;
    }

    public function setUpdateTypeAllergic($value)
    {
        $this->_updateTypeAllergic = $value;
    }

    public static function tableName()
    {
        return 'allergic';
    }

    public function rules()
    {
        return [
          ['updateTypeAllergic', 'required', 'on' => self::SCENARIO_ALLERGIC_BATCH_UPDATE],
          ['updateTypeAllergic',
              'in',
              'range' => [self::UPDATE_TYPE_ALLERGIC_CREATE, self::UPDATE_TYPE_ALLERGIC_UPDATE, self::UPDATE_TYPE_ALLERGIC_DELETE],
              'on' => self::SCENARIO_ALLERGIC_BATCH_UPDATE],

          ['patient_id', 'required', 'except' => self::SCENARIO_ALLERGIC_BATCH_UPDATE],
            [['patient_id', 'allergy'], 'required'],
            [['patient_id'], 'integer'],
            [['allergy'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'allergy' => 'Allergy',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
