<?php

namespace app\models;

use Yii;

class Drug extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'drug';
    }

    public function rules()
    {
        return [
            [['cbarras', 'linea', 'activo'], 'integer'],
            [['nombre', 'descripcion'], 'required'],
            [['precio', 'costo'], 'number'],
            [['nombre', 'descripcion', 'tipo', 'anotaciones'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cbarras' => 'Código',
            'nombre' => 'Medicamento',
            'descripcion' => 'Descripcion',
            'tipo' => 'Tipo',
            'linea' => 'Línea',
            'precio' => 'Precio',
            'costo' => 'Costo',
            'anotaciones' => 'Anotaciones',
            'activo' => 'Activo',
        ];
    }
}
