<?php
namespace app\models;
use Yii;

class Calendar extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'consultation';
    }

    public function rules()
    {
        return [
            [['patient_name', 'date_time_in'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_name' => 'Paciente',
            'date_time_in' => 'Hora',
        ];
    }
}
