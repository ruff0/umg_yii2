<?php
namespace app\models;

use Yii;

class GynecoObstetric extends \yii\db\ActiveRecord
{
const UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE = 'create';
const UPDATE_TYPE_GYNECO_OBSTETRIC_UPDATE = 'update';
const UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE = 'delete';
const SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE = 'batchUpdate';
private $_updateTypeGynecoObstetric;

    public function getUpdateTypeGynecoObstetric()
    {
        if (empty($this->_updateTypeGynecoObstetric)) {
            if ($this->isNewRecord) {
                $this->_updateTypeGynecoObstetric = self::UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE;
            } else {
                $this->_updateTypeGynecoObstetric = self::UPDATE_TYPE_GYNECO_OBSTETRIC_UPDATE;
            }
        }

        return $this->_updateTypeGynecoObstetric;
    }

    public function setUpdateTypeGynecoObstetric($value)
    {
        $this->_updateTypeGynecoObstetric = $value;
    }

    public static function tableName()
    {
        return 'gyneco_obstetric';
    }

    public function rules()
    {
        return [
      ['updateTypeGynecoObstetric', 'required', 'on' => self::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE],
      ['updateTypeGynecoObstetric',
          'in',
          'range' => [self::UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE, self::UPDATE_TYPE_GYNECO_OBSTETRIC_UPDATE, self::UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE],
          'on' => self::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE],

            [['patient_id'], 'required'],
            [['patient_id', 'menstruation_age', 'ivsa_age', 'pregnancy', 'caesarian', 'child_bearing', 'abortion', 'menstruation_pain'], 'integer'],
            [['last_menstruation', 'last_pap'], 'safe'],
            [['menstruation_frequency', 'contraceptive'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'patient_id' => 'No. Paciente',
          'menstruation_age' => 'Edad de Menstruacón',
          'menstruation_frequency' => 'Frecuencia de Menstruación',
          'contraceptive' => 'Métodos Anticonceptivos',
          'last_menstruation' => 'Última Menstruación',
          'ivsa_age' => 'Edad IVSA',
          'last_pap' => 'Último PAP',
          'pregnancy' => 'Embarazo(s)',
          'caesarian' => 'Cesarea(s)',
          'child_bearing' => 'Nacimiento(s)',
          'abortion' => 'Aborto(s)',
          'menstruation_pain' => 'Dolor Menstrual',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
