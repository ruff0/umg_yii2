<?php
namespace app\models;

use Yii;

class Hospitalization extends \yii\db\ActiveRecord
{
const UPDATE_TYPE_HOSPITALIZATION_CREATE = 'create';
const UPDATE_TYPE_HOSPITALIZATION_UPDATE = 'update';
const UPDATE_TYPE_HOSPITALIZATION_DELETE = 'delete';
const SCENARIO_HOSPITALIZATION_BATCH_UPDATE = 'batchUpdate';
private $_updateTypeHospitalization;

    public function getUpdateTypeHospitalization()
    {
        if (empty($this->_updateTypeHospitalization)) {
            if ($this->isNewRecord) {
                $this->_updateTypeHospitalization = self::UPDATE_TYPE_HOSPITALIZATION_CREATE;
            } else {
                $this->_updateTypeHospitalization = self::UPDATE_TYPE_HOSPITALIZATION_UPDATE;
            }
        }

        return $this->_updateTypeHospitalization;
    }

    public function setUpdateTypeHospitalization($value)
    {
        $this->_updateTypeHospitalization = $value;
    }

    public static function tableName()
    {
        return 'hospitalization';
    }

    public function rules()
    {
        return [
      ['updateTypeHospitalization', 'required', 'on' => self::SCENARIO_HOSPITALIZATION_BATCH_UPDATE],
      ['updateTypeHospitalization',
          'in',
          'range' => [self::UPDATE_TYPE_HOSPITALIZATION_CREATE, self::UPDATE_TYPE_HOSPITALIZATION_UPDATE, self::UPDATE_TYPE_HOSPITALIZATION_DELETE],
          'on' => self::SCENARIO_HOSPITALIZATION_BATCH_UPDATE],

            [['patient_id'], 'required'],
            [['patient_id', 'at_age'], 'integer'],
            [['cause'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'No. Paciente',
            'cause' => 'Causa',
            'at_age' => 'Edad',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
