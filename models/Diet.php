<?php

namespace app\models;

use Yii;

class Diet extends \yii\db\ActiveRecord
{
  const UPDATE_TYPE_DIET_CREATE = 'create';
  const UPDATE_TYPE_DIET_UPDATE = 'update';
  const UPDATE_TYPE_DIET_DELETE = 'delete';

  const SCENARIO_DIET_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypeDiet;
      public function getUpdateTypeDiet()
      {
          if (empty($this->_updateTypeDiet)) {
              if ($this->isNewRecord) {
                  $this->_updateTypeDiet = self::UPDATE_TYPE_DIET_CREATE;
              } else {
                  $this->_updateTypeDiet = self::UPDATE_TYPE_DIET_UPDATE;
              }
          }

          return $this->_updateTypeDiet;
      }

      public function setUpdateTypeDiet($value)
      {
          $this->_updateTypeDiet = $value;
      }

      public static function tableName()
      {
        return 'diet';
      }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          ['updateTypeDiet', 'required', 'on' => self::SCENARIO_DIET_BATCH_UPDATE],
          ['updateTypeDiet',
              'in',
              'range' => [self::UPDATE_TYPE_DIET_CREATE, self::UPDATE_TYPE_DIET_UPDATE, self::UPDATE_TYPE_DIET_DELETE],
              'on' => self::SCENARIO_DIET_BATCH_UPDATE],

          ['patient_id', 'required', 'except' => self::SCENARIO_DIET_BATCH_UPDATE],

          [['patient_id'], 'required'],
          [['patient_id', 'breakfast', 'lunch', 'dinner', 'snacks'], 'integer'],
          [['b_details', 'l_details', 'd_details', 's_details'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
          'patient_id' => 'Patient ID',
          'breakfast' => 'Breakfast',
          'lunch' => 'Lunch',
          'dinner' => 'Dinner',
          'snacks' => 'Snacks',
          'b_details' => 'B Details',
          'l_details' => 'L Details',
          'd_details' => 'D Details',
          's_details' => 'S Details',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
