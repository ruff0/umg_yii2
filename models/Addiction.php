<?php

namespace app\models;

use Yii;

class Addiction extends \yii\db\ActiveRecord
{
  const UPDATE_TYPE_ADDICTION_CREATE = 'create';
  const UPDATE_TYPE_ADDICTION_UPDATE = 'update';
  const UPDATE_TYPE_ADDICTION_DELETE = 'delete';
  const SCENARIO_ADDICTION_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypeAddiction;
      public function getUpdateTypeAddiction()
      {
          if (empty($this->_updateTypeAddiction)) {
              if ($this->isNewRecord) {
                  $this->_updateTypeAddiction = self::UPDATE_TYPE_ADDICTION_CREATE;
              } else {
                  $this->_updateTypeAddiction = self::UPDATE_TYPE_ADDICTION_UPDATE;
              }
          }

          return $this->_updateTypeAddiction;
      }

      public function setUpdateTypeAddiction($value)
      {
          $this->_updateTypeAddiction = $value;
      }

    public static function tableName()
    {
        return 'addiction';
    }

    public function rules()
    {
      return [
        ['updateTypeAddiction', 'required', 'on' => self::SCENARIO_ADDICTION_BATCH_UPDATE],
        ['updateTypeAddiction',
            'in',
            'range' => [self::UPDATE_TYPE_ADDICTION_CREATE, self::UPDATE_TYPE_ADDICTION_UPDATE, self::UPDATE_TYPE_ADDICTION_DELETE],
            'on' => self::SCENARIO_ADDICTION_BATCH_UPDATE],

        ['patient_id', 'required', 'except' => self::SCENARIO_ADDICTION_BATCH_UPDATE],

            [['patient_id'], 'required'],
            [['patient_id', 'alcohol', 'alcohol_age', 'alcohol_climax', 'alcohol_yet', 'cigar', 'cigar_age', 'cigar_yet', 'coffee', 'coffee_age', 'coffee_yet', 'painkiller', 'drug', 'drug_age'], 'integer'],
            [['alcohol_detail', 'alcohol_quantity', 'alcohol_frequency', 'alcohol_reason', 'cigar_quantity', 'cigar_frequency', 'cigar_reason', 'coffee_quantity', 'coffee_frequency', 'painkiller_detail', 'drug_detail', 'drug_reason'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'alcohol' => 'Alcohol',
            'alcohol_detail' => 'Alcohol Detail',
            'alcohol_age' => 'Alcohol Age',
            'alcohol_quantity' => 'Alcohol Quantity',
            'alcohol_frequency' => 'Alcohol Frequency',
            'alcohol_climax' => 'Alcohol Climax',
            'alcohol_yet' => 'Alcohol Yet',
            'alcohol_reason' => 'Alcohol Reason',
            'cigar' => 'Cigar',
            'cigar_age' => 'Cigar Age',
            'cigar_quantity' => 'Cigar Quantity',
            'cigar_frequency' => 'Cigar Frequency',
            'cigar_yet' => 'Cigar Yet',
            'cigar_reason' => 'Cigar Reason',
            'coffee' => 'Coffee',
            'coffee_age' => 'Coffee Age',
            'coffee_quantity' => 'Coffee Quantity',
            'coffee_frequency' => 'Coffee Frequency',
            'coffee_yet' => 'Coffee Yet',
            'painkiller' => 'Painkiller',
            'painkiller_detail' => 'Painkiller Detail',
            'drug' => 'Drug',
            'drug_age' => 'Drug Age',
            'drug_detail' => 'Drug Detail',
            'drug_reason' => 'Drug Reason',
        ];
    }

        public function getPatient()
        {
            return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
        }
}
