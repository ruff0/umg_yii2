<?php

namespace app\models;

use Yii;

class Consultation extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'consultation';
    }

    public function rules()
    {
        return [
            [['patient_name', 'doctor_name', 'date_time_in', 'date_time_out'], 'required'],
            [['date_time_in', 'date_time_out'], 'safe'],
            [['patient_name', 'doctor_name', 'place'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'No. Paciente',
            'patient_name' => 'Paciente',
            'doctor_name' => 'Médico',
            'place' => 'Lugar',
            'date_time_in' => 'Hora de Inicio',
            'date_time_out' => 'Hora de Termino',
        ];
    }
}
