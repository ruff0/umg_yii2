<?php

namespace app\models;

use Yii;

class System extends \yii\db\ActiveRecord
{
  const UPDATE_TYPE_SYSTEM_CREATE = 'create';
  const UPDATE_TYPE_SYSTEM_UPDATE = 'update';
  const UPDATE_TYPE_SYSTEM_DELETE = 'delete';
  const SCENARIO_SYSTEM_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypeSystem;
      public function getUpdateTypeSystem()
      {
          if (empty($this->_updateTypeSystem)) {
              if ($this->isNewRecord) {
                  $this->_updateTypeSystem = self::UPDATE_TYPE_SYSTEM_CREATE;
              } else {
                  $this->_updateTypeSystem = self::UPDATE_TYPE_SYSTEM_UPDATE;
              }
          }

          return $this->_updateTypeSystem;
      }

      public function setUpdateTypeSystem($value)
      {
          $this->_updateTypeSystem = $value;
      }

    public static function tableName()
    {
        return 'system';
    }

    public function rules()
    {
        return [
          ['updateTypeSystem', 'required', 'on' => self::SCENARIO_SYSTEM_BATCH_UPDATE],
          ['updateTypeSystem',
              'in',
              'range' => [self::UPDATE_TYPE_SYSTEM_CREATE, self::UPDATE_TYPE_SYSTEM_UPDATE, self::UPDATE_TYPE_SYSTEM_DELETE],
              'on' => self::SCENARIO_SYSTEM_BATCH_UPDATE],

          ['patient_id', 'required', 'except' => self::SCENARIO_SYSTEM_BATCH_UPDATE],

            [['patient_id'], 'required'],
            [['patient_id', 'double_vision', 'blurred_vision', 'colored_vision', 'noise_exposure', 'ear_pain', 'tinnitus', 'difficulty_hearing', 'ear_infection', 'hearing_aid', 'nosebleed', 'dry_nose', 'cant_smell', 'frequent_flu', 'allergy', 'persistent_cough', 'phlegm', 'breathless', 'chest_pain', 'chest_nuisance', 'high_pressure', 'heartblow', 'varicose', 'swelling', 'pain_urinating', 'urine_count', 'urine_blood', 'urine_infection', 'excesive_bleeding', 'bruise', 'anemia', 'acne', 'itch', 'feet_ulceration', 'mole_change', 'hair_loss', 'skin_color_change', 'nail_problems', 'gout', 'sciatica', 'arthritis', 'lumbar_desease', 'pain_paralysis', 'flatfoot'], 'integer'],
            [['view_problem_frequency'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'double_vision' => 'Double Vision',
            'blurred_vision' => 'Blurred Vision',
            'colored_vision' => 'Colored Vision',
            'view_problem_frequency' => 'View Problem Frequency',
            'noise_exposure' => 'Noise Exposure',
            'ear_pain' => 'Ear Pain',
            'tinnitus' => 'Tinnitus',
            'difficulty_hearing' => 'Difficulty Hearing',
            'ear_infection' => 'Ear Infection',
            'hearing_aid' => 'Hearing Aid',
            'nosebleed' => 'Nosebleed',
            'dry_nose' => 'Dry Nose',
            'cant_smell' => 'Cant Smell',
            'frequent_flu' => 'Frequent Flu',
            'allergy' => 'Allergy',
            'persistent_cough' => 'Persistent Cough',
            'phlegm' => 'Phlegm',
            'breathless' => 'Breathless',
            'chest_pain' => 'Chest Pain',
            'chest_nuisance' => 'Chest Nuisance',
            'high_pressure' => 'High Pressure',
            'heartblow' => 'Heartblow',
            'varicose' => 'Varicose',
            'swelling' => 'Swelling',
            'pain_urinating' => 'Pain Urinating',
            'urine_count' => 'Urine Count',
            'urine_blood' => 'Urine Blood',
            'urine_infection' => 'Urine Infection',
            'excesive_bleeding' => 'Excesive Bleeding',
            'bruise' => 'Bruise',
            'anemia' => 'Anemia',
            'acne' => 'Acne',
            'itch' => 'Itch',
            'feet_ulceration' => 'Feet Ulceration',
            'mole_change' => 'Mole Change',
            'hair_loss' => 'Hair Loss',
            'skin_color_change' => 'Skin Color Change',
            'nail_problems' => 'Nail Problems',
            'gout' => 'Gout',
            'sciatica' => 'Sciatica',
            'arthritis' => 'Arthritis',
            'lumbar_desease' => 'Lumbar Desease',
            'pain_paralysis' => 'Pain Paralysis',
            'flatfoot' => 'Flatfoot',
        ];
    }

            public function getPatient()
            {
                return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
            }
}
