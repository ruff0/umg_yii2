<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prescription;

class PrescriptionSearch extends Prescription
{
    public function rules()
    {
        return [
          [['id'], 'integer'],
            [['patient', 'doctor', 'diagnosis', 'cie10_code', 'date', 'valid_until', 'note'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Prescription::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'valid_until' => $this->valid_until,
            'note' => $this->note

        ]);

        $query->andFilterWhere(['like', 'patient', $this->patient])
            ->andFilterWhere(['like', 'doctor', $this->doctor])
            ->andFilterWhere(['like', 'diagnosis', $this->diagnosis])
            ->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
