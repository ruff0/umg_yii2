<?php

namespace app\models;

use Yii;

class PrescriptionDiagnostic extends \yii\db\ActiveRecord
{
    const UPDATE_TYPE_DIAGNOSTIC_CREATE = 'create';
    const UPDATE_TYPE_DIAGNOSTIC_UPDATE = 'update';
    const UPDATE_TYPE_DIAGNOSTIC_DELETE = 'delete';

    const SCENARIO_DIAGNOSTIC_BATCH_UPDATE = 'batchUpdate';

    private $_updateTypeDiagnostic;

    public function getUpdateTypeDiagnostic()
    {
        if (empty($this->_updateTypeDiagnostic)) {
            if ($this->isNewRecord) {
                $this->_updateTypeDiagnostic = self::UPDATE_TYPE_DIAGNOSTIC_CREATE;
            } else {
                $this->_updateTypeDiagnostic = self::UPDATE_TYPE_DIAGNOSTIC_UPDATE;
            }
        }

        return $this->_updateTypeDiagnostic;
    }

    public function setUpdateTypeDiagnostic($value)
    {
        $this->_updateTypeDiagnostic = $value;
    }

    public static function tableName()
    {
        return 'prescription_diagnostic';
    }

    public function rules()
    {
        return [
            ['updateTypeDiagnostic', 'required', 'on' => self::SCENARIO_DIAGNOSTIC_BATCH_UPDATE],
            ['updateTypeDiagnostic',
                'in',
                'range' => [self::UPDATE_TYPE_DIAGNOSTIC_CREATE, self::UPDATE_TYPE_DIAGNOSTIC_UPDATE, self::UPDATE_TYPE_DIAGNOSTIC_DELETE],
                'on' => self::SCENARIO_DIAGNOSTIC_BATCH_UPDATE]
            ,
            //allowing it to be empty because it will be filled by the PrescriptionController
            ['prescription_id', 'required', 'except' => self::SCENARIO_DIAGNOSTIC_BATCH_UPDATE],
            [['prescription_id'], 'integer'],
            // [['diagnostic', 'cie10_code'], 'required'],
            [['diagnostic', 'cie10_code'], 'string', 'max' => 255],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prescription_id' => 'Receta No.',
            'diagnostic' => 'Diagnóstico',
            'cie10_code' => 'CIE10',
        ];
    }

    public function getPrescription()
    {
        return $this->hasOne(Prescription::className(), ['id' => 'prescription_id']);
    }
}
