<?php
namespace app\models;

use Yii;

class Prescription extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'prescription';
    }
    public function rules()
    {
        return [
          [['patient','doctor',
          // 'diagnosis','cie10_code'
        ], 'required'],
            // ['date', 'required'],
            [['patient', 'doctor',
            // 'diagnosis', 'cie10_code',
             'date', 'valid_until' ,'note'], 'string', 'max' => 255],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'Fólio',
            // 'cie10_code' => 'CIE10',
            'patient' => 'Paciente',
            'doctor' => 'Médico',
            // 'diagnosis' => 'Diagnóstico',
            'date' => 'Fecha',
            'valid_until' => 'Caducidad',
            'note' => 'Anotaciones',
        ];
    }

    public function getPrescriptionDetails()
    {
        return $this->hasMany(PrescriptionDetail::className(), ['prescription_id' => 'id']);
    }


    public function getPrescriptionDiagnostics()
    {
        return $this->hasMany(PrescriptionDiagnostic::className(), ['prescription_id' => 'id']);
    }

    public function getCie10()
    {
        return $this->hasOne(Cie10::className(), ['id10' => 'cie10_code']);
    }
}
