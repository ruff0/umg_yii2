<?php

namespace app\models;

use Yii;

class Physical extends \yii\db\ActiveRecord
{
  const UPDATE_TYPE_PHYSICAL_CREATE = 'create';
  const UPDATE_TYPE_PHYSICAL_UPDATE = 'update';
  const UPDATE_TYPE_PHYSICAL_DELETE = 'delete';
  const SCENARIO_PHYSICAL_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypePhysical;
    public function getUpdateTypePhysical()
    {
        if (empty($this->_updateTypePhysical)) {
            if ($this->isNewRecord) {
                $this->_updateTypePhysical = self::UPDATE_TYPE_PHYSICAL_CREATE;
            } else {
                $this->_updateTypePhysical = self::UPDATE_TYPE_PHYSICAL_UPDATE;
            }
        }

        return $this->_updateTypePhysical;
    }

    public function setUpdateTypePhysical($value)
    {
        $this->_updateTypePhysical = $value;
    }

    public static function tableName()
    {
        return 'physical';
    }

    public function rules()
    {
        return [
          ['updateTypePhysical', 'required', 'on' => self::SCENARIO_PHYSICAL_BATCH_UPDATE],
          ['updateTypePhysical',
              'in',
              'range' => [self::UPDATE_TYPE_PHYSICAL_CREATE, self::UPDATE_TYPE_PHYSICAL_UPDATE, self::UPDATE_TYPE_PHYSICAL_DELETE],
              'on' => self::SCENARIO_PHYSICAL_BATCH_UPDATE],

          ['patient_id', 'required', 'except' => self::SCENARIO_PHYSICAL_BATCH_UPDATE],
            [['patient_id', 'date'], 'required'],
            [['patient_id', 'age', 'pulse', 'eye_left', 'eye_right', 'hear_left', 'hear_right'], 'integer'],
            [['waist', 'weight', 'height', 'mmhg', 'temperature'], 'number'],
            [['date', 'attention', 'memory', 'ideation', 'psychotechnic', 'sensibility', 'reflex', 'muscle', 'force', 'nutritional', 'eye_condition', 'hear_condition', 'breath', 'vascular', 'skin', 'digestive', 'genital','neurologic','endocrinologic','psychosocial'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'date' => 'Fecha',
            'age' => 'Edad',
            'waist' => 'Cintura',
            'weight' => 'Peso (Kg)',
            'height' => 'Altura (m)',
            'mmhg' => 'T/A (mmHg)',
            'pulse' => 'Pulso',
            'temperature' => 'Temperatura',
            'attention' => 'Atención',
            'memory' => 'Memoria',
            'ideation' => 'Ideación',
            'psychotechnic' => 'Psicotécnica',
            'sensibility' => 'Sensibilidad',
            'reflex' => 'Reflejos',
            'muscle' => 'Musculatura',
            'force' => 'Fuerza',
            'nutritional' => 'Estado Nutricional',
            'eye_condition' => 'Condición Visual',
            'eye_left' => 'Ojo Izquierdo',
            'eye_right' => 'Ojo Derecho',
            'hear_condition' => 'Condición Auditiva',
            'hear_left' => 'Oído Izquierdo',
            'hear_right' => 'Oído Derecho',
            'breath' => 'Respiración',
            'vascular' => 'Cardio-Vascular',
            'skin' => 'Piel',
            'digestive' => 'Digestivo',
            'genital' => 'Genito-Urinario',
            'neurologic' => 'Neurológico',
            'endocrinologic' => 'Endocrinológico',
            'psychosocial' => 'Psicosocial',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
  }
