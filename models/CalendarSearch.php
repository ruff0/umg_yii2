<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calendar;

class CalendarSearch extends Calendar
{

    public function search($params)
    {
        $query = Calendar::find();
        // $query = Calendar::find()->where(['user_id'=>Yii::$app->user->getId()]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 30],
            'sort'=> ['defaultOrder' => ['start'=>SORT_DESC]]
        ]);


            if (!($this->load($params) && $this->validate())) {
                return $dataProvider;
            }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'patient_name' => $this->patient_name,
            'date_time_in' => $this->date_time_in,
        ]);

        return $dataProvider;
    }
}
