<?php

namespace app\models;

use Yii;

class Personal extends \yii\db\ActiveRecord
{


  const UPDATE_TYPE_PERSONAL_CREATE = 'create';
  const UPDATE_TYPE_PERSONAL_UPDATE = 'update';
  const UPDATE_TYPE_PERSONAL_DELETE = 'delete';

  const SCENARIO_PERSONAL_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypePersonal;




      public function getUpdateTypePersonal()
      {
          if (empty($this->_updateTypePersonal)) {
              if ($this->isNewRecord) {
                  $this->_updateTypePersonal = self::UPDATE_TYPE_PERSONAL_CREATE;
              } else {
                  $this->_updateTypePersonal = self::UPDATE_TYPE_PERSONAL_UPDATE;
              }
          }

          return $this->_updateTypePersonal;
      }

      public function setUpdateTypePersonal($value)
      {
          $this->_updateTypePersonal = $value;
      }
      /**
      * @inheritdoc
      */
      public static function tableName()
      {
        return 'personal';
      }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          ['updateTypePersonal', 'required', 'on' => self::SCENARIO_PERSONAL_BATCH_UPDATE],
          ['updateTypePersonal',
              'in',
              'range' => [self::UPDATE_TYPE_PERSONAL_CREATE, self::UPDATE_TYPE_PERSONAL_UPDATE, self::UPDATE_TYPE_PERSONAL_DELETE],
              'on' => self::SCENARIO_PERSONAL_BATCH_UPDATE],

          ['patient_id', 'required', 'except' => self::SCENARIO_PERSONAL_BATCH_UPDATE],

            [['patient_id'], 'integer'],
            [['desease', 'medication'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'No. Paciente',
            'desease' => 'Enfermedad',
            'medication' => 'Medicina',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
