<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cie10".
 *
 * @property string $id10
 * @property string $dec10
 * @property string $grp10
 */
class Cie10 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cie10';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id10'], 'required'],
            [['id10'], 'string', 'max' => 10],
            [['dec10'], 'string', 'max' => 400],
            [['grp10'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id10' => 'Código',
            'dec10' => 'Enfermedad',
            'grp10' => 'Grupo',
        ];
    }
}
