<?php
namespace app\models;
use Yii;

class Patient extends \yii\db\ActiveRecord
{
    public $photofile;

    public static function tableName()
    {
        return 'patient';
    }

    public function rules()
    {
        return [
            [['created_date', 'modified_date', 'birth_date'], 'safe'],
            [['name'], 'required'],
            [['age'], 'integer'],
            [['sex'], 'safe'],
            [['photofile'], 'file'],
            // [['file'], 'file'],
            [['name', 'photo', 'birth_place', 'status', 'scholarity', 'work_area', 'profession', 'experience', 'address', 'city', 'postal_code', 'phone', 'on_emergency', 'emergency_phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Creado',
            'modified_date' => 'Modificado',
            'name' => 'Nombre',
            'sex' => 'Género',
            'birth_date' => 'Fecha de Nacimiento',
            'birth_place' => 'Lugar de Nacimiento',
            'age' => 'Edad',
            'status' => 'Edo. Civil',
            'scholarity' => 'Escolaridad',
            'work_area' => 'Área de Trabajo',
            'profession' => 'Puesto',
            'experience' => 'Antiguedad',
            'address' => 'Dirección',
            'city' => 'Municipio',
            'postal_code' => 'C.P.',
            'phone' => 'Teléfono',
            'on_emergency' => 'En Emergencia Acudir A',
            'emergency_phone' => 'Teléfono de Emergencia',
            'photofile' => 'Fotografía',
        ];
    }

    public function getParental()
    {
        return $this->hasMany(Parental::className(), ['patient_id' => 'id']);
    }

    public function getPersonal()
    {
        return $this->hasMany(Personal::className(), ['patient_id' => 'id']);
    }

    public function getPersonalHistory()
    {
        return $this->hasMany(PersonalHistory::className(), ['patient_id' => 'id']);
    }

    public function getQuirurgicalIntervention()
    {
        return $this->hasMany(QuirurgicalIntervention::className(), ['patient_id' => 'id']);
    }

    public function getHospitalization()
    {
        return $this->hasMany(Hospitalization::className(), ['patient_id' => 'id']);
    }

    public function getGynecoObstetric()
    {
        return $this->hasMany(GynecoObstetric::className(), ['patient_id' => 'id']);
    }

    public function getDiet()
    {
        return $this->hasMany(Diet::className(), ['patient_id' => 'id']);
    }
    public function getAddiction()
    {
        return $this->hasMany(Addiction::className(), ['patient_id' => 'id']);
    }
    public function getSystem()
    {
        return $this->hasMany(System::className(), ['patient_id' => 'id']);
    }
    public function getPhysical()
    {
        return $this->hasMany(Physical::className(), ['patient_id' => 'id']);
    }
    public function getLabtest()
    {
        return $this->hasMany(Labtest::className(), ['patient_id' => 'id']);
    }
    public function getAllergic()
    {
        return $this->hasMany(Allergic::className(), ['patient_id' => 'id']);
    }
}
