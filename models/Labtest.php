<?php
namespace app\models;

use Yii;

class Labtest extends \yii\db\ActiveRecord
{
  const UPDATE_TYPE_LABTEST_CREATE = 'create';
  const UPDATE_TYPE_LABTEST_UPDATE = 'update';
  const UPDATE_TYPE_LABTEST_DELETE = 'delete';
  const SCENARIO_LABTEST_BATCH_UPDATE = 'batchUpdate';
  private $_updateTypeLabtest;
      public function getUpdateTypeLabtest()
      {
          if (empty($this->_updateTypeLabtest)) {
              if ($this->isNewRecord) {
                  $this->_updateTypeLabtest = self::UPDATE_TYPE_LABTEST_CREATE;
              } else {
                  $this->_updateTypeLabtest = self::UPDATE_TYPE_LABTEST_UPDATE;
              }
          }

          return $this->_updateTypeLabtest;
      }

      public function setUpdateTypeLabtest($value)
      {
          $this->_updateTypeLabtest = $value;
      }
    public static function tableName()
    {
        return 'labtest';
    }

    public function rules()
    {
        return [
          ['updateTypeLabtest', 'required', 'on' => self::SCENARIO_LABTEST_BATCH_UPDATE],
          ['updateTypeLabtest',
              'in',
              'range' => [self::UPDATE_TYPE_LABTEST_CREATE, self::UPDATE_TYPE_LABTEST_UPDATE, self::UPDATE_TYPE_LABTEST_DELETE],
              'on' => self::SCENARIO_LABTEST_BATCH_UPDATE],

          ['patient_id', 'required', 'except' => self::SCENARIO_LABTEST_BATCH_UPDATE],
            [['patient_id'], 'required'],
            [['patient_id'], 'integer'],
            [['analysis', 'result'], 'string', 'max' => 255],
            [['date'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'analysis' => 'Analysis',
            'result' => 'Result',
            'date' => 'Fecha'
        ];
    }

        public function getPatient()
        {
            return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
        }
}
