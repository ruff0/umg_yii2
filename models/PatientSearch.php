<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Patient;

class PatientSearch extends Patient
{
    public function rules()
    {
        return [
            [['id', 'sex', 'age'], 'integer'],
            [['created_date', 'modified_date', 'name', 'birth_date', 'birth_place', 'status', 'scholarity', 'work_area', 'profession', 'experience', 'address', 'city', 'postal_code', 'phone', 'on_emergency', 'emergency_phone'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Patient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'modified_date' => $this->modified_date,
            'sex' => $this->sex,
            'birth_date' => $this->birth_date,
            'age' => $this->age,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'birth_place', $this->birth_place])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'scholarity', $this->scholarity])
            ->andFilterWhere(['like', 'work_area', $this->work_area])
            ->andFilterWhere(['like', 'profession', $this->profession])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'on_emergency', $this->on_emergency])
            ->andFilterWhere(['like', 'emergency_phone', $this->emergency_phone]);

        return $dataProvider;
    }
}
