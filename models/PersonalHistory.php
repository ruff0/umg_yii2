<?php

namespace app\models;

use Yii;

class PersonalHistory extends \yii\db\ActiveRecord
{
const UPDATE_TYPE_PERSONAL_HISTORY_CREATE = 'create';
const UPDATE_TYPE_PERSONAL_HISTORY_UPDATE = 'update';
const UPDATE_TYPE_PERSONAL_HISTORY_DELETE = 'delete';

const SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE = 'batchUpdate';
private $_updateTypePersonalHistory;

    public function getUpdateTypePersonalHistory()
    {
        if (empty($this->_updateTypePersonalHistory)) {
            if ($this->isNewRecord) {
                $this->_updateTypePersonalHistory = self::UPDATE_TYPE_PERSONAL_HISTORY_CREATE;
            } else {
                $this->_updateTypePersonalHistory = self::UPDATE_TYPE_PERSONAL_HISTORY_UPDATE;
            }
        }

        return $this->_updateTypePersonalHistory;
    }

    public function setUpdateTypePersonalHistory($value)
    {
        $this->_updateTypePersonalHistory = $value;
    }

    public static function tableName()
    {
        return 'personal_history';
    }


    public function rules()
    {
        return [
      ['updateTypePersonalHistory', 'required', 'on' => self::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE],
      ['updateTypePersonalHistory',
          'in',
          'range' => [self::UPDATE_TYPE_PERSONAL_HISTORY_CREATE, self::UPDATE_TYPE_PERSONAL_HISTORY_UPDATE, self::UPDATE_TYPE_PERSONAL_HISTORY_DELETE],
          'on' => self::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE],

            [['patient_id'], 'required'],
            [['patient_id', 'desease_suffered_age'], 'integer'],
            [['desease_suffered'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'No. Paciente',
            'desease_suffered' => 'Padecimiento',
            'desease_suffered_age' => 'Edad',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
