<?php

namespace app\models;

use Yii;

class QuirurgicalIntervention extends \yii\db\ActiveRecord
{
const UPDATE_TYPE_QUIRURGICAL_INTERVENTION_CREATE = 'create';
const UPDATE_TYPE_QUIRURGICAL_INTERVENTION_UPDATE = 'update';
const UPDATE_TYPE_QUIRURGICAL_INTERVENTION_DELETE = 'delete';

const SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE = 'batchUpdate';
private $_updateTypeQuirurgicalIntervention;

    public function getUpdateTypeQuirurgicalIntervention()
    {
        if (empty($this->_updateTypeQuirurgicalIntervention)) {
            if ($this->isNewRecord) {
                $this->_updateTypeQuirurgicalIntervention = self::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_CREATE;
            } else {
                $this->_updateTypeQuirurgicalIntervention = self::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_UPDATE;
            }
        }

        return $this->_updateTypeQuirurgicalIntervention;
    }

    public function setUpdateTypeQuirurgicalIntervention($value)
    {
        $this->_updateTypeQuirurgicalIntervention = $value;
    }

    public static function tableName()
    {
        return 'quirurgical_intervention';
    }


    public function rules()
    {
        return [
      ['updateTypeQuirurgicalIntervention', 'required', 'on' => self::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE],
      ['updateTypeQuirurgicalIntervention',
          'in',
          'range' => [self::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_CREATE, self::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_UPDATE, self::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_DELETE],
          'on' => self::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE],

            [['patient_id'], 'required'],
            [['patient_id', 'at_age'], 'integer'],
            [['cause'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'No. Paciente',
            'cause' => 'Causa',
            'at_age' => 'Edad',
        ];
    }

    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }
}
