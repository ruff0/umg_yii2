<?php

namespace app\models;

use Yii;

class PrescriptionDetail extends \yii\db\ActiveRecord
{
    const UPDATE_TYPE_CREATE = 'create';
    const UPDATE_TYPE_UPDATE = 'update';
    const UPDATE_TYPE_DELETE = 'delete';

    const SCENARIO_BATCH_UPDATE = 'batchUpdate';

    private $_updateType;

    public function getUpdateType()
    {
        if (empty($this->_updateType)) {
            if ($this->isNewRecord) {
                $this->_updateType = self::UPDATE_TYPE_CREATE;
            } else {
                $this->_updateType = self::UPDATE_TYPE_UPDATE;
            }
        }

        return $this->_updateType;
    }

    public function setUpdateType($value)
    {
        $this->_updateType = $value;
    }

    public static function tableName()
    {
        return 'prescription_detail';
    }

    public function rules()
    {
        return [
            ['updateType', 'required', 'on' => self::SCENARIO_BATCH_UPDATE],
            ['updateType',
                'in',
                'range' => [self::UPDATE_TYPE_CREATE, self::UPDATE_TYPE_UPDATE, self::UPDATE_TYPE_DELETE],
                'on' => self::SCENARIO_BATCH_UPDATE]
            ,
            ['medicine_name', 'required'],
            //allowing it to be empty because it will be filled by the PrescriptionController
            ['prescription_id', 'required', 'except' => self::SCENARIO_BATCH_UPDATE],
            [['prescription_id', 'quantity'], 'integer'],
            [['medicine_name'], 'string', 'max' => 255],
            [['dosage'], 'integer'],
            [['frequency'], 'integer'],
            [['duration'], 'integer'],
            [['indication'], 'string', 'max' => 255]

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prescription_id' => 'Receta No.',
            'quantity' => 'Cantidad',
            'medicine_name' => 'Medicamento',
            // 'dosage' => 'Dósis',
            // 'frequency' => 'Frecuéncia',
            // 'indication' => 'Indicación',
            'dosage' => 'Tomar',
            'frequency' => 'Cada',
            'indication' => 'Durante',
        ];
    }

    public function getPrescription()
    {
        return $this->hasOne(Prescription::className(), ['id' => 'prescription_id']);
    }
}
