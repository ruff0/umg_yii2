<?php

use yii\db\Schema;
use yii\db\Migration;

class m150204_103949_create_prescription_tables extends Migration
{
    public function up()
    {
        $this->createTable('prescription', [
            'id' => 'pk',
            'diagnosis' => 'string'
        ]);

        $this->createTable('prescription_detail', [
            'id' => 'pk',
            'prescription_id' => 'int',
            'medicine_name' => 'string'
        ]);

        $this->addForeignKey('prescription_detail_prescription_fk', 'prescription_detail', 'prescription_id', 'prescription', 'id', 'RESTRICT', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('prescription_detail');
        $this->dropTable('prescription');
    }
}
