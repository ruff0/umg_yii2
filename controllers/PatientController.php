<?php
namespace app\controllers;

use app\models\Parental;
use app\models\Personal;
use app\models\Diet;
use app\models\Addiction;
use app\models\System;
use app\models\Physical;
use app\models\Labtest;
use app\models\Allergic;
use app\models\PersonalHistory;
use app\models\QuirurgicalIntervention;
use app\models\Hospitalization;
use app\models\GynecoObstetric;
use app\models\form\PatientForm;
use Yii;
use app\models\Patient;
use app\models\PatientSearch;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use app\models\FormUpload;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class PatientController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpload($id)
    {
        $modelid = $this->findModel($id);
        $model = new FormUpload;
        $msg = null;

        $path = 'patients/'.$modelid->id.'/';
        FileHelper::createDirectory($path);

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstances($model, 'file');

            if ($model->file && $model->validate()) {
                foreach ($model->file as $file) {
                    $file->saveAs($path . $file->baseName . '.' . $file->extension);
                    $msg = "<p><strong class='label label-info'>Subida realizada</strong></p>";
                }
            }
        }
        return $this->render("upload", ["model" => $model, "msg" => $msg]);
    }

    public function actionIndex()
    {
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReport()
    {
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionAllergy($id)
    {
        $model = $this->findModel($id);



        return $this->render('view', [
            'model' => $model,
          ]);
    }


    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;

        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);


        foreach ($formGynecoObstetrics as $i => $formGynecoObstetric) {
            if (isset($formGynecoObstetric['id']) && isset($formGynecoObstetric['updateTypeGynecoObstetric']) && $formGynecoObstetric['updateTypeGynecoObstetric'] != GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE) {
                $modelGynecoObstetric = GynecoObstetric::findOne(['id' => $formGynecoObstetric['id'], 'patient_id' => $model->id]);
                $modelGynecoObstetric->setScenario(GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[$i] = $modelGynecoObstetric;
            } else {
                $modelGynecoObstetric = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[] = $modelGynecoObstetric;
            }
        }


        foreach ($formDiets as $i => $formDiet) {
            if (isset($formDiet['id']) && isset($formDiet['updateTypeDiet']) && $formDiet['updateTypeDiet'] != Diet::UPDATE_TYPE_DIET_CREATE) {
                $modelDiet = Diet::findOne(['id' => $formDiet['id'], 'patient_id' => $model->id]);
                $modelDiet->setScenario(Diet::SCENARIO_DIET_BATCH_UPDATE);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[$i] = $modelDiet;
            } else {
                $modelDiet = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[] = $modelDiet;
            }
        }

        foreach ($formAddictions as $i => $formAddiction) {
            if (isset($formAddiction['id']) && isset($formAddiction['updateTypeAddiction']) && $formAddiction['updateTypeAddiction'] != Addiction::UPDATE_TYPE_ADDICTION_CREATE) {
                $modelAddiction = Addiction::findOne(['id' => $formAddiction['id'], 'patient_id' => $model->id]);
                $modelAddiction->setScenario(Addiction::SCENARIO_ADDICTION_BATCH_UPDATE);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[$i] = $modelAddiction;
            } else {
                $modelAddiction = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[] = $modelAddiction;
            }
        }

        if (Yii::$app->request->post('addRowGynecoObstetricAlone') == 'true') {
            $modelGynecoObstetrics[] = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
            return $this->render('update_gynecoobstetric', [
              'model' => $model,
              'modelPersonals' => $modelPersonals,
              'modelDetails' => $modelDetails,
              'modelPersonalHistorys' => $modelPersonalHistorys,
              'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
              'modelHospitalizations' => $modelHospitalizations,
              'modelGynecoObstetrics' => $modelGynecoObstetrics,
              'modelDiets' => $modelDiets,
              'modelAddictions' => $modelAddictions,
              'modelSystems' => $modelSystems,
              'modelPhysicals' => $modelPhysicals,
              'modelLabtests' => $modelLabtests,
          ]);
        }


        if (Yii::$app->request->post('addRowDietAlone') == 'true') {
            $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
            return $this->render('update_habits', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

        if (Yii::$app->request->post('addRowAddictionAlone') == 'true') {
            $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
            return $this->render('update_habits', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

        if (Yii::$app->request->post('addRowHabits') == 'true') {
            $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
            $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
            return $this->render('update_habits', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();


                foreach ($modelGynecoObstetrics as $modelGynecoObstetric) {
                    //details that has been flagged for deletion will be deleted
                                  if ($modelGynecoObstetric->updateTypeGynecoObstetric == GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE) {
                                      $modelGynecoObstetric->delete();
                                  } else {
                                      //new or updated records go here
                                      $modelGynecoObstetric->patient_id = $model->id;
                                      $modelGynecoObstetric->save();
                                  }
                }
                foreach ($modelDiets as $modelDiet) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelDiet->updateTypeDiet == Diet::UPDATE_TYPE_DIET_DELETE) {
                                                $modelDiet->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelDiet->patient_id = $model->id;
                                                $modelDiet->save();
                                            }
                }
                foreach ($modelAddictions as $modelAddiction) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelAddiction->updateTypeAddiction == Addiction::UPDATE_TYPE_ADDICTION_DELETE) {
                                                $modelAddiction->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelAddiction->patient_id = $model->id;
                                                $modelAddiction->save();
                                            }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('view', [
        'model' => $model,
        'modelDetails' => $modelDetails,
        'modelPersonals' => $modelPersonals,
        'modelPersonalHistorys' => $modelPersonalHistorys,
        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
        'modelHospitalizations' => $modelHospitalizations,
        'modelGynecoObstetrics' => $modelGynecoObstetrics,
        'modelDiets' => $modelDiets,
        'modelAddictions' => $modelAddictions,
        'modelSystems' => $modelSystems,
        'modelPhysicals' => $modelPhysicals,
        'modelLabtests' => $modelLabtests,
      ]);
    }

    public function actionCreate()
    {
        $model = new Patient();
        $modelDetails = [];
        $modelPersonals = [];
        $modelPersonalHistorys = [];
        $modelQuirurgicalInterventions = [];
        $modelHospitalizations = [];
        $modelGynecoObstetrics = [];
        $modelDiets = [];
        $modelAddictions = [];
        $modelSystems = [];
        $modelPhysicals = [];
        $modelLabtests = [];

        $formDetails = Yii::$app->request->post('Parental', []);
        foreach ($formDetails as $i => $formDetail) {
            $modelDetail = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
            $modelDetail->setAttributes($formDetail);
            $modelDetails[] = $modelDetail;
        }

        $formPersonals = Yii::$app->request->post('Personal', []);
        foreach ($formPersonals as $i => $formPersonal) {
            $modelPersonal = new Personal(['scenario' => Personal::SCENARIO_PERSONAL_BATCH_UPDATE]);
            $modelPersonal->setAttributes($formPersonal);
            $modelPersonals[] = $modelPersonal;
        }

        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        foreach ($formPersonalHistorys as $i => $formPersonalHistory) {
            $modelPersonalHistory = new PersonalHistory(['scenario' => PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE]);
            $modelPersonalHistory->setAttributes($formPersonalHistory);
            $modelPersonalHistorys[] = $modelPersonalHistory;
        }

        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        foreach ($formQuirurgicalInterventions as $i => $formQuirurgicalIntervention) {
            $modelQuirurgicalIntervention = new QuirurgicalIntervention(['scenario' => QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE]);
            $modelQuirurgicalIntervention->setAttributes($formQuirurgicalIntervention);
            $modelQuirurgicalInterventions[] = $modelQuirurgicalIntervention;
        }

        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        foreach ($formHospitalizations as $i => $formHospitalization) {
            $modelHospitalization = new Hospitalization(['scenario' => Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE]);
            $modelHospitalization->setAttributes($formHospitalization);
            $modelHospitalizations[] = $modelHospitalization;
        }

        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        foreach ($formGynecoObstetrics as $i => $formGynecoObstetric) {
            $modelGynecoObstetric = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
            $modelGynecoObstetric->setAttributes($formGynecoObstetric);
            $modelGynecoObstetrics[] = $modelGynecoObstetric;
        }

        $formDiets = Yii::$app->request->post('Diet', []);
        foreach ($formDiets as $i => $formDiet) {
            $modelDiet = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
            $modelDiet->setAttributes($formDiet);
            $modelDiets[] = $modelDiet;
        }

        $formAddictions = Yii::$app->request->post('Addiction', []);
        foreach ($formAddictions as $i => $formAddiction) {
            $modelAddiction = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
            $modelAddiction->setAttributes($formAddiction);
            $modelAddictions[] = $modelAddiction;
        }

        $formSystems = Yii::$app->request->post('System', []);
        foreach ($formSystems as $i => $formSystem) {
            $modelSystem = new System(['scenario' => System::SCENARIO_SYSTEM_BATCH_UPDATE]);
            $modelSystem->setAttributes($formSystem);
            $modelSystems[] = $modelSystem;
        }

        $formPhysicals = Yii::$app->request->post('Physical', []);
        foreach ($formPhysicals as $i => $formPhysical) {
            $modelPhysical = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
            $modelPhysical->setAttributes($formPhysical);
            $modelPhysicals[] = $modelPhysical;
        }

        $formLabtests = Yii::$app->request->post('Labtest', []);
        foreach ($formLabtests as $i => $formLabtest) {
            $modelLabtest = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
            $modelLabtest->setAttributes($formLabtest);
            $modelLabtests[] = $modelLabtest;
        }

        //handling if the addRows buttons has been pressed

if ((Yii::$app->request->post('addRow')=='true') ||
(Yii::$app->request->post('addRowPersonal') == 'true') ||
(Yii::$app->request->post('addRowPersonalHistory') == 'true') ||
(Yii::$app->request->post('addRowQuirurgicalIntervention') == 'true') ||
(Yii::$app->request->post('addRowHospitalization') == 'true') ||
(Yii::$app->request->post('addRowGynecoObstetric') == 'true') ||
(Yii::$app->request->post('addRowDiet') == 'true') ||
(Yii::$app->request->post('addRowAddiction') == 'true') ||
(Yii::$app->request->post('addRowSystem') == 'true') ||
(Yii::$app->request->post('addRowPhysical') == 'true') ||
(Yii::$app->request->post('addRowLabtest') == 'true')) {
    if (Yii::$app->request->post('addRow') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelDetails[] = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowPersonal') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelPersonals[] = new Personal(['scenario' => Personal::SCENARIO_PERSONAL_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowPersonalHistory') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelPersonalHistorys[] = new PersonalHistory(['scenario' => PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowQuirurgicalIntervention') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelQuirurgicalInterventions[] = new QuirurgicalIntervention(['scenario' => QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowHospitalization') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelHospitalizations[] = new Hospitalization(['scenario' => Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowGynecoObstetric') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelGynecoObstetrics[] = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowDiet') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowAddiction') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
    }
    if (Yii::$app->request->post('addRowSystem') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelSystems[] = new System(['scenario' => System::SCENARIO_SYSTEM_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowPhysical') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelPhysicals[] = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
    }

    if (Yii::$app->request->post('addRowLabtest') == 'true') {
        $model->load(Yii::$app->request->post());
        $modelLabtests[] = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
    }

    return $this->render('create', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
            'modelLabtests' => $modelLabtests,
        ]);
} else {
}

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $photoName = $model->name;
            $model->photofile = UploadedFile::getInstance($model, 'photofile');
            if ($model->photofile !==null) {
                $model->photofile->saveAs('patients/'.$photoName.'.'.$model->photofile->extension);
                $model->photo = 'patients/'.$photoName.'.'.$model->photofile->extension;
            } else {
            }

            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets)  || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals) || ($modelLabtests) && $model->validate()) {
                $model->save();

                foreach ($modelDetails as $modelDetail) {
                    $modelDetail->patient_id = $model->id;
                    $modelDetail->save();
                }
                foreach ($modelPersonals as $modelPersonal) {
                    $modelPersonal->patient_id = $model->id;
                    $modelPersonal->save();
                }
                foreach ($modelPersonalHistorys as $modelPersonalHistory) {
                    $modelPersonalHistory->patient_id = $model->id;
                    $modelPersonalHistory->save();
                }
                foreach ($modelQuirurgicalInterventions as $modelQuirurgicalIntervention) {
                    $modelQuirurgicalIntervention->patient_id = $model->id;
                    $modelQuirurgicalIntervention->save();
                }
                foreach ($modelHospitalizations as $modelHospitalization) {
                    $modelHospitalization->patient_id = $model->id;
                    $modelHospitalization->save();
                }
                foreach ($modelGynecoObstetrics as $modelGynecoObstetric) {
                    $modelGynecoObstetric->patient_id = $model->id;
                    $modelGynecoObstetric->save();
                }
                foreach ($modelDiets as $modelDiet) {
                    $modelDiet->patient_id = $model->id;
                    $modelDiet->save();
                }
                foreach ($modelAddictions as $modelAddiction) {
                    $modelAddiction->patient_id = $model->id;
                    $modelAddiction->save();
                }
                foreach ($modelSystems as $modelSystem) {
                    $modelSystem->patient_id = $model->id;
                    $modelSystem->save();
                }
                foreach ($modelPhysicals as $modelPhysical) {
                    $modelPhysical->patient_id = $model->id;
                    $modelPhysical->save();
                }
                foreach ($modelLabtests as $modelLabtest) {
                    $modelLabtest->patient_id = $model->id;
                    $modelLabtest->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
                'model' => $model,
                'modelDetails' => $modelDetails,
                'modelPersonals' => $modelPersonals,
                'modelPersonalHistorys' => $modelPersonalHistorys,
                'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                'modelHospitalizations' => $modelHospitalizations,
                'modelGynecoObstetrics' => $modelGynecoObstetrics,
                'modelDiets' => $modelDiets,
                'modelAddictions' => $modelAddictions,
                'modelSystems' => $modelSystems,
                'modelPhysicals' => $modelPhysicals,
                'modelLabtests' => $modelLabtests,
            ]);
    }




    public function actionAddallergics($id)
    {
        $model = $this->findModel($id);
        $modelAllergics = $model->allergic;

        $formAllergics = Yii::$app->request->post('Allergic', []);

        foreach ($formAllergics as $i => $formAllergic) {
            if (isset($formAllergic['id']) && isset($formAllergic['updateTypeAllergic']) && $formAllergic['updateTypeAllergic'] != Allergic::UPDATE_TYPE_ALLERGIC_CREATE) {
                $modelAllergic = Allergic::findOne(['id' => $formAllergic['id'], 'patient_id' => $model->id]);
                $modelAllergic->setScenario(Allergic::SCENARIO_ALLERGIC_BATCH_UPDATE);
                $modelAllergic->setAttributes($formAllergic);
                $modelAllergics[$i] = $modelAllergic;
            } else {
                $modelAllergic = new Allergic(['scenario' => Allergic::SCENARIO_ALLERGIC_BATCH_UPDATE]);
                $modelAllergic->setAttributes($formAllergic);
                $modelAllergics[] = $modelAllergic;
            }
        }

        if (Yii::$app->request->post('addRowAllergicAlone') == 'true') {
            $modelAllergics[] = new Allergic(['scenario' => Allergic::SCENARIO_ALLERGIC_BATCH_UPDATE]);
            return $this->render('update_allergic', [
                              'model' => $model,

                              'modelAllergics' => $modelAllergics,
                          ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelAllergics) || $model->validate()) {
                $model->save();

                foreach ($modelAllergics as $modelAllergic) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelAllergic->updateTypeAllergic == Allergic::UPDATE_TYPE_ALLERGIC_DELETE) {
                                                      $modelAllergic->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelAllergic->patient_id = $model->id;
                                                      $modelAllergic->save();
                                                  }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update_allergic', [
                  'model' => $model,
                  'modelAllergics' => $modelAllergics,
              ]);
    }

    public function actionAddgynecoobstetric($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;


        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);


        foreach ($formGynecoObstetrics as $i => $formGynecoObstetric) {
            if (isset($formGynecoObstetric['id']) && isset($formGynecoObstetric['updateTypeGynecoObstetric']) && $formGynecoObstetric['updateTypeGynecoObstetric'] != GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE) {
                $modelGynecoObstetric = GynecoObstetric::findOne(['id' => $formGynecoObstetric['id'], 'patient_id' => $model->id]);
                $modelGynecoObstetric->setScenario(GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[$i] = $modelGynecoObstetric;
            } else {
                $modelGynecoObstetric = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[] = $modelGynecoObstetric;
            }
        }


        if (Yii::$app->request->post('addRowGynecoObstetricAlone') == 'true') {
            $modelGynecoObstetrics[] = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
            return $this->render('update_gynecoobstetric', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();


                foreach ($modelGynecoObstetrics as $modelGynecoObstetric) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelGynecoObstetric->updateTypeGynecoObstetric == GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE) {
                                                $modelGynecoObstetric->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelGynecoObstetric->patient_id = $model->id;
                                                $modelGynecoObstetric->save();
                                            }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update_gynecoobstetric', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
            'modelLabtests' => $modelLabtests,
        ]);
    }




    public function actionAddphysical($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;


        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);


        foreach ($formPhysicals as $i => $formPhysical) {
            if (isset($formPhysical['id']) && isset($formPhysical['updateTypePhysical']) && $formPhysical['updateTypePhysical'] != Physical::UPDATE_TYPE_PHYSICAL_CREATE) {
                $modelPhysical = Physical::findOne(['id' => $formPhysical['id'], 'patient_id' => $model->id]);
                $modelPhysical->setScenario(Physical::SCENARIO_PHYSICAL_BATCH_UPDATE);
                $modelPhysical->setAttributes($formPhysical);
                $modelPhysicals[$i] = $modelPhysical;
            } else {
                $modelPhysical = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
                $modelPhysical->setAttributes($formPhysical);
                $modelPhysicals[] = $modelPhysical;
            }
        }


        if (Yii::$app->request->post('addRowPhysicalAlone') == 'true') {
            $modelPhysicals[] = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
            return $this->render('update_physical', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

                //handling the nutriotional_state update button
                if (Yii::$app->request->post('updateNutritional') == 'true') {
                    $model->load(Yii::$app->request->post());
                    // $modelDetails[] = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
                    return $this->render('update_physical', [
                        'model' => $model,
                        'modelDetails' => $modelDetails,
                        'modelPersonals' => $modelPersonals,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
                }


        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();


                foreach ($modelPhysicals as $modelPhysical) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelPhysical->updateTypePhysical == Physical::UPDATE_TYPE_PHYSICAL_DELETE) {
                                                $modelPhysical->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelPhysical->patient_id = $model->id;
                                                $modelPhysical->save();
                                            }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update_physical', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
            'modelLabtests' => $modelLabtests,
        ]);
    }


    public function actionAddanalysis($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;


        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);


        foreach ($formLabtests as $i => $formLabtest) {
            if (isset($formLabtest['id']) && isset($formLabtest['updateTypeLabtest']) && $formLabtest['updateTypeLabtest'] != Labtest::UPDATE_TYPE_LABTEST_CREATE) {
                $modelLabtest = Labtest::findOne(['id' => $formLabtest['id'], 'patient_id' => $model->id]);
                $modelLabtest->setScenario(Labtest::SCENARIO_LABTEST_BATCH_UPDATE);
                $modelLabtest->setAttributes($formLabtest);
                $modelLabtests[$i] = $modelLabtest;
            } else {
                $modelLabtest = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
                $modelLabtest->setAttributes($formLabtest);
                $modelLabtests[] = $modelLabtest;
            }
        }


        if (Yii::$app->request->post('addRowLabtestAlone') == 'true') {
            $modelLabtests[] = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
            return $this->render('update_labtest', [
                          'model' => $model,
                          'modelPersonals' => $modelPersonals,
                          'modelDetails' => $modelDetails,
                          'modelPersonalHistorys' => $modelPersonalHistorys,
                          'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                          'modelHospitalizations' => $modelHospitalizations,
                          'modelGynecoObstetrics' => $modelGynecoObstetrics,
                          'modelDiets' => $modelDiets,
                          'modelAddictions' => $modelAddictions,
                          'modelSystems' => $modelSystems,
                          'modelPhysicals' => $modelPhysicals,
                          'modelLabtests' => $modelLabtests,
                      ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();


                foreach ($modelLabtests as $modelLabtest) {
                    //details that has been flagged for deletion will be deleted
                                              if ($modelLabtest->updateTypeLabtest == Labtest::UPDATE_TYPE_LABTEST_DELETE) {
                                                  $modelLabtest->delete();
                                              } else {
                                                  //new or updated records go here
                                                  $modelLabtest->patient_id = $model->id;
                                                  $modelLabtest->save();
                                              }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update_labtest', [
              'model' => $model,
              'modelDetails' => $modelDetails,
              'modelPersonals' => $modelPersonals,
              'modelPersonalHistorys' => $modelPersonalHistorys,
              'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
              'modelHospitalizations' => $modelHospitalizations,
              'modelGynecoObstetrics' => $modelGynecoObstetrics,
              'modelDiets' => $modelDiets,
              'modelAddictions' => $modelAddictions,
              'modelSystems' => $modelSystems,
              'modelPhysicals' => $modelPhysicals,
              'modelLabtests' => $modelLabtests,
          ]);
    }


    public function actionAddhabits($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;


        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);


        foreach ($formDiets as $i => $formDiet) {
            if (isset($formDiet['id']) && isset($formDiet['updateTypeDiet']) && $formDiet['updateTypeDiet'] != Diet::UPDATE_TYPE_DIET_CREATE) {
                $modelDiet = Diet::findOne(['id' => $formDiet['id'], 'patient_id' => $model->id]);
                $modelDiet->setScenario(Diet::SCENARIO_DIET_BATCH_UPDATE);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[$i] = $modelDiet;
            } else {
                $modelDiet = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[] = $modelDiet;
            }
        }

        foreach ($formAddictions as $i => $formAddiction) {
            if (isset($formAddiction['id']) && isset($formAddiction['updateTypeAddiction']) && $formAddiction['updateTypeAddiction'] != Addiction::UPDATE_TYPE_ADDICTION_CREATE) {
                $modelAddiction = Addiction::findOne(['id' => $formAddiction['id'], 'patient_id' => $model->id]);
                $modelAddiction->setScenario(Addiction::SCENARIO_ADDICTION_BATCH_UPDATE);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[$i] = $modelAddiction;
            } else {
                $modelAddiction = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[] = $modelAddiction;
            }
        }


        if (Yii::$app->request->post('addRowDietAlone') == 'true') {
            $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
            return $this->render('update_habits', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

        if (Yii::$app->request->post('addRowAddictionAlone') == 'true') {
            $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
            return $this->render('update_habits', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }

        if (Yii::$app->request->post('addRowHabits') == 'true') {
            $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
            $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
            return $this->render('update_habits', [
                        'model' => $model,
                        'modelPersonals' => $modelPersonals,
                        'modelDetails' => $modelDetails,
                        'modelPersonalHistorys' => $modelPersonalHistorys,
                        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                        'modelHospitalizations' => $modelHospitalizations,
                        'modelGynecoObstetrics' => $modelGynecoObstetrics,
                        'modelDiets' => $modelDiets,
                        'modelAddictions' => $modelAddictions,
                        'modelSystems' => $modelSystems,
                        'modelPhysicals' => $modelPhysicals,
                        'modelLabtests' => $modelLabtests,
                    ]);
        }


        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();


                foreach ($modelDiets as $modelDiet) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelDiet->updateTypeDiet == Diet::UPDATE_TYPE_DIET_DELETE) {
                                                $modelDiet->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelDiet->patient_id = $model->id;
                                                $modelDiet->save();
                                            }
                }
                foreach ($modelAddictions as $modelAddiction) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelAddiction->updateTypeAddiction == Addiction::UPDATE_TYPE_ADDICTION_DELETE) {
                                                $modelAddiction->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelAddiction->patient_id = $model->id;
                                                $modelAddiction->save();
                                            }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update_habits', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
            'modelLabtests' => $modelLabtests,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;


        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);

        foreach ($formDetails as $i => $formDetail) {
            //loading the models if they are not new
                  if (isset($formDetail['id']) && isset($formDetail['updateType']) && $formDetail['updateType'] != Parental::UPDATE_TYPE_CREATE) {
                      //making sure that it is actually a child of the main model
                      $modelDetail = Parental::findOne(['id' => $formDetail['id'], 'patient_id' => $model->id]);
                      $modelDetail->setScenario(Parental::SCENARIO_BATCH_UPDATE);
                      $modelDetail->setAttributes($formDetail);
                      $modelDetails[$i] = $modelDetail;
                      //validate here if the modelDetail loaded is valid, and if it can be updated or deleted
                  } else {
                      $modelDetail = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
                      $modelDetail->setAttributes($formDetail);
                      $modelDetails[] = $modelDetail;
                  }
        }

        foreach ($formPersonals as $i => $formPersonal) {
            if (isset($formPersonal['id']) && isset($formPersonal['updateTypePersonal']) && $formPersonal['updateTypePersonal'] != Personal::UPDATE_TYPE_PERSONAL_CREATE) {
                $modelPersonal = Personal::findOne(['id' => $formPersonal['id'], 'patient_id' => $model->id]);
                $modelPersonal->setScenario(Personal::SCENARIO_PERSONAL_BATCH_UPDATE);
                $modelPersonal->setAttributes($formPersonal);
                $modelPersonals[$i] = $modelPersonal;
            } else {
                $modelPersonal = new Personal(['scenario' => Personal::SCENARIO_PERSONAL_BATCH_UPDATE]);
                $modelPersonal->setAttributes($formPersonal);
                $modelPersonals[] = $modelPersonal;
            }
        }

        foreach ($formPersonalHistorys as $i => $formPersonalHistory) {
            if (isset($formPersonalHistory['id']) && isset($formPersonalHistory['updateTypePersonalHistory']) && $formPersonalHistory['updateTypePersonalHistory'] != PersonalHistory::UPDATE_TYPE_PERSONAL_HISTORY_CREATE) {
                $modelPersonalHistory = PersonalHistory::findOne(['id' => $formPersonalHistory['id'], 'patient_id' => $model->id]);
                $modelPersonalHistory->setScenario(PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE);
                $modelPersonalHistory->setAttributes($formPersonalHistory);
                $modelPersonalHistorys[$i] = $modelPersonalHistory;
            } else {
                $modelPersonalHistory = new PersonalHistory(['scenario' => PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE]);
                $modelPersonalHistory->setAttributes($formPersonalHistory);
                $modelPersonalHistorys[] = $modelPersonalHistory;
            }
        }

        foreach ($formQuirurgicalInterventions as $i => $formQuirurgicalIntervention) {
            if (isset($formQuirurgicalIntervention['id']) && isset($formQuirurgicalIntervention['updateTypeQuirurgicalIntervention']) && $formQuirurgicalIntervention['updateTypeQuirurgicalIntervention'] != QuirurgicalIntervention::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_CREATE) {
                $modelQuirurgicalIntervention = QuirurgicalIntervention::findOne(['id' => $formQuirurgicalIntervention['id'], 'patient_id' => $model->id]);
                $modelQuirurgicalIntervention->setScenario(QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE);
                $modelQuirurgicalIntervention->setAttributes($formQuirurgicalIntervention);
                $modelQuirurgicalInterventions[$i] = $modelQuirurgicalIntervention;
            } else {
                $modelQuirurgicalIntervention = new QuirurgicalIntervention(['scenario' => QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE]);
                $modelQuirurgicalIntervention->setAttributes($formQuirurgicalIntervention);
                $modelQuirurgicalInterventions[] = $modelQuirurgicalIntervention;
            }
        }

        foreach ($formHospitalizations as $i => $formHospitalization) {
            if (isset($formHospitalization['id']) && isset($formHospitalization['updateTypeHospitalization']) && $formHospitalization['updateTypeHospitalization'] != Hospitalization::UPDATE_TYPE_HOSPITALIZATION_CREATE) {
                $modelHospitalization = Hospitalization::findOne(['id' => $formHospitalization['id'], 'patient_id' => $model->id]);
                $modelHospitalization->setScenario(Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE);
                $modelHospitalization->setAttributes($formHospitalization);
                $modelHospitalizations[$i] = $modelHospitalization;
            } else {
                $modelHospitalization = new Hospitalization(['scenario' => Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE]);
                $modelHospitalization->setAttributes($formHospitalization);
                $modelHospitalizations[] = $modelHospitalization;
            }
        }

        foreach ($formGynecoObstetrics as $i => $formGynecoObstetric) {
            if (isset($formGynecoObstetric['id']) && isset($formGynecoObstetric['updateTypeGynecoObstetric']) && $formGynecoObstetric['updateTypeGynecoObstetric'] != GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE) {
                $modelGynecoObstetric = GynecoObstetric::findOne(['id' => $formGynecoObstetric['id'], 'patient_id' => $model->id]);
                $modelGynecoObstetric->setScenario(GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[$i] = $modelGynecoObstetric;
            } else {
                $modelGynecoObstetric = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[] = $modelGynecoObstetric;
            }
        }

        foreach ($formDiets as $i => $formDiet) {
            if (isset($formDiet['id']) && isset($formDiet['updateTypeDiet']) && $formDiet['updateTypeDiet'] != Diet::UPDATE_TYPE_DIET_CREATE) {
                $modelDiet = Diet::findOne(['id' => $formDiet['id'], 'patient_id' => $model->id]);
                $modelDiet->setScenario(Diet::SCENARIO_DIET_BATCH_UPDATE);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[$i] = $modelDiet;
            } else {
                $modelDiet = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[] = $modelDiet;
            }
        }

        foreach ($formAddictions as $i => $formAddiction) {
            if (isset($formAddiction['id']) && isset($formAddiction['updateTypeAddiction']) && $formAddiction['updateTypeAddiction'] != Addiction::UPDATE_TYPE_ADDICTION_CREATE) {
                $modelAddiction = Addiction::findOne(['id' => $formAddiction['id'], 'patient_id' => $model->id]);
                $modelAddiction->setScenario(Addiction::SCENARIO_ADDICTION_BATCH_UPDATE);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[$i] = $modelAddiction;
            } else {
                $modelAddiction = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[] = $modelAddiction;
            }
        }

        foreach ($formSystems as $i => $formSystem) {
            if (isset($formSystem['id']) && isset($formSystem['updateTypeSystem']) && $formSystem['updateTypeSystem'] != System::UPDATE_TYPE_SYSTEM_CREATE) {
                $modelSystem = System::findOne(['id' => $formSystem['id'], 'patient_id' => $model->id]);
                $modelSystem->setScenario(System::SCENARIO_SYSTEM_BATCH_UPDATE);
                $modelSystem->setAttributes($formSystem);
                $modelSystems[$i] = $modelSystem;
            } else {
                $modelSystem = new System(['scenario' => System::SCENARIO_SYSTEM_BATCH_UPDATE]);
                $modelSystem->setAttributes($formSystem);
                $modelSystems[] = $modelSystem;
            }
        }

        foreach ($formPhysicals as $i => $formPhysical) {
            if (isset($formPhysical['id']) && isset($formPhysical['updateTypePhysical']) && $formPhysical['updateTypePhysical'] != Physical::UPDATE_TYPE_PHYSICAL_CREATE) {
                $modelPhysical = Physical::findOne(['id' => $formPhysical['id'], 'patient_id' => $model->id]);
                $modelPhysical->setScenario(Physical::SCENARIO_PHYSICAL_BATCH_UPDATE);
                $modelPhysical->setAttributes($formPhysical);
                $modelPhysicals[$i] = $modelPhysical;
            } else {
                $modelPhysical = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
                $modelPhysical->setAttributes($formPhysical);
                $modelPhysicals[] = $modelPhysical;
            }
        }
        foreach ($formLabtests as $i => $formLabtest) {
            if (isset($formLabtest['id']) && isset($formLabtest['updateTypeLabtest']) && $formLabtest['updateTypeLabtest'] != Labtest::UPDATE_TYPE_LABTEST_CREATE) {
                $modelLabtest = Labtest::findOne(['id' => $formLabtest['id'], 'patient_id' => $model->id]);
                $modelLabtest->setScenario(Labtest::SCENARIO_LABTEST_BATCH_UPDATE);
                $modelLabtest->setAttributes($formLabtest);
                $modelLabtests[$i] = $modelLabtest;
            } else {
                $modelLabtest = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
                $modelLabtest->setAttributes($formLabtest);
                $modelLabtests[] = $modelLabtest;
            }
        }

        //handling if the addRows buttons has been pressed

        if ((Yii::$app->request->post('addRow')=='true') ||
        (Yii::$app->request->post('addRowPersonal') == 'true') ||
        (Yii::$app->request->post('addRowPersonalHistory') == 'true') ||
        (Yii::$app->request->post('addRowQuirurgicalIntervention') == 'true') ||
        (Yii::$app->request->post('addRowHospitalization') == 'true') ||
        (Yii::$app->request->post('addRowGynecoObstetric') == 'true') ||
        (Yii::$app->request->post('addRowDiet') == 'true') ||
        (Yii::$app->request->post('addRowAddiction') == 'true') ||
        (Yii::$app->request->post('addRowSystem') == 'true') ||
        (Yii::$app->request->post('addRowPhysical') == 'true') ||
        (Yii::$app->request->post('addRowLabtest') == 'true')) {
            if (Yii::$app->request->post('addRow') == 'true') {
                $modelDetails[] = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowPersonal') == 'true') {
                $modelPersonals[] = new Personal(['scenario' => Personal::SCENARIO_PERSONAL_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowPersonalHistory') == 'true') {
                $modelPersonalHistorys[] = new PersonalHistory(['scenario' => PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowQuirurgicalIntervention') == 'true') {
                $modelQuirurgicalInterventions[] = new QuirurgicalIntervention(['scenario' => QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowHospitalization') == 'true') {
                $modelHospitalizations[] = new Hospitalization(['scenario' => Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowGynecoObstetric') == 'true') {
                $modelGynecoObstetrics[] = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowDiet') == 'true') {
                $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
            }

            if (Yii::$app->request->post('addRowAddiction') == 'true') {
                $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowSystem') == 'true') {
                $modelSystems[] = new System(['scenario' => System::SCENARIO_SYSTEM_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowPhysical') == 'true') {
                $modelPhysicals[] = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
            }
            if (Yii::$app->request->post('addRowLabtest') == 'true') {
                $modelLabtests[] = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
            }
          //handling the nutriotional_state update button
          if (Yii::$app->request->post('updateNutritional') == 'true') {
              $model->load(Yii::$app->request->post());
              // $modelDetails[] = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
          }
            return $this->render('update', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
            'modelLabtests' => $modelLabtests,
        ]);
        } else {
        }


        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();

                foreach ($modelDetails as $modelDetail) {
                    //details that has been flagged for deletion will be deleted
                                        if ($modelDetail->updateType == Parental::UPDATE_TYPE_DELETE) {
                                            $modelDetail->delete();
                                        } else {
                                            //new or updated records go here
                                            $modelDetail->patient_id = $model->id;
                                            $modelDetail->save();
                                        }
                }
                foreach ($modelPersonals as $modelPersonal) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelPersonal->updateTypePersonal == Personal::UPDATE_TYPE_PERSONAL_DELETE) {
                                                $modelPersonal->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelPersonal->patient_id = $model->id;
                                                $modelPersonal->save();
                                            }
                }
                foreach ($modelPersonalHistorys as $modelPersonalHistory) {
                    if ($modelPersonalHistory->updateTypePersonalHistory == PersonalHistory::UPDATE_TYPE_PERSONAL_HISTORY_DELETE) {
                        $modelPersonalHistory->delete();
                    } else {
                        $modelPersonalHistory->patient_id = $model->id;
                        $modelPersonalHistory->save();
                    }
                }
                foreach ($modelQuirurgicalInterventions as $modelQuirurgicalIntervention) {
                    if ($modelQuirurgicalIntervention->updateTypeQuirurgicalIntervention == QuirurgicalIntervention::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_DELETE) {
                        $modelQuirurgicalIntervention->delete();
                    } else {
                        $modelQuirurgicalIntervention->patient_id = $model->id;
                        $modelQuirurgicalIntervention->save();
                    }
                }
                foreach ($modelHospitalizations as $modelHospitalization) {
                    if ($modelHospitalization->updateTypeHospitalization == Hospitalization::UPDATE_TYPE_HOSPITALIZATION_DELETE) {
                        $modelHospitalization->delete();
                    } else {
                        $modelHospitalization->patient_id = $model->id;
                        $modelHospitalization->save();
                    }
                }
                foreach ($modelGynecoObstetrics as $modelGynecoObstetric) {
                    if ($modelGynecoObstetric->updateTypeGynecoObstetric == GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE) {
                        $modelGynecoObstetric->delete();
                    } else {
                        $modelGynecoObstetric->patient_id = $model->id;
                        $modelGynecoObstetric->save();
                    }
                }
                foreach ($modelDiets as $modelDiet) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelDiet->updateTypeDiet == Diet::UPDATE_TYPE_DIET_DELETE) {
                                                $modelDiet->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelDiet->patient_id = $model->id;
                                                $modelDiet->save();
                                            }
                }
                foreach ($modelAddictions as $modelAddiction) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelAddiction->updateTypeAddiction == Addiction::UPDATE_TYPE_ADDICTION_DELETE) {
                                                $modelAddiction->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelAddiction->patient_id = $model->id;
                                                $modelAddiction->save();
                                            }
                }
                foreach ($modelSystems as $modelSystem) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelSystem->updateTypeSystem == System::UPDATE_TYPE_SYSTEM_DELETE) {
                                                $modelSystem->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelSystem->patient_id = $model->id;
                                                $modelSystem->save();
                                            }
                }
                foreach ($modelPhysicals as $modelPhysical) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelPhysical->updateTypePhysical == Physical::UPDATE_TYPE_PHYSICAL_DELETE) {
                                                $modelPhysical->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelPhysical->patient_id = $model->id;
                                                $modelPhysical->save();
                                            }
                }
                foreach ($modelLabtests as $modelLabtest) {
                    //details that has been flagged for deletion will be deleted
                                            if ($modelLabtest->updateTypeLabtest == Labtest::UPDATE_TYPE_LABTEST_DELETE) {
                                                $modelLabtest->delete();
                                            } else {
                                                //new or updated records go here
                                                $modelLabtest->patient_id = $model->id;
                                                $modelLabtest->save();
                                            }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
              'modelLabtests' => $modelLabtests,
        ]);
    }



    public function actionModify($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->parental;
        $modelPersonals = $model->personal;
        $modelPersonalHistorys = $model->personalHistory;
        $modelQuirurgicalInterventions = $model->quirurgicalIntervention;
        $modelHospitalizations = $model->hospitalization;
        $modelGynecoObstetrics = $model->gynecoObstetric;
        $modelDiets = $model->diet;
        $modelAddictions = $model->addiction;
        $modelSystems = $model->system;
        $modelPhysicals = $model->physical;
        $modelLabtests = $model->labtest;


        $formDetails = Yii::$app->request->post('Parental', []);
        $formPersonals = Yii::$app->request->post('Personal', []);
        $formPersonalHistorys = Yii::$app->request->post('PersonalHistory', []);
        $formQuirurgicalInterventions = Yii::$app->request->post('QuirurgicalIntervention', []);
        $formHospitalizations = Yii::$app->request->post('Hospitalization', []);
        $formGynecoObstetrics = Yii::$app->request->post('GynecoObstetric', []);
        $formDiets = Yii::$app->request->post('Diet', []);
        $formAddictions = Yii::$app->request->post('Addiction', []);
        $formSystems = Yii::$app->request->post('System', []);
        $formPhysicals = Yii::$app->request->post('Physical', []);
        $formLabtests = Yii::$app->request->post('Labtest', []);

        foreach ($formDetails as $i => $formDetail) {
            //loading the models if they are not new
                        if (isset($formDetail['id']) && isset($formDetail['updateType']) && $formDetail['updateType'] != Parental::UPDATE_TYPE_CREATE) {
                            //making sure that it is actually a child of the main model
                            $modelDetail = Parental::findOne(['id' => $formDetail['id'], 'patient_id' => $model->id]);
                            $modelDetail->setScenario(Parental::SCENARIO_BATCH_UPDATE);
                            $modelDetail->setAttributes($formDetail);
                            $modelDetails[$i] = $modelDetail;
                            //validate here if the modelDetail loaded is valid, and if it can be updated or deleted
                        } else {
                            $modelDetail = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
                            $modelDetail->setAttributes($formDetail);
                            $modelDetails[] = $modelDetail;
                        }
        }

        foreach ($formPersonals as $i => $formPersonal) {
            if (isset($formPersonal['id']) && isset($formPersonal['updateTypePersonal']) && $formPersonal['updateTypePersonal'] != Personal::UPDATE_TYPE_PERSONAL_CREATE) {
                $modelPersonal = Personal::findOne(['id' => $formPersonal['id'], 'patient_id' => $model->id]);
                $modelPersonal->setScenario(Personal::SCENARIO_PERSONAL_BATCH_UPDATE);
                $modelPersonal->setAttributes($formPersonal);
                $modelPersonals[$i] = $modelPersonal;
            } else {
                $modelPersonal = new Personal(['scenario' => Personal::SCENARIO_PERSONAL_BATCH_UPDATE]);
                $modelPersonal->setAttributes($formPersonal);
                $modelPersonals[] = $modelPersonal;
            }
        }

        foreach ($formPersonalHistorys as $i => $formPersonalHistory) {
            if (isset($formPersonalHistory['id']) && isset($formPersonalHistory['updateTypePersonalHistory']) && $formPersonalHistory['updateTypePersonalHistory'] != PersonalHistory::UPDATE_TYPE_PERSONAL_HISTORY_CREATE) {
                $modelPersonalHistory = PersonalHistory::findOne(['id' => $formPersonalHistory['id'], 'patient_id' => $model->id]);
                $modelPersonalHistory->setScenario(PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE);
                $modelPersonalHistory->setAttributes($formPersonalHistory);
                $modelPersonalHistorys[$i] = $modelPersonalHistory;
            } else {
                $modelPersonalHistory = new PersonalHistory(['scenario' => PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE]);
                $modelPersonalHistory->setAttributes($formPersonalHistory);
                $modelPersonalHistorys[] = $modelPersonalHistory;
            }
        }

        foreach ($formQuirurgicalInterventions as $i => $formQuirurgicalIntervention) {
            if (isset($formQuirurgicalIntervention['id']) && isset($formQuirurgicalIntervention['updateTypeQuirurgicalIntervention']) && $formQuirurgicalIntervention['updateTypeQuirurgicalIntervention'] != QuirurgicalIntervention::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_CREATE) {
                $modelQuirurgicalIntervention = QuirurgicalIntervention::findOne(['id' => $formQuirurgicalIntervention['id'], 'patient_id' => $model->id]);
                $modelQuirurgicalIntervention->setScenario(QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE);
                $modelQuirurgicalIntervention->setAttributes($formQuirurgicalIntervention);
                $modelQuirurgicalInterventions[$i] = $modelQuirurgicalIntervention;
            } else {
                $modelQuirurgicalIntervention = new QuirurgicalIntervention(['scenario' => QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE]);
                $modelQuirurgicalIntervention->setAttributes($formQuirurgicalIntervention);
                $modelQuirurgicalInterventions[] = $modelQuirurgicalIntervention;
            }
        }

        foreach ($formHospitalizations as $i => $formHospitalization) {
            if (isset($formHospitalization['id']) && isset($formHospitalization['updateTypeHospitalization']) && $formHospitalization['updateTypeHospitalization'] != Hospitalization::UPDATE_TYPE_HOSPITALIZATION_CREATE) {
                $modelHospitalization = Hospitalization::findOne(['id' => $formHospitalization['id'], 'patient_id' => $model->id]);
                $modelHospitalization->setScenario(Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE);
                $modelHospitalization->setAttributes($formHospitalization);
                $modelHospitalizations[$i] = $modelHospitalization;
            } else {
                $modelHospitalization = new Hospitalization(['scenario' => Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE]);
                $modelHospitalization->setAttributes($formHospitalization);
                $modelHospitalizations[] = $modelHospitalization;
            }
        }

        foreach ($formGynecoObstetrics as $i => $formGynecoObstetric) {
            if (isset($formGynecoObstetric['id']) && isset($formGynecoObstetric['updateTypeGynecoObstetric']) && $formGynecoObstetric['updateTypeGynecoObstetric'] != GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_CREATE) {
                $modelGynecoObstetric = GynecoObstetric::findOne(['id' => $formGynecoObstetric['id'], 'patient_id' => $model->id]);
                $modelGynecoObstetric->setScenario(GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[$i] = $modelGynecoObstetric;
            } else {
                $modelGynecoObstetric = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
                $modelGynecoObstetric->setAttributes($formGynecoObstetric);
                $modelGynecoObstetrics[] = $modelGynecoObstetric;
            }
        }

        foreach ($formDiets as $i => $formDiet) {
            if (isset($formDiet['id']) && isset($formDiet['updateTypeDiet']) && $formDiet['updateTypeDiet'] != Diet::UPDATE_TYPE_DIET_CREATE) {
                $modelDiet = Diet::findOne(['id' => $formDiet['id'], 'patient_id' => $model->id]);
                $modelDiet->setScenario(Diet::SCENARIO_DIET_BATCH_UPDATE);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[$i] = $modelDiet;
            } else {
                $modelDiet = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
                $modelDiet->setAttributes($formDiet);
                $modelDiets[] = $modelDiet;
            }
        }

        foreach ($formAddictions as $i => $formAddiction) {
            if (isset($formAddiction['id']) && isset($formAddiction['updateTypeAddiction']) && $formAddiction['updateTypeAddiction'] != Addiction::UPDATE_TYPE_ADDICTION_CREATE) {
                $modelAddiction = Addiction::findOne(['id' => $formAddiction['id'], 'patient_id' => $model->id]);
                $modelAddiction->setScenario(Addiction::SCENARIO_ADDICTION_BATCH_UPDATE);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[$i] = $modelAddiction;
            } else {
                $modelAddiction = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
                $modelAddiction->setAttributes($formAddiction);
                $modelAddictions[] = $modelAddiction;
            }
        }

        foreach ($formSystems as $i => $formSystem) {
            if (isset($formSystem['id']) && isset($formSystem['updateTypeSystem']) && $formSystem['updateTypeSystem'] != System::UPDATE_TYPE_SYSTEM_CREATE) {
                $modelSystem = System::findOne(['id' => $formSystem['id'], 'patient_id' => $model->id]);
                $modelSystem->setScenario(System::SCENARIO_SYSTEM_BATCH_UPDATE);
                $modelSystem->setAttributes($formSystem);
                $modelSystems[$i] = $modelSystem;
            } else {
                $modelSystem = new System(['scenario' => System::SCENARIO_SYSTEM_BATCH_UPDATE]);
                $modelSystem->setAttributes($formSystem);
                $modelSystems[] = $modelSystem;
            }
        }

        foreach ($formPhysicals as $i => $formPhysical) {
            if (isset($formPhysical['id']) && isset($formPhysical['updateTypePhysical']) && $formPhysical['updateTypePhysical'] != Physical::UPDATE_TYPE_PHYSICAL_CREATE) {
                $modelPhysical = Physical::findOne(['id' => $formPhysical['id'], 'patient_id' => $model->id]);
                $modelPhysical->setScenario(Physical::SCENARIO_PHYSICAL_BATCH_UPDATE);
                $modelPhysical->setAttributes($formPhysical);
                $modelPhysicals[$i] = $modelPhysical;
            } else {
                $modelPhysical = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
                $modelPhysical->setAttributes($formPhysical);
                $modelPhysicals[] = $modelPhysical;
            }
        }
        foreach ($formLabtests as $i => $formLabtest) {
            if (isset($formLabtest['id']) && isset($formLabtest['updateTypeLabtest']) && $formLabtest['updateTypeLabtest'] != Labtest::UPDATE_TYPE_LABTEST_CREATE) {
                $modelLabtest = Labtest::findOne(['id' => $formLabtest['id'], 'patient_id' => $model->id]);
                $modelLabtest->setScenario(Labtest::SCENARIO_LABTEST_BATCH_UPDATE);
                $modelLabtest->setAttributes($formLabtest);
                $modelLabtests[$i] = $modelLabtest;
            } else {
                $modelLabtest = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
                $modelLabtest->setAttributes($formLabtest);
                $modelLabtests[] = $modelLabtest;
            }
        }

              //handling if the addRows buttons has been pressed
              if ((Yii::$app->request->post('addRow')=='true') ||
              (Yii::$app->request->post('addRowPersonal') == 'true') ||
              (Yii::$app->request->post('addRowPersonalHistory') == 'true') ||
              (Yii::$app->request->post('addRowQuirurgicalIntervention') == 'true') ||
              (Yii::$app->request->post('addRowHospitalization') == 'true') ||
              (Yii::$app->request->post('addRowGynecoObstetric') == 'true') ||
              (Yii::$app->request->post('addRowDiet') == 'true') ||
              (Yii::$app->request->post('addRowAddiction') == 'true') ||
              (Yii::$app->request->post('addRowSystem') == 'true') ||
              (Yii::$app->request->post('addRowPhysical') == 'true') ||
              (Yii::$app->request->post('addRowLabtest') == 'true')) {
                  if (Yii::$app->request->post('addRow') == 'true') {
                      $modelDetails[] = new Parental(['scenario' => Parental::SCENARIO_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowPersonal') == 'true') {
                      $modelPersonals[] = new Personal(['scenario' => Personal::SCENARIO_PERSONAL_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowPersonalHistory') == 'true') {
                      $modelPersonalHistorys[] = new PersonalHistory(['scenario' => PersonalHistory::SCENARIO_PERSONAL_HISTORY_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowQuirurgicalIntervention') == 'true') {
                      $modelQuirurgicalInterventions[] = new QuirurgicalIntervention(['scenario' => QuirurgicalIntervention::SCENARIO_QUIRURGICAL_INTERVENTION_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowHospitalization') == 'true') {
                      $modelHospitalizations[] = new Hospitalization(['scenario' => Hospitalization::SCENARIO_HOSPITALIZATION_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowGynecoObstetric') == 'true') {
                      $modelGynecoObstetrics[] = new GynecoObstetric(['scenario' => GynecoObstetric::SCENARIO_GYNECO_OBSTETRIC_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowDiet') == 'true') {
                      $modelDiets[] = new Diet(['scenario' => Diet::SCENARIO_DIET_BATCH_UPDATE]);
                  }

                  if (Yii::$app->request->post('addRowAddiction') == 'true') {
                      $modelAddictions[] = new Addiction(['scenario' => Addiction::SCENARIO_ADDICTION_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowSystem') == 'true') {
                      $modelSystems[] = new System(['scenario' => System::SCENARIO_SYSTEM_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowPhysical') == 'true') {
                      $modelPhysicals[] = new Physical(['scenario' => Physical::SCENARIO_PHYSICAL_BATCH_UPDATE]);
                  }
                  if (Yii::$app->request->post('addRowLabtest') == 'true') {
                      $modelLabtests[] = new Labtest(['scenario' => Labtest::SCENARIO_LABTEST_BATCH_UPDATE]);
                  }
                  return $this->render('update', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelPersonals' => $modelPersonals,
            'modelPersonalHistorys' => $modelPersonalHistorys,
            'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
            'modelHospitalizations' => $modelHospitalizations,
            'modelGynecoObstetrics' => $modelGynecoObstetrics,
            'modelDiets' => $modelDiets,
            'modelAddictions' => $modelAddictions,
            'modelSystems' => $modelSystems,
            'modelPhysicals' => $modelPhysicals,
            'modelLabtests' => $modelLabtests,
        ]);
              } else {
              }


        if ($model->load(Yii::$app->request->post())) {
            $photoName = $model->name;
            $model->photofile = UploadedFile::getInstance($model, 'photofile');
            if ($model->photofile !== null) {
                $model->photofile->saveAs('patients/'.$photoName.'.'.$model->photofile->extension);
                $model->photo = 'patients/'.$photoName.'.'.$model->photofile->extension;
            } else {
            }

            if (Model::validateMultiple($modelDetails) || ($modelPersonals) || ($modelDiets) || ($modelAddictions) || ($modelPersonalHistorys) || ($modelQuirurgicalInterventions) || ($modelHospitalizations) || ($modelGynecoObstetrics) || ($modelSystems) || ($modelPhysicals)  || ($modelLabtests) && $model->validate()) {
                $model->save();

                foreach ($modelDetails as $modelDetail) {
                    //details that has been flagged for deletion will be deleted
                                              if ($modelDetail->updateType == Parental::UPDATE_TYPE_DELETE) {
                                                  $modelDetail->delete();
                                              } else {
                                                  //new or updated records go here
                                                  $modelDetail->patient_id = $model->id;
                                                  $modelDetail->save();
                                              }
                }
                foreach ($modelPersonals as $modelPersonal) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelPersonal->updateTypePersonal == Personal::UPDATE_TYPE_PERSONAL_DELETE) {
                                                      $modelPersonal->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelPersonal->patient_id = $model->id;
                                                      $modelPersonal->save();
                                                  }
                }
                foreach ($modelPersonalHistorys as $modelPersonalHistory) {
                    if ($modelPersonalHistory->updateTypePersonalHistory == PersonalHistory::UPDATE_TYPE_PERSONAL_HISTORY_DELETE) {
                        $modelPersonalHistory->delete();
                    } else {
                        $modelPersonalHistory->patient_id = $model->id;
                        $modelPersonalHistory->save();
                    }
                }
                foreach ($modelQuirurgicalInterventions as $modelQuirurgicalIntervention) {
                    if ($modelQuirurgicalIntervention->updateTypeQuirurgicalIntervention == QuirurgicalIntervention::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_DELETE) {
                        $modelQuirurgicalIntervention->delete();
                    } else {
                        $modelQuirurgicalIntervention->patient_id = $model->id;
                        $modelQuirurgicalIntervention->save();
                    }
                }
                foreach ($modelHospitalizations as $modelHospitalization) {
                    if ($modelHospitalization->updateTypeHospitalization == Hospitalization::UPDATE_TYPE_HOSPITALIZATION_DELETE) {
                        $modelHospitalization->delete();
                    } else {
                        $modelHospitalization->patient_id = $model->id;
                        $modelHospitalization->save();
                    }
                }
                foreach ($modelGynecoObstetrics as $modelGynecoObstetric) {
                    if ($modelGynecoObstetric->updateTypeGynecoObstetric == GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE) {
                        $modelGynecoObstetric->delete();
                    } else {
                        $modelGynecoObstetric->patient_id = $model->id;
                        $modelGynecoObstetric->save();
                    }
                }
                foreach ($modelDiets as $modelDiet) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelDiet->updateTypeDiet == Diet::UPDATE_TYPE_DIET_DELETE) {
                                                      $modelDiet->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelDiet->patient_id = $model->id;
                                                      $modelDiet->save();
                                                  }
                }
                foreach ($modelAddictions as $modelAddiction) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelAddiction->updateTypeAddiction == Addiction::UPDATE_TYPE_ADDICTION_DELETE) {
                                                      $modelAddiction->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelAddiction->patient_id = $model->id;
                                                      $modelAddiction->save();
                                                  }
                }
                foreach ($modelSystems as $modelSystem) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelSystem->updateTypeSystem == System::UPDATE_TYPE_SYSTEM_DELETE) {
                                                      $modelSystem->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelSystem->patient_id = $model->id;
                                                      $modelSystem->save();
                                                  }
                }
                foreach ($modelPhysicals as $modelPhysical) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelPhysical->updateTypePhysical == Physical::UPDATE_TYPE_PHYSICAL_DELETE) {
                                                      $modelPhysical->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelPhysical->patient_id = $model->id;
                                                      $modelPhysical->save();
                                                  }
                }
                foreach ($modelLabtests as $modelLabtest) {
                    //details that has been flagged for deletion will be deleted
                                                  if ($modelLabtest->updateTypeLabtest == Labtest::UPDATE_TYPE_LABTEST_DELETE) {
                                                      $modelLabtest->delete();
                                                  } else {
                                                      //new or updated records go here
                                                      $modelLabtest->patient_id = $model->id;
                                                      $modelLabtest->save();
                                                  }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('modify', [
                  'model' => $model,
                  'modelDetails' => $modelDetails,
                  'modelPersonals' => $modelPersonals,
                  'modelPersonalHistorys' => $modelPersonalHistorys,
                  'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
                  'modelHospitalizations' => $modelHospitalizations,
                  'modelGynecoObstetrics' => $modelGynecoObstetrics,
                  'modelDiets' => $modelDiets,
                  'modelAddictions' => $modelAddictions,
                  'modelSystems' => $modelSystems,
                  'modelPhysicals' => $modelPhysicals,
                    'modelLabtests' => $modelLabtests,
              ]);
    }

    public function actionDelete($id)
    {
        $model2=$this->findModel($id);
        $model=$this->findModel($id)->delete();

        //  $model->delete();
                   foreach ($model2->parental as $modelDetail) {
                       $modelDetail->delete();
                   }
        foreach ($model2->personal as $modelPersonal) {
            $modelPersonal->delete();
        }
        foreach ($model2->personalHistory as $modelPersonalHistory) {
            $modelPersonalHistory->delete();
        }
        foreach ($model2->quirurgicalIntervention as $modelQuirurgicalIntervention) {
            $modelQuirurgicalIntervention->delete();
        }
        foreach ($model2->hospitalization as $modelHospitalization) {
            $modelHospitalization->delete();
        }
        foreach ($model2->gynecoObstetric as $modelGynecoObstetric) {
            $modelGynecoObstetric->delete();
        }

        foreach ($model2->diet as $modelDiet) {
            $modelDiet->delete();
        }
        foreach ($model2->addiction as $modelAddiction) {
            $modelAddiction->delete();
        }
        foreach ($model2->system as $modelSystem) {
            $modelSystem->delete();
        }
        foreach ($model2->physical as $modelPhysical) {
            $modelPhysical->delete();
        }
        foreach ($model2->labtest as $modelLabtest) {
            $modelLabtest->delete();
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
