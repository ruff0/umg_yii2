<?php
namespace app\controllers;

use app\models\PrescriptionDetail;
use app\models\PrescriptionDiagnostic;
use Yii;
use app\models\Prescription;
use app\models\PrescriptionSearch;
use app\models\Cie10;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// use kartik\mpdf\Pdf;
use mPDF;

class PrescriptionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new PrescriptionSearch();
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Prescription::find(),
        // ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Prescription();
        $modelDetails = [];
        $modelDiagnostics = [];

        $formDetails = Yii::$app->request->post('PrescriptionDetail', []);
        foreach ($formDetails as $i => $formDetail) {
            $modelDetail = new PrescriptionDetail(['scenario' => PrescriptionDetail::SCENARIO_BATCH_UPDATE]);
            $modelDetail->setAttributes($formDetail);
            $modelDetails[] = $modelDetail;
        }

        $formDiagnostics = Yii::$app->request->post('PrescriptionDiagnostic', []);
        foreach ($formDiagnostics as $i => $formDiagnostic) {
            $modelDiagnostic = new PrescriptionDiagnostic(['scenario' => PrescriptionDiagnostic::SCENARIO_DIAGNOSTIC_BATCH_UPDATE]);
            $modelDiagnostic->setAttributes($formDiagnostic);
            $modelDiagnostics[] = $modelDiagnostic;
        }

        //handling if the addRow button has been pressed
        if (Yii::$app->request->post('addRow') == 'true') {
            $model->load(Yii::$app->request->post());
            $modelDetails[] = new PrescriptionDetail(['scenario' => PrescriptionDetail::SCENARIO_BATCH_UPDATE]);
            return $this->render('create', [
                'model' => $model,
                'modelDetails' => $modelDetails,
                'modelDiagnostics' => $modelDiagnostics
            ]);
        }

        if (Yii::$app->request->post('addRowDiagnostic') == 'true') {
            $model->load(Yii::$app->request->post());
            $modelDiagnostics[] = new PrescriptionDiagnostic(['scenario' => PrescriptionDiagnostic::SCENARIO_DIAGNOSTIC_BATCH_UPDATE]);
            return $this->render('create', [
                'model' => $model,
                'modelDetails' => $modelDetails,
                'modelDiagnostics' => $modelDiagnostics

            ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelDiagnostics) && $model->validate()) {
                $model->save();
                foreach ($modelDetails as $modelDetail) {
                    $modelDetail->prescription_id = $model->id;
                    $modelDetail->save();
                }
                foreach ($modelDiagnostics as $modelDiagnostic) {
                    $modelDiagnostic->prescription_id = $model->id;
                    $modelDiagnostic->save();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelDiagnostics' => $modelDiagnostics
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->prescriptionDetails;
        $modelDiagnostics = $model->prescriptionDiagnostics;


        $formDetails = Yii::$app->request->post('PrescriptionDetail', []);
        $formDiagnostics = Yii::$app->request->post('PrescriptionDiagnostic', []);

        foreach ($formDetails as $i => $formDetail) {
            //loading the models if they are not new
            if (isset($formDetail['id']) && isset($formDetail['updateType']) && $formDetail['updateType'] != PrescriptionDetail::UPDATE_TYPE_CREATE) {
                //making sure that it is actually a child of the main model
                $modelDetail = PrescriptionDetail::findOne(['id' => $formDetail['id'], 'prescription_id' => $model->id]);
                $modelDetail->setScenario(PrescriptionDetail::SCENARIO_BATCH_UPDATE);
                $modelDetail->setAttributes($formDetail);
                $modelDetails[$i] = $modelDetail;
                //validate here if the modelDetail loaded is valid, and if it can be updated or deleted
            } else {
                $modelDetail = new PrescriptionDetail(['scenario' => PrescriptionDetail::SCENARIO_BATCH_UPDATE]);
                $modelDetail->setAttributes($formDetail);
                $modelDetails[] = $modelDetail;
            }
        }

        foreach ($formDiagnostics as $i => $formDiagnostic) {
            //loading the models if they are not new
            if (isset($formDiagnostic['id']) && isset($formDiagnostic['updateTypeDiagnostic']) && $formDiagnostic['updateTypeDiagnostic'] != PrescriptionDiagnostic::UPDATE_TYPE_DIAGNOSTIC_CREATE) {
                //making sure that it is actually a child of the main model
                $modelDiagnostic = PrescriptionDiagnostic::findOne(['id' => $formDiagnostic['id'], 'prescription_id' => $model->id]);
                $modelDiagnostic->setScenario(PrescriptionDiagnostic::SCENARIO_DIAGNOSTIC_BATCH_UPDATE);
                $modelDiagnostic->setAttributes($formDiagnostic);
                $modelDiagnostics[$i] = $modelDiagnostic;
                //validate here if the modelDiagnostic loaded is valid, and if it can be updated or deleted
            } else {
                $modelDiagnostic = new PrescriptionDiagnostic(['scenario' => PrescriptionDiagnostic::SCENARIO_DIAGNOSTIC_BATCH_UPDATE]);
                $modelDiagnostic->setAttributes($formDiagnostic);
                $modelDiagnostics[] = $modelDiagnostic;
            }
        }
        //handling if the addRow button has been pressed
        if (Yii::$app->request->post('addRow') == 'true') {
            $modelDetails[] = new PrescriptionDetail(['scenario' => PrescriptionDetail::SCENARIO_BATCH_UPDATE]);
            return $this->render('update', [
                'model' => $model,
                'modelDetails' => $modelDetails,
                'modelDiagnostics' => $modelDiagnostics

            ]);
        }

        if (Yii::$app->request->post('addRowDiagnostic') == 'true') {
            $modelDiagnostics[] = new PrescriptionDiagnostic(['scenario' => PrescriptionDiagnostic::SCENARIO_DIAGNOSTIC_BATCH_UPDATE]);
            return $this->render('update', [
                'model' => $model,
                'modelDetails' => $modelDetails,
                'modelDiagnostics' => $modelDiagnostics

            ]);
        }
        if ($model->load(Yii::$app->request->post())) {
            if (Model::validateMultiple($modelDetails) || ($modelDiagnostics) && $model->validate()) {
                $model->save();
                foreach ($modelDetails as $modelDetail) {
                    //details that has been flagged for deletion will be deleted
                    if ($modelDetail->updateType == PrescriptionDetail::UPDATE_TYPE_DELETE) {
                        $modelDetail->delete();
                    } else {
                        //new or updated records go here
                        $modelDetail->prescription_id = $model->id;
                        $modelDetail->save();
                    }
                }


                foreach ($modelDiagnostics as $modelDiagnostic) {
                    //details that has been flagged for deletion will be deleted
                    if ($modelDiagnostic->updateTypeDiagnostic == PrescriptionDiagnostic::UPDATE_TYPE_DIAGNOSTIC_DELETE) {
                        $modelDiagnostic->delete();
                    } else {
                        //new or updated records go here
                        $modelDiagnostic->prescription_id = $model->id;
                        $modelDiagnostic->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelDetails' => $modelDetails,
            'modelDiagnostics' => $modelDiagnostics
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        foreach ($model->prescriptionDetails as $modelDetail) {
            $modelDetail->delete();
        }
        foreach ($model->prescriptionDiagnostics as $modelDiagnostic) {
            $modelDiagnostic->delete();
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Prescription::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPdf($id)
    {
        $model = $this->findModel($id);
        $mpdf = new mPDF('', 'letter', 9, 'dejavusans');
        $mpdf->SetDefaultBodyCSS('color', '#000000');
        $mpdf->AddPage('portrait');
        $mpdf->useDefaultCSS2 = true;
        $mpdf->SetTitle($model->patient);

        $mpdf->WriteHTML('
        <div style="margin-bottom:20px;">
        <table width="100%">
          <tr>
            <td class="col-md-2">

              <h1>Unidad Médica Guadalupana</h1>
            </td>
            <td>
            <div align="right">
            <img src="img/logo_umg.png" width="80px">
            </div>
            </td></tr></table>
        </div>
        <div style="margin-top:20px;"width="100%" >
          <div>
            <table width="100%">

              <tr><th align="left" width="10%">

              <div>Folio</div>

              </th>
              <th align="left"  width="50%">

              <div>Paciente</div>

              </th>
              <th align="left" width="40%" align="left">

              <div align="left">Doctor</div>

              </th>
              </tr>

            ');
        $mpdf->WriteHTML('<tr><td>');
        $mpdf->WriteHTML($model->id);
        $mpdf->WriteHTML('</td><td>');
        $mpdf->WriteHTML($model->patient);
        $mpdf->WriteHTML('</td><td align="left">');
        $mpdf->WriteHTML($model->doctor);
        $mpdf->WriteHTML('</td></tr>');
        $mpdf->WriteHTML('

            </table>
            </div>

            <table width="100%">
              <tr><th align="left" width="90%">
              <div>Diagnóstico</div>
              </th>
              <th align="left" width="10%">
                <div>CIE10</div>
              </th>
              </tr>');
        $mpdf->WriteHTML('<tr><td>');
        $mpdf->WriteHTML($model->diagnosis);
        $mpdf->WriteHTML('</td><td>');
        $mpdf->WriteHTML($model->cie10_code);
        $mpdf->WriteHTML('</td></tr></table><table>
        <tr><th align="left">Anotaciones</th></tr>
      ');
        $mpdf->WriteHTML('<tr><td>');
        $mpdf->WriteHTML($model->note);
        $mpdf->WriteHTML('</td></tr>
        </table>
          <table style="margin-top:10px;" width="100%">
          <tr>
          <th align="left">Cantidad</th>
          <th align="left">Medicamento</th>
          <th align="left">Tomar</th>
          <th align="left">Cada</th>
          <th align="left">Durante</th>
          <th align="left">Indicación</th>
          </tr>
          <p>&nbsp;</p>');
        foreach ($model->prescriptionDetails as $prescriptionDetail) {
            $mpdf->WriteHTML('<tr><td>');
            $mpdf->WriteHTML($prescriptionDetail->quantity);
            $mpdf->WriteHTML('</td><td>');
            $mpdf->WriteHTML($prescriptionDetail->medicine_name);
            $mpdf->WriteHTML('</td><td>');
            $mpdf->WriteHTML($prescriptionDetail->dosage);
            $mpdf->WriteHTML('</td><td>');
            $mpdf->WriteHTML($prescriptionDetail->frequency);
            $mpdf->WriteHTML('</td><td>');
            $mpdf->WriteHTML($prescriptionDetail->duration);
            $mpdf->WriteHTML('</td><td>');
            $mpdf->WriteHTML($prescriptionDetail->indication);
            $mpdf->WriteHTML('</td></tr>');
        }
        $mpdf->WriteHTML('</table></div></font>');
        $mpdf->Output();
        exit;
    }
}
