<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div align="center" class=" image">
              <img src="/img/logo_umg.png" class="img-circle" alt="User Image"/>
                <!-- <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/> -->
            </div>
            <!-- <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div> -->
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],

                    [
                        'label' => 'Citas',
                        'icon' => 'fa fa-calendar-plus-o',
                        'url' => '#',
                        'items' => [
                                  ['label' => 'Lista', 'icon' => 'fa fa-list-alt', 'url' => ['consultation/index'],],
                                  // ['label' => 'Calendario', 'icon' => 'fa fa-calendar', 'url' => ['consultation/calendar'],],
                                  ['label' => 'Nueva Cita', 'icon' => 'fa fa-pencil-square-o', 'url' => ['consultation/create'],],

                            ],
                    ],

                         ['label' => 'Recetas',   'icon' => 'fa fa-stethoscope',
                           'url' => '#',
                           'items' => [
                                     ['label' => 'Lista', 'icon' => 'fa fa-file-text', 'url' => ['prescription/index'],],
                                     ['label' => 'Nueva Receta', 'icon' => 'fa fa-pencil-square-o', 'url' => ['prescription/create'],],

                               ],
                       ],

                       ['label' => 'Pacientes',   'icon' => 'fa fa-group',
                         'url' => '#',
                         'items' => [
                                   ['label' => 'Lista', 'icon' => 'fa fa-list', 'url' => ['patient/index'],],
                                   ['label' => 'Nuevo Paciente', 'icon' => 'fa fa-user-plus', 'url' => ['patient/create'],],

                             ],
                     ],
                       ['label' => 'Medicamentos', 'icon' => 'fa fa-medkit', 'url' => ['drug/index']],
                       ['label' => 'CIE10', 'icon' => 'fa fa-heartbeat', 'url' => ['cie10/index']],
                       ['label' => 'Médicos', 'icon' => 'fa fa-user-md', 'url' => ['doctor/index']],

                        //  ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                ],
            ]
        ) ?>

    </section>

</aside>
