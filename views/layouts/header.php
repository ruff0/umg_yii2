<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">UMG</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
<!--
    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>




    </nav> -->
    <nav class="navbar navbar-static-top" role="navigation" >
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
      </a>
        <?php
            NavBar::begin([
                // 'brandLabel' => 'My Company',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Inicio', 'url' => ['/site/index']],
                    // ['label' => 'About', 'url' => ['/site/about']],
                    // ['label' => 'Contact', 'url' => ['/site/contact']],
                    ['label' => 'Citas', 'url' => ['/consultation/index']],
                    ['label' => 'Calendario', 'url' => ['/consultation/calendar']],
                    ['label' => 'Recetas', 'url' => ['/prescription/index']],
                    ['label' => 'Pacientes', 'url' => ['/patient/index']],
                    ['label' => 'Doctores', 'url' => ['/doctor/index']],
                    ['label' => 'Medicamentos', 'url' => ['/drug/index']],
                    ['label' => 'Diagnósticos', 'url' => ['/cie10/index']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => ['/site/login']] :
                        ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>
      </nav>
</header>
