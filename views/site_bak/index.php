<?php
/* @var $this yii\web\View */
$this->title = 'Unidad Médica Guadalupana';
?>
<div class="site-index">
<div align="center">

<div class="col-md-12">
  <img src="img/logo_umg.png" width="140px">
</div>
<div>
<img src="img/transparent.png" width="2px">
</div>

    <div class="body-content">

        <div class="row">

          <div class="col-md-12">
            <p><a class="btn btn-sm btn-primary" href="/index.php?r=prescription%2Fcreate">Nueva Receta</a></nb></p>
            </div>

            <div class="col-md-12">
            <p>  <a class="btn btn-sm btn-info" href="/index.php?r=consultation%2Fcreate">Nueva Cita</a></nb></p>
            </div>

            <div class="col-md-12">
            <p>  <a class="btn btn-sm btn-success" href="/index.php?r=patient%2Fcreate">Nuevo Paciente</a></p>
          </div>
          <div class="col-md-12">
            <p>  <a class="btn btn-sm btn-danger" href="/index.php?r=drug%2Fcreate">Nuevo Medicamento</a></p>
          </div>
        </div>

    </div>
</div>
</div>
