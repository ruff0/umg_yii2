<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\mpdf\Pdf;
use app\models\Prescription;
use app\models\PrescriptionDiagnostic;
use app\models\Cie10;


$this->title ='Receta No. '. $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Recetas', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="prescription-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
<div align="right">
    <?= Html::a('<i class="fa fa-file"></i> PDF', ['pdf', 'id'=>$model->id], [
      'class'=>'btn btn-warning',
      'target'=>'_blank',
      'data-toggle'=>'tooltip',
      'title'=>'La Receta en PDF aparecerá en una ventana nueva.'
    ]);
    ?>
</div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'patient',
            'doctor',
            'date',
            'valid_until',
            // 'diagnosis',
            // 'cie10_code',
        ],
    ]) ?>
    <!-- <h2>Details</h2> -->
    <table class="prescription-details table">
        <tr>
            <!-- <th>ID</th> -->
            <th>Cantidad</th>
            <th>Medicamento</th>
            <th>Dósis</th>
            <th>Frecuéncia</th>
            <th>Duración</th>
            <th>Indicación</th>

        </tr>
        <?php foreach($model->prescriptionDetails as $prescriptionDetail) :?>
            <tr>
                <!-- <td><?= $prescriptionDetail->id ?></td> -->
                <td><?= $prescriptionDetail->quantity ?></td>
                <td><?= $prescriptionDetail->medicine_name ?></td>
                <td><?= $prescriptionDetail->dosage ?></td>
                <td><?= $prescriptionDetail->frequency ?></td>
                <td><?= $prescriptionDetail->duration ?></td>
                <td><?= $prescriptionDetail->indication ?></td>

            </tr>
        <?php endforeach; ?>
    </table>


    <table class="prescription-diganostics table">
        <tr>
            <!-- <th>ID</th> -->
            <th>Diagnóstico</th>
            <th>Código CIE10</th>

        </tr>
        <?php foreach($model->prescriptionDiagnostics as $prescriptionDiagnostic) :?>
            <tr>
                <!-- <td><?= $prescriptionDiagnostic->id ?></td> -->
                <td><?= $prescriptionDiagnostic->diagnostic ?></td>

                <?php
        
                if ($prescriptionDiagnostic->cie10_code == 'nocode')
                {
                    $code = Cie10::find()->select('id10')->where(['dec10' => $prescriptionDiagnostic->diagnostic])->one();
                    if ($code !== null){

                      $nicecode = $code->id10;
                      echo "<td>";
                      echo $nicecode;
                      echo "</td>";
                    }
                    else {
                      $nicecode = null;
                    }
                }
               else{
                    echo "<td>";
                    echo $prescriptionDiagnostic->cie10_code;
                    echo "</td>";
                }

                ?>

            </tr>
        <?php endforeach; ?>
    </table>

    <div align="right">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary ']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Borrar ésta Receta?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
      </div>
</div>
