<?php
use app\models\Patient;
use app\models\Doctor;
use app\models\Cie10;
use app\models\Prescription;
use app\models\PrescriptionDetail;
use app\models\PrescriptionDiagnostic;
use app\models\Allergic;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use yii\db\Query;
use app\models\Drug;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
// use kartik\grid\GridView;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

?>
<?php $this->registerJs("
    $('.delete-button').click(function() {
        var detail = $(this).closest('.prescription-detail');
        var updateType = detail.find('.update-type');
        if (updateType.val() === " . json_encode(PrescriptionDetail::UPDATE_TYPE_UPDATE) . ") {
            updateType.val(" . json_encode(PrescriptionDetail::UPDATE_TYPE_DELETE) . ");
            detail.hide();
        } else {
            detail.remove();
        }

    });

    $('.delete-button-diagnostic').click(function() {
        var diagnostic = $(this).closest('.prescription-diagnostic');
        var updateTypeDiagnostic = diagnostic.find('.update-type-diagnostic');
        if (updateTypeDiagnostic.val() === " . json_encode(PrescriptionDiagnostic::UPDATE_TYPE_DIAGNOSTIC_UPDATE) . ") {
            updateTypeDiagnostic.val(" . json_encode(PrescriptionDiagnostic::UPDATE_TYPE_DIAGNOSTIC_DELETE) . ");
            diagnostic.hide();
        } else {
            diagnostic.remove();
        }

    });

");
?>

<div class="prescription-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false
    ]); ?>

    <?php
      $patname = Yii::$app->getRequest()->getQueryParam('patname');
      // echo $patname;
      $patid = Yii::$app->getRequest()->getQueryParam('patid');
      // echo $patid;
    ?>

    <?php $pmod = $model->patient;
        $pat = Patient::find()->where(['name'=>$pmod])->one();
        $pid = $pat['id'];
        if (($patid == null)||($patid == false)) {
            $allergicProvider = new ActiveDataProvider([
            'query' => Allergic::find()->where(['patient_id'=> $pid]),
            'sort' => false
          ]);
        } else {
            $allergicProvider = new ActiveDataProvider([
              'query' => Allergic::find()->where(['patient_id'=> $patid]),
              'sort' => false
            ]
          );
        }
    ?>

    <?php
    $patientData = ArrayHelper::map(Patient::find()->all(), 'id', 'name')
    ?>
        <div class="col-md-6">
            <?php
            if (($patname == null)||($patname == false)) {
                echo $form->field($model, 'patient')->label('Paciente')->widget(TypeaheadBasic::classname(), [
                'data' => $patientData,
                'scrollable' => true,
                'options' => [  'placeholder' => 'Paciente'],
                'pluginOptions' => ['highlight'=>true],
              ]);
            } else {
                echo $form->field($model, 'patient')->label('Paciente')->widget(TypeaheadBasic::classname(), [
              'data' => $patientData,
              'scrollable' => true,
              'options' => [ 'readOnly' => true, 'placeholder' => 'Paciente', 'value' => $patname],
              'pluginOptions' => ['highlight'=>true],
            ]);
            }
            ?>
        </div>

        <?php
        $doctorData = ArrayHelper::map(Doctor::find()->all(), 'id', 'name')
        ?>
        <div class="col-md-6">
            <?= $form->field($model, 'doctor')->label('Médico')->widget(TypeaheadBasic::classname(), [
              'data' => $doctorData,
              'scrollable' => true,
              'options' => ['placeholder' => 'Médico'],
              'pluginOptions' => ['highlight'=>true],
            ]);
            ?>
        </div>


        <?php
        $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10');
        ?>

<!--
<div class="col-md-3">

  <?= $form->field($model, 'date')->label('Fecha')->widget(DatePicker::className(), [
      'attribute' => 'date',
      'model'=>$model,
          'template' => '{addon}{input}',
      // 'form' => $form, // best for correct client validation
      'language' => 'es',

      'clientOptions' => [
          'autoclose' => true,
          'format' => 'dd-M-yyyy'
      ]
  ]);?>
  </div> -->

  <div class="col-md-3">
      <?= $form->field($model, 'valid_until')->label('Caducidad')->widget(DatePicker::className(), [
          'attribute' => 'valid_until',
          'model'=>$model,
              'template' => '{addon}{input}',
          // 'form' => $form, // best for correct client validation
          'language' => 'es',

          'clientOptions' => [
              'autoclose' => true,
              'format' => 'yyyy-mm-dd'
          ]
      ]);?>
  </div>

  <div class="col-md-9">
    <?= $form->field($model, 'note')->label('Anotaciones')->textInput(['maxlength' => true]) ?>
  </div>


  <div class="col-md-9">
    <?php
    $count = $allergicProvider->getCount();
    if ($count !== 0) {
        echo yii\grid\GridView::widget([
        'dataProvider' => $allergicProvider,
        'layout' => "{pager}\n{items}",
        'columns'=> [['attribute' => 'allergy', 'label' => 'Alérgia(s)']],
      ]);
    } else {
    }
    ?>
  </div>


  <?php Pjax::begin(); ?>

  <?php foreach ($modelDiagnostics as $i => $modelDiagnostic) : ?>
      <div class="row prescription-diagnostic prescription-diagnostic-<?= $i ?>">
          <div class="col-md-12">
              <?= Html::activeHiddenInput($modelDiagnostic, "[$i]id") ?>
              <?= Html::activeHiddenInput($modelDiagnostic, "[$i]updateTypeDiagnostic", ['class' => 'update-type-diagnostic']) ?>
              <table>
                  <tr class="col-md-14">
                    <td class="col-md-12">
                        <?=  $form->field($modelDiagnostic, "[$i]diagnostic")->label('Diagnóstico')->widget(TypeaheadBasic::classname(), [
                          'data'=>$cie10Data,
                          'scrollable' => true,
                          // 'attribute'=>'diagnosis',
                          'options' => ['placeholder' => 'Diagnóstico'],
                          'pluginOptions' => ['highlight'=>true],
                          'pluginEvents' => [
                                        'typeahead:selected' => 'function(){

                                          log("typeahead:closed");
                                          }',
                                        ],
                          ])?>
                      </td>
                      <td class="col-md-1">
                          <?= Html::button('x', ['class' => 'delete-button-diagnostic btn btn-danger', 'data-target' => "prescription-diagnostic-$i"]) ?>
                      </td>
                    </tr>

                          <?php $code = Cie10::find()->select('id10')->where(['dec10' => $modelDiagnostic->diagnostic])->one(); ?>
                          <?php if (isset($code->id10) == true){
                            $nicecode = $code->id10;
                          }
                          else{$nicecode = 'nocode';}
                          ?>
                  <tr class="col-md-14" style="display:none">
                    <td class="col-md-14" style="display:none">
                          <?= $form->field($modelDiagnostic, "[$i]cie10_code")->textInput(['maxlength' => true, 'value' => $nicecode ]) ?>
                    </td>
                  </tr>

              </table>

          </div>
      </div>
  <?php endforeach; ?>

    <?php foreach ($modelDetails as $i => $modelDetail) : ?>
      <hr>
        <div class="row prescription-detail prescription-detail-<?= $i ?>">
            <div class="col-md-14">
                <?= Html::activeHiddenInput($modelDetail, "[$i]id") ?>
                <?= Html::activeHiddenInput($modelDetail, "[$i]updateType", ['class' => 'update-type']) ?>
                <?php
                $drugData = ArrayHelper::map(Drug::find()->all(), 'id', 'nombre')
                ?>
                <table>

                    <tr class="col-md-12">
                      <td class="col-md-2">
                        <?= $form->field($modelDetail, "[$i]quantity")->label('Cantidad')->textInput(['maxlength' => 255]) ?>
                      </td>
                      <td class="col-md-10">
                        <?= $form->field($modelDetail, "[$i]medicine_name")->label('Medicamento')->widget(TypeaheadBasic::classname(), [
                          'data' => $drugData,
                          'scrollable' => true,
                          'options' => ['placeholder' => 'Medicamento'],
                          'pluginOptions' => ['highlight'=>true],
                        ]);
                        ?>
                      </td>
                    </tr>
                    <tr class="col-md-12">
                      <td class="col-md-2">
                        <?= $form->field($modelDetail, "[$i]dosage")->label('Tomar')->textInput(['maxlength' => 255]) ?>
                      </td>
                      <td class="col-md-2">
                        <?= $form->field($modelDetail, "[$i]frequency")->label('Cada')->textInput(['maxlength' => 255]) ?>
                      </td>
                      <td class="col-md-2">
                        <?= $form->field($modelDetail, "[$i]duration")->label('Durante')->textInput(['maxlength' => 255]) ?>
                      </td>
                      <td class="col-md-6">
                        <?= $form->field($modelDetail, "[$i]indication")->label('Indicación')->textInput(['maxlength' => 255]) ?>
                      </td>
                      <td class="col-md-1">
                        <?= Html::button('x', ['class' => 'delete-button btn btn-danger', 'data-target' => "prescription-detail-$i"]) ?>
                      </td>
                    </tr>
                </table>

            </div>
        </div>
    <?php endforeach; ?>


    <?php Pjax::end(); ?>
    <div class="form-group col-md-12">
      <div class="pull-left col-md-2">
        <?= Html::submitButton('<i class="fa fa-plus"></i> Agregar Medicamento', ['name' => 'addRow', 'value' => 'true', 'class' => 'btn btn-info']) ?>
      </div>
      <div class="col-md-2">
        <?= Html::submitButton('<i class="fa fa-plus"></i> Agregar Diagnóstico', ['name' => 'addRowDiagnostic', 'value' => 'true', 'class' => 'btn btn-primary']) ?>
      </div>
      <div class="pull-right">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-eyedropper"></i> Recetar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-info']) ?>
      </div>
  </div>


    <?php ActiveForm::end(); ?>

</div>
