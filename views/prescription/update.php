<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prescription */
/* @var $modelDetails app\models\PrescriptionDetail */

$this->title = 'Actualizar Receta: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="prescription-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelDetails' => $modelDetails,
        'modelDiagnostics' => $modelDiagnostics
        
    ]) ?>

</div>
