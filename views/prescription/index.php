<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use yii\widgets\Pjax;

$this->title = 'Recetas';
?>
<div class="prescription-index">
  <?php Pjax::begin(); ?>
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

      <div class="col-md-12">
        <div class="pull-left">
          <p>
          <?= Html::a('<i class="fa fa-eyedropper"></i> Nueva Receta', ['prescription/create', 'patname' => null, 'patid' => null], ['class' => 'btn btn-success', 'data' => [
                          'method' => 'post',
                    ],
          ]) ?>
        </p>
        </div>
      </div>
<?php
$diagnosisData = ArrayHelper::map(Cie10::find()->orderBy('id10')->asArray()->all(), 'id10', 'dec10');
?>
<?php $gridColumns = [
    // ['class' => 'yii\grid\SerialColumn'],
    [
        'class'=>'kartik\grid\SerialColumn',
        'contentOptions'=>['class'=>'kartik-sheet-style'],
        'width'=>'36px',
        'header'=>'',
        'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return Yii::$app->controller->renderPartial('_view', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true,
        'hiddenFromExport' => false,
    ],
    // [
    //   'pageSummary'=>true,
    //     'attribute'=>'diagnosis',
    //     'vAlign' => 'middle',
    //     'hAlign' => 'center',
    //     'width' => '80px',
    //     'filterType' =>GridView::FILTER_SELECT2,
    //     'filter' => $diagnosisData,
    //     'filterWidgetOptions'=> [
    //                 'pluginOptions' => [
    //                     'allowClear' => true,
    //         ],
    //     ],
    //     'filterInputOptions'=>['placeholder'=>'Diagnóstico'],
    // ],
    // 'id',
    'diagnosis',
    'patient',
    'doctor',
    'date',

    ['class' => 'yii\grid\ActionColumn'],
]?>

    <?= GridView::widget([
      'dataProvider'=> $dataProvider,
      'filterModel' => $searchModel,
      'columns' => $gridColumns,
      'headerRowOptions'=>['class'=>'kartik-sheet-style'],
      'filterRowOptions'=>['class'=>'kartik-sheet-style'],
      'resizableColumns'=>true,
      'pjax'=>true,
      'bordered'=>true,
      'striped'=>true,
      'condensed'=>true,
      'showPageSummary'=>false,
      'panel'=>[
          'type'=>GridView::TYPE_PRIMARY,
          'heading'=>false,
          ],
      'persistResize'=>true,
      'pjaxSettings'=>[
          'neverTimeout'=>true,
            ],
      'responsive'=>true,
      'hover'=>true,
      'toolbar'=> [
            '{export}',
            '{toggleData}',
            ],
      'export'=>[
            // 'fontAwesome' => true,
            'PDF' => [
                    'options' => [
                        // 'title' => $tituloexport,
                        //  'subject' => $tituloexport,
                        // 'author' => 'NYCSPrep CIMS',
                        // 'keywords' => 'NYCSPrep, preceptors, pdf'
                    ]
                ],
            ],
      'exportConfig' => [
            GridView::EXCEL => [
              'label' => 'Guardar en XLS',
              'showHeader' => true,
              // 'filename' => $tituloexport,


            ],
            GridView::PDF => [
              'label' => 'Guardar en PDF',
              'showHeader' => true,
              'showCaption' => true,
              'showPageSummary' => true,
              'showFooter' => true,
              // 'title' => $tituloexport,
              // 'options' => ['title' => $tituloexport],
              // 'config' => ['options' => ['title' => $tituloexport],],
              // 'filename' => $tituloexport,
            ],
      ]

    ]); ?>
    <?php Pjax::end(); ?>
</div>
