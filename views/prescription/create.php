<?php
use yii\helpers\Html;
$this->title = 'Crear Receta';
?>
<div class="form-group col-md-12">
  <?= Html::a('<i class="fa fa-chevron-left"></i>', ['prescription/index'], ['class' => 'btn btn-success', 'data' => [ ],]) ?>
</div>
<div class="prescription-create">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?= $this->render('_form', [
        'model' => $model,
        'modelDetails' => $modelDetails,
        'modelDiagnostics' => $modelDiagnostics
    ]) ?>
</div>
