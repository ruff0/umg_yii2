<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\mpdf\Pdf;
use app\models\Prescription;
?>
<div class="prescription-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'patient',
            // 'doctor',
            // 'date',
            'valid_until',
            'diagnosis',
            'cie10_code',
        ],
    ]) ?>
    <!-- <h2>Details</h2> -->
    <table class="prescription-details table">
        <tr>
            <!-- <th>ID</th> -->
            <th>Cantidad</th>
            <th>Medicamento</th>
            <th>Dósis</th>
            <th>Frecuéncia</th>
            <th>Duración</th>
            <th>Indicación</th>

        </tr>
        <?php foreach($model->prescriptionDetails as $prescriptionDetail) :?>
            <tr>
                <!-- <td><?= $prescriptionDetail->id ?></td> -->
                <td><?= $prescriptionDetail->quantity ?></td>
                <td><?= $prescriptionDetail->medicine_name ?></td>
                <td><?= $prescriptionDetail->dosage ?></td>
                <td><?= $prescriptionDetail->frequency ?></td>
                <td><?= $prescriptionDetail->duration ?></td>
                <td><?= $prescriptionDetail->indication ?></td>

            </tr>
        <?php endforeach; ?>
    </table>


</div>
