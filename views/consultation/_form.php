<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\widgets\Pjax;
use app\models\Patient;
use app\models\Doctor;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use kartik\typeahead\TypeaheadBasic;

/* @var $this yii\web\View */
/* @var $model app\models\Consultation */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(); ?>
<div class="consultation-form">
    <?php $form = ActiveForm::begin(); ?>
      <?php
      $patientData = ArrayHelper::map(Patient::find()->all(), 'id', 'name')
      ?>
      <div class="col-md-12">
          <?= $form->field($model, 'patient_name')->label('Paciente')->widget(TypeaheadBasic::classname(), [
            'data' => ArrayHelper::map(Patient::find()->all(), 'id', 'name'),
            'scrollable' => true,
            'options' => ['placeholder' => 'Paciente'],
            'pluginOptions' => ['highlight'=>true],
          ]);
          ?>
      </div>

    <?php
    $doctorData = ArrayHelper::map(Doctor::find()->all(), 'id', 'name')
    ?>
    <div class="col-md-6">
          <?= $form->field($model, 'doctor_name')->label('Médico')->widget(TypeaheadBasic::classname(), [
            'data' => $doctorData,
            'scrollable' => true,
            'options' => ['placeholder' => 'Médico'],
            'pluginOptions' => ['highlight'=>true],
          ]);
          ?>
    </div>
    <div class="col-md-6">
          <?= DateTimePicker::widget([
          'attribute' => 'date_time_in',
          'model' => $model,
          'value' => '08-Apr-2004 10:20 AM',
          'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
          'options' => ['placeholder' => 'Hora de Inicio'],
          'pluginOptions' => [
              'format' => 'yyyy-mm-dd HH:ii:ss',
              'showMeridian' => false,
              'autoclose' => true,
              'todayBtn' => true
              ]
          ]);?>
      </div>
      <div class="col-md-6">
            <?= DateTimePicker::widget([
            'attribute' => 'date_time_out',
            'model' => $model,
            'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
            'options' => ['placeholder' => 'Hora de Termino'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd HH:ii:ss',
                'showMeridian' => false,
                'autoclose' => true,
                'todayBtn' => true
                ]
            ]);?>
        </div>
&nbsp;
    <div class="col-md-12" align="right">
      <div class="form-group">
          <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>
