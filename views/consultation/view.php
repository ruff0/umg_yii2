<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Consultation */

$this->title = $model->patient_name;
// $this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultation-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'patient_name',
            // 'place',
            'date_time_in',
            'date_time_out',
            'doctor_name',
        ],
    ]) ?>
    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-remove"></i> Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Borrar ésta cita?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
