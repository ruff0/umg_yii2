<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;

$this->title = 'Citas';
?>
<div class="consultation-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
        <div class="pull-left"><p>
            <?= Html::a('Agregar Cita', ['create'], ['class' => 'btn btn-success']) ?>
        </p></div>
    </div>
    <?php $gridColumns = [
          // ['class' => 'yii\grid\SerialColumn'],

          // 'id',
          'patient_name',
          'doctor_name',
          // 'place',
          'date_time_in',
          'date_time_out',

          ['class' => 'yii\grid\ActionColumn'],
      ]?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'resizableColumns'=>true,
        'pjax'=>true,
        'bordered'=>true,
        'striped'=>true,
        'condensed'=>true,
        'showPageSummary'=>false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>false,
            ],
        'persistResize'=>false,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
              ],
        'responsive'=>true,
        'hover'=>true,
        'export'=>[
              // 'fontAwesome' => true,
              'PDF' => [
                      'options' => [
                          // 'title' => $tituloexport,
                          //  'subject' => $tituloexport,
                          // 'author' => 'NYCSPrep CIMS',
                          // 'keywords' => 'NYCSPrep, preceptors, pdf'
                      ]
                  ],
              ],
        'exportConfig' => [
              GridView::EXCEL => [
                'label' => 'Guardar en XLS',
                'showHeader' => true,
                // 'filename' => $tituloexport,


              ],
              GridView::PDF => [
                'label' => 'Guardar en PDF',
                'showHeader' => true,
                'showCaption' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                // 'title' => $tituloexport,
                // 'options' => ['title' => $tituloexport],
                // 'config' => ['options' => ['title' => $tituloexport],],
                // 'filename' => $tituloexport,
              ],
        ]

    ]); ?>
    <?php Pjax::end(); ?>
</div>
