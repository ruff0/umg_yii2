<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Parental;
use app\models\Patient;
use app\models\Personal;
use app\models\Diet;
use app\models\Addiction;
use app\models\System;
use app\models\Physical;
use app\models\Labtest;
use app\models\PersonalHistory;
use app\models\QuirurgicalIntervention;
use app\models\Hospitalization;
use app\models\GynecoObstetric;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\db\Expression;
?>
<?php $this->registerJs("

   $('.delete-button-physical').click(function() {
     var detailphysical = $(this).closest('.physical');
     var updateTypePhysical = detailphysical.find('.update-type-physical');
     if (updateTypePhysical.val() === " . json_encode(Physical::UPDATE_TYPE_PHYSICAL_UPDATE) . ") {
       updateTypePhysical.val(" . json_encode(Physical::UPDATE_TYPE_PHYSICAL_DELETE) . ");
       detailphysical.hide();
     } else {
     detailphysical.remove();
     }
   });


   $('.block-btn-diet').click(function() {
     $('.block-btn-diet').attr('disabled', true);
     $('.block-div-diet').attr('hidden', true);

        return true;
   });

");
?>
<?= DetailView::widget([
  'model' => $model,
  "options" => ['class' => 'bg-gray-light   table table-striped ', ],
  'attributes' => [
    // 'id',    // 'on_emergency',     // 'emergency_phone',     // 'modified_date',
    'name',
    // 'sex',    // 'birth_date',    // 'birth_place',    'age',
    // 'status',    // 'scholarity',    // 'work_area',    // 'profession',    // 'experience',
    // 'address',    // 'city',    // 'postal_code',    // 'phone',    // 'created_date',
  ],
  ]) ?>

<div class="patient-form">
  <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
      ]); ?>

          <div class="col-md-5" style="display:none">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>


                <?php if(  $model->physical == false ){
                    }
                    else {echo "<div><h4>Examen Físico</h4></div>";
                }?>
                <?php foreach ($modelPhysicals as $j => $modelPhysical) : ?>
                    <div class="row warning physical physical-<?= $j ?>">
                        <div class="col-md-11 warning">
                            <?= Html::activeHiddenInput($modelPhysical, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelPhysical, "[$j]updateTypePhysical", ['class' => 'update-type-physical']) ?>
                          <table>
                            <tr>
                              <?php
                              $birth_year = $model->birth_date;
                              $birth_year = substr($birth_year,0,4);
                              $now_year = new Expression('NOW()');
                                        $now_year = (new \yii\db\Query)->select($now_year)->scalar();
                                        $just_now = substr($now_year, 0, 10);
                                        $now_year = substr($now_year, 0, 4);
                              $birth_month = $model->birth_date;
                              $birth_month = substr($birth_month,5,2);
                              $now_month = new Expression('NOW()');
                                        $now_month = (new \yii\db\Query)->select($now_month)->scalar();
                                        $now_month =substr($now_month, 5, 2);
                              $month_dif= $now_month-$birth_month;
                              if ($birth_year == null)
                              {
                                $year_dif= 0;
                              }
                              else{
                                $year_dif= $now_year-$birth_year;
                              }
                              if ($month_dif >= 0 ){}
                                else{
                                $year_dif = $year_dif - 1;
                              }
                              ?>
                              <div class="col-md-12">
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical,"[$j]date")->textInput(['value'=>$just_now, 'readOnly' => true])?>
                                </div>
                                <div class="col-md-1">
                                  <?= $form->field($modelPhysical, "[$j]age")->textInput(['value'=>$year_dif, 'readOnly' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]waist")->textInput() ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]weight")->textInput() ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]height")->textInput() ?>
                                </div>

        <div class="form-group col-md-3">
          <?= Html::submitButton('Calcular Estado Nutricional', ['name' => 'updateNutritional', 'value' => 'true', 'class' => 'btn btn-info']) ?>
        </div>

                                <div class="col-md-3">
                                  <?php
                                  $m2 = $modelPhysical->height;
                                  $m2 = $m2*$m2;
                                  $a = $modelPhysical->weight;
                                  if ($m2 == 0){
                                     $nstate = 'No Calculado';

                                    // $bmi = 0;
                                  }
                                  else
                                  {
                                        $bmi = $a/$m2;
                                        // $nstate = "Aún no calculado";
                                      if (($bmi < 16)){
                                        $nstate = "Infrapeso: Delgadez severa";
                                      }
                                      if (($bmi >= 16) && ($bmi <= 16.99 )){
                                        $nstate = "Infrapeso: Delgadez moderada";
                                      }
                                      if (($bmi >= 17) && ($bmi <= 18.49 )){
                                        $nstate = "Infrapeso: Delgadez aceptable";
                                      }
                                      if (($bmi >= 18.5) && ($bmi <= 24.99 )){
                                        $nstate = "Peso Normal";
                                      }
                                      if (($bmi >= 25) && ($bmi <= 29.99 )){
                                        $nstate = "Sobrepeso";
                                      }
                                      if (($bmi >= 30) && ($bmi <= 34.99 )){
                                        $nstate = "Obeso";
                                      }
                                      if (($bmi >= 35) && ($bmi <= 39.99 )){
                                        $nstate = "Obeso: Tipo II";
                                      }
                                      if (($bmi >= 40)){
                                        $nstate = "Obeso: Tipo III";
                                      }
                                      else
                                      {
                                      }
                                }
                                  echo  $form->field($modelPhysical, "[$j]nutritional")->widget(Select2::classname(),
                                  [
                                    'data' => null,
                                    'language' => 'es',
                                    'options' => ['placeholder' => 'Estado Nutricional', 'value' => $nstate],
                                    'pluginOptions' => ['allowClear' => true],
                                  ]);
                                  ?>
                                </div>

                            </div>

                              <div class="col-md-12">
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]mmhg")->textInput() ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]pulse")->textInput() ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]temperature")->textInput() ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]attention")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]memory")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]ideation")->textInput(['maxlength' => true]) ?>
                                </div>
                              </div>
                              <div class="col-md-12">

                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]psychotechnic")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]sensibility")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]reflex")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]muscle")->textInput(['maxlength' => true]) ?>
                                </div>

                                <div class="col-md-2">
                                  <?= $form->field($modelPhysical, "[$j]force")->textInput(['maxlength' => true]) ?>
                                </div>
                                </div>
                                <div class="col-md-12">


                                  <div class="col-md-2">
                                    <?= $form->field($modelPhysical, "[$j]eye_condition")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Agudez Visual'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelPhysical, "[$j]eye_left")->textInput() ?>
                                  </div>

                                  <div class="col-md-2">
                                    <?= $form->field($modelPhysical, "[$j]eye_right")->textInput() ?>
                                  </div>

                                  <div class="col-md-2">
                                    <?= $form->field($modelPhysical, "[$j]hear_condition")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Agudez Auditiva'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>

                                  <div class="col-md-2">
                                    <?= $form->field($modelPhysical, "[$j]hear_left")->textInput() ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelPhysical, "[$j]hear_right")->textInput() ?>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]breath")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Respiratorio'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>

                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]vascular")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Cardiovascular'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>

                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]skin")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Dérmico'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>

                                  <div class="col-md-3">

                                    <?= $form->field($modelPhysical, "[$j]digestive")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Digestivo'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]genital")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Genito-Urinario'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>
                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]neurologic")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Neurológico'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>
                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]endocrinologic")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Endocrinológico'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>
                                  <div class="col-md-3">
                                    <?= $form->field($modelPhysical, "[$j]psychosocial")->widget(Select2::classname(),
                                    [
                                      'data' => null,
                                      'language' => 'es',
                                      'options' => ['placeholder' => 'Psicosocial'],
                                      'pluginOptions' => ['allowClear' => true],
                                    ]);
                                    ?>
                                  </div>
                                </div>


                              </tr>
                              </table>
                        </div>
                        <div class="col-md-1 warning">
                            <?= Html::button('x', ['class' => 'delete-button-physical btn btn-danger', 'data-target' => "physical-$j"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>

                <div class="form-group col-md-18">


                    <div class="form-group col-md-2">
                      <?= Html::submitButton('Agregar Examen Físico', ['name' => 'addRowPhysicalAlone', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                    </div>
                  </div>

    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
  </div>
