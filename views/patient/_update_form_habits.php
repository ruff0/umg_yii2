<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Diet;
use app\models\Addiction;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\db\Expression;

?>
<?php $this->registerJs("

   $('.delete-button-diet').click(function() {
     var detaildiet = $(this).closest('.diet');
     var updateTypeDiet = detaildiet.find('.update-type-diet');
     if (updateTypeDiet.val() === " . json_encode(Diet::UPDATE_TYPE_DIET_UPDATE) . ") {
       updateTypeDiet.val(" . json_encode(Diet::UPDATE_TYPE_DIET_DELETE) . ");
       detaildiet.hide();
     } else {
     detaildiet.remove();
     }
   });

   $('.delete-button-addiction').click(function() {
     var detailaddiction = $(this).closest('.addiction');
     var updateTypeAddiction = detailaddiction.find('.update-type-addiction');
     if (updateTypeAddiction.val() === " . json_encode(Addiction::UPDATE_TYPE_ADDICTION_UPDATE) . ") {
       updateTypeAddiction.val(" . json_encode(Addiction::UPDATE_TYPE_ADDICTION_DELETE) . ");
       detailaddiction.hide();
     } else {
     detailaddiction.remove();
     }
   });
");
?>
<?= DetailView::widget([
  'model' => $model,
  "options" => ['class' => 'bg-gray-light   table table-striped ', ],
  'attributes' => [
    // 'id',    // 'on_emergency',     // 'emergency_phone',     // 'modified_date',
    'name',
    // 'sex',    // 'birth_date',    // 'birth_place',    'age',
    // 'status',    // 'scholarity',    // 'work_area',    // 'profession',    // 'experience',
    // 'address',    // 'city',    // 'postal_code',    // 'phone',    // 'created_date',
  ],
  ]) ?>

<div class="patient-form">
  <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin(['enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
      ]); ?>

          <div class="col-md-5" style="display:none">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>

                <?php if ($model->diet == false) {
      } else {
          echo "<div><h4>Hábitos Dietéticos</h4></div>";
      }?>
                <?php foreach ($modelDiets as $j => $modelDiet) : ?>
                    <div class="row  diet diet-<?= $j ?>">
                            <?= Html::activeHiddenInput($modelDiet, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelDiet, "[$j]updateTypeDiet", ['class' => 'update-type-diet']) ?>
                            <?php
                            $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10')
                            ?>
                                <div class="col-md-12">
                                  <div class="col-md-1">
                                    <?= $form->field($modelDiet, "[$j]breakfast")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                  <div class="col-md-8">
                                    <?= $form->field($modelDiet, "[$j]b_details")->label('Desayuno Detalles:')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="col-md-1 ">
                                    <?= $form->field($modelDiet, "[$j]lunch")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                  <div class="col-md-8">
                                    <?= $form->field($modelDiet, "[$j]l_details")->label('Comida Detalles:')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                </div>

                                <div class="col-md-12">

                                  <div class="col-md-1">
                                    <?= $form->field($modelDiet, "[$j]dinner")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                  <div class="col-md-8">
                                    <?= $form->field($modelDiet, "[$j]d_details")->label('Cena Detalles:')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="col-md-1">
                                    <?= $form->field($modelDiet, "[$j]snacks")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                  <div class="col-md-8">
                                    <?= $form->field($modelDiet, "[$j]s_details")->label('Tentempiés Detalles:')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-1 ">
                                    <?= Html::button('x', ['class' => 'delete-button-diet btn btn-danger', 'data-target' => "diet-$j"]) ?>
                                  </div>
                                </div>
                    </div>
                  </br>
                  </br>
                <?php endforeach; ?>

                <?php if ($model->addiction == false) {
                                              } else {
                                                  echo "<div>
                            <h4>Otros Hábitos</h4>

                          </div>";
                                              }?>
                <?php foreach ($modelAddictions as $j => $modelAddiction) : ?>
                    <div class="row  addiction addiction-<?= $j ?>">
                            <?= Html::activeHiddenInput($modelAddiction, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelAddiction, "[$j]updateTypeAddiction", ['class' => 'update-type-addiction']) ?>
                            <div class="col-md-12">
                                  <div class="col-md-1">
                                    <?= $form->field($modelAddiction, "[$j]alcohol")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                  <div class="col-md-1">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_age")->label('Desde que edad bebe?')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_detail")->label('Tipo de Alcohol')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-1">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_quantity")->label('Cantidad')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_frequency")->label('Frecuencia')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-1">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_climax")->label('Embriaguez?')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-1">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_yet")->label('Sigue Bebiendo?')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]alcohol_reason")->label('Mótivo para beber')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="col-md-1">
                                    <?= $form->field($modelAddiction, "[$j]cigar")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelAddiction, "[$j]cigar_age")->label('Desde que edad fuma cigarrillo?')->textInput(['maxlength' => 255]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelAddiction, "[$j]cigar_quantity")->label('Cantidad')->textInput(['maxlength' => 255]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelAddiction, "[$j]cigar_frequency")->label('Frecuencia')->textInput(['maxlength' => 255]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelAddiction, "[$j]cigar_yet")->label('Sigue Fumando?')->textInput(['maxlength' => 255]) ?>
                                </div>
                                <div class="col-md-2">
                                  <?= $form->field($modelAddiction, "[$j]cigar_reason")->label('Motivo por el que fuma?')->textInput(['maxlength' => 255]) ?>
                                </div>
                              </div>
                              <div class="col-md-12">

                                    <div class="col-md-2">
                                      <?= $form->field($modelAddiction, "[$j]coffee")->widget(SwitchInput::classname(), [
                                              'pluginOptions' => [
                                                            'onText' => 'Si',
                                                            'offText' => 'No',
                                                            // 'handleWidth'=> 120,
                                                            // 'onValue' => false,
                                                            // 'offValue' => true,
                                                            'onColor' => 'primary',
                                                            // 'offColor' => 'btn btn-sample',
                                                      ]
                                                ]);?>
                                    </div>
                                    <div class="col-md-2">
                                      <?= $form->field($modelAddiction, "[$j]coffee_age")->label('Desde que edad toma café?')->textInput(['maxlength' => 255]) ?>
                                    </div>
                                    <div class="col-md-2">
                                      <?= $form->field($modelAddiction, "[$j]coffee_quantity")->label('Cantidad de tazas')->textInput(['maxlength' => 255]) ?>
                                    </div>
                                    <div class="col-md-2">
                                      <?= $form->field($modelAddiction, "[$j]coffee_frequency")->label('Frecuencia')->textInput(['maxlength' => 255]) ?>
                                    </div>
                                    <div class="col-md-2">
                                      <?= $form->field($modelAddiction, "[$j]coffee_yet")->label('Sigue tomando café?')->textInput(['maxlength' => 255]) ?>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-2">

                                    <?= $form->field($modelAddiction, "[$j]painkiller")->widget(SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                          'onText' => 'Si',
                                                          'offText' => 'No',
                                                          // 'handleWidth'=> 120,
                                                          // 'onValue' => false,
                                                          // 'offValue' => true,
                                                          'onColor' => 'primary',
                                                          // 'offColor' => 'btn btn-sample',
                                                    ]
                                              ]);?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]painkiller_detail")->label('Tranquilizantes que usa')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]drug")->widget(SwitchInput::classname(), [
                                      'pluginOptions' => [
                                        'onText' => 'Si',
                                        'offText' => 'No',
                                        // 'handleWidth'=> 120,
                                        // 'onValue' => false,
                                        // 'offValue' => true,
                                        'onColor' => 'primary',
                                        // 'offColor' => 'btn btn-sample',
                                      ]
                                    ]);?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]drug_age")->label('Edad desde la que consume droga')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]drug_detail")->label('Drogas que usa')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]drug_detail")->label('Sigue consumiendo droga?')->textInput(['maxlength' => 255]) ?>
                                  </div>
                                  <div class="col-md-2">
                                    <?= $form->field($modelAddiction, "[$j]drug_reason")->label('Motivo por el que consuem droga?')->textInput(['maxlength' => 255]) ?>
                                  </div>

                                  <div class="col-md-1">
                                    <?= Html::button('x', ['class' => 'delete-button-addiction btn btn-danger', 'data-target' => "addiction-$j"]) ?>
                                  </div>
                                </div>
                        </div>
                <?php endforeach; ?>

                <!-- <div class="form-group col-md-18">
                    <div class="form-group   col-md-2">
                      <?= Html::submitButton('Hábitos Dietéticos', ['name' => 'addRowDiet', 'value' => 'true', 'class' => 'btn btn-info'])  ?>
                    </div>
                    <div class="form-group col-md-2">
                      <?= Html::submitButton('Adicciones', ['name' => 'addRowAddiction', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                    </div>
                </div> -->

    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
  </div>
