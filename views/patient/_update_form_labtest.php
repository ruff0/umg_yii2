<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Parental;
use app\models\Patient;
use app\models\Personal;
use app\models\Diet;
use app\models\Addiction;
use app\models\System;
use app\models\Physical;
use app\models\Labtest;
use app\models\PersonalHistory;
use app\models\QuirurgicalIntervention;
use app\models\Hospitalization;
use app\models\GynecoObstetric;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\db\Expression;
?>
<?php $this->registerJs("

   $('.delete-button-labtest').click(function() {
     var detaillabtest = $(this).closest('.labtest');
     var updateTypeLabtest = detaillabtest.find('.update-type-labtest');
     if (updateTypeLabtest.val() === " . json_encode(Labtest::UPDATE_TYPE_LABTEST_UPDATE) . ") {
       updateTypeLabtest.val(" . json_encode(Labtest::UPDATE_TYPE_LABTEST_DELETE) . ");
       detaillabtest.hide();
     } else {
     detaillabtest.remove();
     }
   });

   $('.block-btn-diet').click(function() {
     $('.block-btn-diet').attr('disabled', true);
     $('.block-div-diet').attr('hidden', true);

        return true;
   });

");
?>
<?= DetailView::widget([
  'model' => $model,
  "options" => ['class' => 'bg-gray-light   table table-striped ', ],
  'attributes' => [
    // 'id',    // 'on_emergency',     // 'emergency_phone',     // 'modified_date',
    'name',
    // 'sex',    // 'birth_date',    // 'birth_place',    'age',
    // 'status',    // 'scholarity',    // 'work_area',    // 'profession',    // 'experience',
    // 'address',    // 'city',    // 'postal_code',    // 'phone',    // 'created_date',
  ],
  ]) ?>

<div class="patient-form">
  <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
      ]); ?>

          <div class="col-md-5" style="display:none">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>


                <?php if(  $model->labtest == false ){
                          }
                          else {echo "<div>
                          <h4>Análisis Clínicos</h4>
                          </div>";
                      }?>
                <?php foreach ($modelLabtests as $j => $modelLabtest) : ?>
                    <div class="row warning labtest labtest-<?= $j ?>">
                        <div class="col-md-11 warning">
                            <?= Html::activeHiddenInput($modelLabtest, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelLabtest, "[$j]updateTypeLabtest", ['class' => 'update-type-labtest']) ?>
                              <table>
                                <tr>
                                  <td class="col-md-2 warning">
                                    <?= $form->field($modelLabtest, "[$j]analysis")->label('Análisis')->textInput(['maxlength' => 255]) ?>
                                  </td>
                                  <td class="col-md-2 warning">
                                    <?= $form->field($modelLabtest, "[$j]result")->label('Resultados')->textInput(['maxlength' => 255]) ?>
                                  </td>
                                  <td class="col-md-2 warning">
                                  <?= DatePicker::widget([
                                  'attribute' => "[$j]date",
                                  'model' => $modelLabtest,
                                  // 'value' => '08-Apr-2004 10:20 AM',
                                  'type' => DatePicker::TYPE_INPUT,
                                  'options' => [
                                    // 'placeholder' => 'Fecha de Nacimiento'
                                   ],
                                  'pluginOptions' => [
                                      'format' => 'yyyy-mm-dd',
                                      'showMeridian' => false,
                                      'autoclose' => true,
                                      'todayBtn' => false
                                      ]
                                  ]);?>
                                  </td>
                                </tr>
                              </table>
                        </div>
                        <div class="col-md-1 warning">
                            <?= Html::button('x', ['class' => 'delete-button-labtest btn btn-danger', 'data-target' => "labtest-$j"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="form-group col-md-18">

                    <div class="form-group col-md-3">
                      <?= Html::submitButton('Análisis Clínicos', ['name' => 'addRowLabtestAlone', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                    </div>
                  </div>
    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
  </div>
