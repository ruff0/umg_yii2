<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Patient;
?>

<div class="patient-view">
<div>
    <?= DetailView::widget([
        'model' => $model,
        "options" => ['class' => 'bg-gray-light table-responsive table-condensed thead-inverse table-hover table table-striped table-bordered  container-fluid', ],
        'attributes' => [
            'sex',
            'birth_date',
            'birth_place',
            'age',
            'status',
            'scholarity',
            'work_area',
            'profession',
            'experience',
            'address',
            'city',
            'postal_code',// 'phone',    'created_date',
        ],
    ]) ?>
  
  </div>
</div>
