<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\db\Expression;

?>

<div class="patient-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
        'options' => ['enctype'=> 'multipart/form-data']
      ]); ?>

<div id="patient-form">

    <div class="col-md-5">
      <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'sex')->widget(SwitchInput::classname(), [
              'pluginOptions' => [
                            'onText' => '<i class="fa fa-mars"></i> Hombre',
                            'offText' => '<i class="fa fa-venus"></i> Mujer',
                            // 'handleWidth'=> 120,
                            'onValue' => false,
                            'offValue' => true,
                            'onColor' => 'primary',
                            'offColor' => 'btn btn-sample',
                      ],
                      'pluginEvents' => [
                             'switchChange.bootstrapSwitch' => 'function(event, state) {
                                 if (state == 0 || state == false  || state == null){
                                     $("#'.Html::getInputId($model, 'sex').'").attr("value", 0);

                                 }
                                 else {
                                   $("#'.Html::getInputId($model, 'sex').'").attr("value", 1);
                                 }
                             }',
                      ],
                ]);?>
    </div>

    <div class="col-md-2">
      <b>Fecha de Nacimiento </b>
      <?= DatePicker::widget([
      'attribute' => 'birth_date',
      'model' => $model,
      // 'value' => '08-Apr-2004 10:20 AM',
      'type' => DatePicker::TYPE_INPUT,
      'options' => [
        // 'placeholder' => 'Fecha de Nacimiento'
       ],
      'pluginOptions' => [
          'format' => 'yyyy-mm-dd',
          'showMeridian' => false,
          'autoclose' => true,
          'todayBtn' => false
          ]
      ]);?>
    </div>

    <?php
    $birth_year = $model->birth_date;
    $birth_year = substr($birth_year,0,4);
    $now_year = new Expression('NOW()');
              $now_year = (new \yii\db\Query)->select($now_year)->scalar();
              $now_year =substr($now_year, 0, 4);
    $birth_month = $model->birth_date;
    $birth_month = substr($birth_month,5,2);
    $now_month = new Expression('NOW()');
              $now_month = (new \yii\db\Query)->select($now_month)->scalar();
              $now_month =substr($now_month, 5, 2);
    $month_dif= $now_month-$birth_month;
    if ($birth_year == null)
    {

      $year_dif= 0;
    }
    else{
      $year_dif= $now_year-$birth_year;
    }

    if ($month_dif >= 0 ){}
      else{
      $year_dif = $year_dif - 1;
    }
    ?>


    <div class="col-md-1">
      <?= $form->field($model, 'age')->textInput(['readOnly'=>true, 'value' => $year_dif]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5">
    <?= $form->field($model, 'photofile')->fileInput() ?>
  </div>
    <div class="col-md-2">
      <?= $form->field($model, 'scholarity')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'work_area')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'profession')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-1">
      <?= $form->field($model, 'experience')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'birth_place')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-3">
      <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4">
      <?= $form->field($model, 'on_emergency')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'emergency_phone')->textInput(['maxlength' => true]) ?>
    </div>
</div>


    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-floppy-disk"></i> Guardar' : '<i class="glyphicon glyphicon-floppy-disk"></i> Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </div>
