<?php
use yii\helpers\Html;
$this->title = 'Update Patient Allergies: ' . $model->name;
?>
<div class="patient-update-allergic">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?= $this->render('_update_form_allergic', [
        'model' => $model,

        'modelAllergics' => $modelAllergics,
    ]) ?>
</div>
