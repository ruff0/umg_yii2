<?php
use yii\helpers\Html;
$this->title = 'Paciente Nuevo';
?>
<div class="patient-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelDetails' => $modelDetails,
        'modelPersonals' => $modelPersonals,
        'modelPersonalHistorys' => $modelPersonalHistorys,
        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
        'modelHospitalizations' => $modelHospitalizations,
        'modelGynecoObstetrics' => $modelGynecoObstetrics,
        'modelDiets' => $modelDiets,
        'modelAddictions' => $modelAddictions,
        'modelSystems' => $modelSystems,
        'modelPhysicals' => $modelPhysicals,
        'modelLabtests' => $modelLabtests,
    ]) ?>
</div>
