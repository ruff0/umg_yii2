<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Patient;
use yii\widgets\ActiveForm;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\FileHelper;


$this->title = $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div>
  <?= Html::a('<i class="fa fa-chevron-left"></i>', ['patient/index', 'patname' => $model->name, 'patid' => $model->id], ['class' => 'btn btn-success', 'data' => [ ],]) ?>
</div>

<div class="patient-view">
<div class="com-md-12">
    <div class="pull-right">

  <?= Html::a('<i class="fa fa-eyedropper"></i> Recetar', ['prescription/create', 'patname' => $model->name, 'patid' => $model->id], ['class' => 'btn btn-info', 'data' => [
                //  'confirm' => "Recetar?",
            ],
]) ?>
  &nbsp;
      <?= Html::a('<i class="fa fa-pencil"></i> Modificar Datos Personales', ['modify', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        &nbsp;

      <?= Html::a('<i class="fa fa-remove"></i> Eliminar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Borrar éste Paciente?',
          'method' => 'post',
        ],
        ]) ?>

      </div>

    <div class="pull left">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
  </div>

<div>
  <?php
  $birth_year = $model->birth_date;
  $birth_year = substr($birth_year,0,4);
  $now_year = new Expression('NOW()');
            $now_year = (new \yii\db\Query)->select($now_year)->scalar();
            $just_now = substr($now_year, 0, 10);
            $now_year = substr($now_year, 0, 4);
  $birth_month = $model->birth_date;
  $birth_month = substr($birth_month,5,2);
  $now_month = new Expression('NOW()');
            $now_month = (new \yii\db\Query)->select($now_month)->scalar();
            $now_month =substr($now_month, 5, 2);
  $month_dif= $now_month-$birth_month;
  if ($birth_year == null)
  {

    $year_dif= 0;
  }
  else{
    $year_dif= $now_year-$birth_year;
  }

  if ($month_dif >= 0 ){}
    else{
    $year_dif = $year_dif - 1;
  }
  ?>

<?php if ($model->sex==0){
  $model->sex = "Mujer";
}
else{
  $model->sex = "Hombre";
}?>

<?php $model->age =  $year_dif;?>
    <?= DetailView::widget([
        'model' => $model,
        "options" => ['class' => 'bg-gray-light   table table-striped ', ],
        'attributes' => [
            // 'id',
              ['attribute'=>'photo',
              'label'=> 'Fotografía',
              'value'=> $model->photo,
              'format'=>['image',['width'=>100, 'height'=>100]]
            ],
            'on_emergency',            'emergency_phone',
            // 'modified_date',            // 'name',
            'sex',            'birth_date',            'birth_place',            'age',            'status',
            'scholarity',            'work_area',            'profession',            'experience',            'address',
            'city',            'postal_code',            'phone',            'created_date',
        ],
    ]) ?>
  </div>


  <?php if (($model->parental || $model->personal || $model->quirurgicalIntervention || $model->hospitalization || $model->personalHistory || $model->system) == null) {
    } else {
        echo "<hr>&nbsp;<div>
      <h3>Historia Clínica</h3>
      </div>&nbsp;";
    }?>


    <?php echo Yii::$app->controller->renderPartial('partials/parentals', ["model"=>$model], true); ?>

    <?php echo Yii::$app->controller->renderPartial('partials/personals', ["model"=>$model], true); ?>

    <?php echo Yii::$app->controller->renderPartial('partials/quirurgicals', ["model"=>$model], true); ?>

    <?php echo Yii::$app->controller->renderPartial('partials/hospitalizations', ["model"=>$model], true); ?>

    <?php echo Yii::$app->controller->renderPartial('partials/personalhistorys', ["model"=>$model], true); ?>

    <?php echo Yii::$app->controller->renderPartial('partials/systems', ["model"=>$model], true); ?>



    <div class="pull-right">
        <?php
        echo "<p>";
        echo Html::a('<i class="fa fa-pencil"></i> Actualizar Historia Clínica', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        echo "</p>"?>
    </div>


    <?php echo Yii::$app->controller->renderPartial('partials/gynecoobstetrics', ["model"=>$model], true); ?>


        <?php ActiveForm::begin(); ?>

        <div class="pull-left">
          <?php if ($model->gynecoObstetric == true) {
    } else {
        if ($model->sex ==0 || $model->sex == null) {
            echo Html::submitButton('<i class="fa fa-pencil"></i> Agregar Datos Gineco-Obstétricos', ['name' => 'addRowGynecoObstetricAlone', 'value' => 'true', 'class' => 'btn addRowGynecoObstetricAlone btn-sample']);
        }
    }
          ?>
        </div>
        <?php ActiveForm::end(); ?>

        <div class="pull-left">
          <?php
          if ($model->gynecoObstetric == true) {
              if ($model->sex == 0 || $model->sex == null) {
                  echo Html::a('<i class="fa fa-pencil"></i> Datos Gineco-Obstétricos', ['addgynecoobstetric', 'id' => $model->id], ['class' => 'btn btn-sample']);
              } else {
              }
          }
          ?>
        </div>
<div>




              <?php echo Yii::$app->controller->renderPartial('partials/allergies', ["model"=>$model], true); ?>


              <div class="pull-left">
                 <?php foreach ($model->system as $system) :?>
                <?php if ($model->system !== true) {
                    if (($system->allergy == false) || ($system->allergy == null)) {
                    } else {
                        echo Html::a('<i class="fa fa-pencil"></i> Alérgias', ['addallergics', 'id' => $model->id], ['class' => 'btn btn-warning']);
                    }
                }
                ?>
              <?php endforeach;?>
              </div>

              <?php echo Yii::$app->controller->renderPartial('partials/habits', ["model"=>$model], true); ?>

        <?php ActiveForm::begin(); ?>
        <?php
        if ($model->diet == true) {
        } else {
            if ($model->addiction == false) {


                echo "<p>";
                // echo Html::a('<i class="fa fa-pencil"></i> Agregar Hábitos', ['addhabits', 'id' => $model->id], ['class' => 'addRowHabits btn btn-warning']);

                echo Html::submitButton('<i class="fa fa-pencil"></i> Agregar Hábitos', ['name' => 'addRowHabits', 'value' => 'true', 'class' => 'btn addRowHabits btn-success']);
                echo "</p>";
            }
        }
        ?>
        <?php if ($model->diet == true) {
        } else {
            if ($model->addiction == true) {
                echo "<p>";
                echo Html::submitButton('<i class="fa fa-pencil"></i> Agregar Hábitos Dietéticos', ['name' => 'addRowDietAlone', 'value' => 'true', 'class' => 'btn addRowDietAlone btn-warning']);
                echo "</p>";
            }
        }?>

        <?php if ($model->addiction == true) {
        } else {
            if ($model->diet == true) {
                echo "<p>";
                echo Html::submitButton('<i class="fa fa-pencil"></i> Agregar Otros Hábitos', ['name' => 'addRowAddictionAlone', 'value' => 'true', 'class' => 'btn addRowAddictionAlone btn-info']);
                echo "</p>";
            }
        }?>
        <?php ActiveForm::end(); ?>

        <?php if (($model->diet == true) && ($model->addiction == true)) {
            echo "<p>";
            echo Html::a('<i class="fa fa-pencil"></i> Hábitos', ['addhabits', 'id' => $model->id], ['class' => 'btn btn-info']);
            echo "</p>";
        } else {
        }
        ?>
        <?php echo Yii::$app->controller->renderPartial('partials/physicals', ["model"=>$model], true); ?>


                <div class="pull-right">
                    <?= Html::a('<i class="fa fa-thermometer-3"></i> Agregar Exámen Físico', ['addphysical', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                </div>

        <?php echo Yii::$app->controller->renderPartial('partials/analysis', ["model"=>$model], true); ?>




    <div class="pull-left">
        <?= Html::a('<i class="fa fa-tint"></i> Agregar Análisis Clínico', ['addanalysis', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php echo Yii::$app->controller->renderPartial('partials/attachments', ["model"=>$model], true); ?>


      <div class="col-md-12">

        <div class="pull-right">
          <?= Html::a('<i class="fa fa-paperclip"></i> Anexos', ['patient/upload', 'id' => $model->id], ['class' => 'btn btn-warning', 'data' => [ ],]) ?>
        </div>
      </div>
</div>
