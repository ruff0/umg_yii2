
<div class="col-md-14">
  <?php if ($model->system == false) {
} else {
echo "<div>
      <h4>Apartatos y Sistemas</h4>
      </div>
      <table class='table table-striped' >
          <tr>
              <!-- <th>ID</th> -->
              <td>Visión Doble</td>
              <td>Visión Borrosa</td>
              <td>Halos de color</td>
              <td>Frecuencia</td>
              <td>Expuesto a Ruidos</td>
              <td>Zumbido</td>
              <td>Infección de Oídos</td>
              <td>Dolor en algun Oído</td>
              <td>Dificultad para oír</td>
              <td>Aparato Auricular</td>
              <td>Sangrados por la Nariz</td>
              <td>Resequedad en la Nariz</td>
              <td>No puede oler</td>
              <td>Gripas Frecuentes</td>
                <td>Alérgias</td>
          </tr>";
  ?>
  <?php foreach ($model->system as $system) :?>
              <tr>
                <td><?= $system->double_vision ?></td>
                <td><?= $system->blurred_vision ?></td>
                <td><?= $system->colored_vision ?></td>
                <td><?= $system->view_problem_frequency ?></td>
                <td><?= $system->noise_exposure ?></td>
                <td><?= $system->ear_pain ?></td>
                <td><?= $system->tinnitus ?></td>
                <td><?= $system->difficulty_hearing ?></td>
                <td><?= $system->ear_infection ?></td>
                <td><?= $system->hearing_aid ?></td>
                <td><?= $system->nosebleed ?></td>
                <td><?= $system->dry_nose ?></td>
                <td><?= $system->cant_smell ?></td>
                <td><?= $system->frequent_flu ?></td>
                <td><?= $system->allergy ?></td>

    <?php endforeach; ?>

            <?php echo
            "<tr>

              <td>Tos Persistente</td>
              <td>Flemas</td>
              <td>Falta de Aire</td>
              <td>Dolor de Pecho</td>
              <td>Molestia en el Pecho</td>
              <td>Presión Alta</td>
              <td>Soplos en el Corazón</td>
              <td>Várices</td>
              <td>Hinchazón</td>
              <td>Dolor al Orinar</td>
              <td>Expulsión de Calculos</td>
              <td>Sangre en la Orina</td>
              <td>Infecciónes Urinarias</td>
                <td>Sangrado Excesivo</td>
                <td>Manchas o Moretones</td>


          </tr>";
?>
      <?php foreach ($model->system as $system) :?>
          <tr>

            <td><?= $system->persistent_cough ?></td>
            <td><?= $system->phlegm ?></td>
            <td><?= $system->breathless ?></td>
            <td><?= $system->chest_pain ?></td>
            <td><?= $system->chest_nuisance ?></td>
            <td><?= $system->high_pressure ?></td>
            <td><?= $system->heartblow ?></td>
            <td><?= $system->varicose ?></td>
            <td><?= $system->swelling ?></td>
            <td><?= $system->pain_urinating ?></td>
            <td><?= $system->urine_count ?></td>
            <td><?= $system->urine_blood ?></td>
            <td><?= $system->urine_infection ?></td>
            <td><?= $system->excesive_bleeding ?></td>
              <td><?= $system->bruise ?></td>

          </tr>
            <?php endforeach; ?>


          <?php echo "<tr>

          <td>Anemia</td>
          <td>Acné</td>
          <td>Comezón</td>
          <td>Ulceración en pies</td>
            <td>Crecimiento o cambio de lunar</td>
            <td>Caída de Cabello</td>
            <td>Cambio de color en piel</td>
            <td>Problemas de uñas</td>
            <td>Gota</td>
            <td>Ciática</td>
            <td>Artritis</td>
            <td>Lumbalgia</td>
            <td>Parálisis</td>
            <td>Pie Plano</td>
            </tr>
          ";}?>
          <?php foreach ($model->system as $system) :?>
              <tr>

            <td><?= $system->anemia ?></td>
            <td><?= $system->acne ?></td>
            <td><?= $system->itch ?></td>
            <td><?= $system->feet_ulceration ?></td>
            <td><?= $system->mole_change ?></td>
            <td><?= $system->hair_loss ?></td>
            <td><?= $system->skin_color_change ?></td>
            <td><?= $system->nail_problems ?></td>
            <td><?= $system->gout ?></td>
            <td><?= $system->sciatica ?></td>
            <td><?= $system->arthritis ?></td>
            <td><?= $system->lumbar_desease ?></td>
            <td><?= $system->pain_paralysis ?></td>
            <td><?= $system->flatfoot ?></td>
          </tr>
      <?php endforeach; ?>
  </table>

</div>
