<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\FileHelper;
?>
<div class="col-md-14">

      <div class="col-md-12">
        <?php
        $path = 'patients/'.$model->id.'/';
        if (is_dir($path)) {

          echo "<div ><h3>Ánexos</h3></div>";

          $files = FileHelper::findFiles($path);

          if (isset($files[0])) {
            foreach ($files as $index => $file) {
              $nameFile = substr($file, strrpos($file, '/') + 1);
              echo Html::a($nameFile, Url::base().$path.$nameFile) . "<br/>" ;
            }
          } else {
            echo "No hay Ánexos.";
          }
        }
        else{}
          ?>
        </div>
</div>
