
<div class="col-md-14">


      <?php
      if ($model->gynecoObstetric == false) {
      } else {
          echo "<div>
      <h4>Datos Gineco-Obstétricos</h4>
      </div>
      <table class='table table-striped' >
            <tr>
                <!-- <th>ID</th> -->
                <th>Edad de Inicio de Menstruación</th>
                <th>Ritmo</th>
                <th>Método de Planicifación Familiar</th>
                <th>Fecha de Última Menstruación</th>
                <th>IVSA</th>
                <th>Último PAP</th>
                <th>Embarazo(s)</th>
                <th>Cesárea(s)</th>
                <th>Parto(s)</th>
                <th>Aborto(s)</th>
                <th>Mensturaciones Dolorosas</th>
            </tr>";
      }
      ?>
              <?php foreach ($model->gynecoObstetric as $gynecoObstetric) :?>
                  <tr>
                      <td><?= $gynecoObstetric->menstruation_age ?></td>
                      <td><?= $gynecoObstetric->menstruation_frequency ?></td>
                      <td><?= $gynecoObstetric->contraceptive ?></td>
                      <td><?= $gynecoObstetric->last_menstruation ?></td>
                      <td><?= $gynecoObstetric->ivsa_age ?></td>
                      <td><?= $gynecoObstetric->last_pap ?></td>
                      <td><?= $gynecoObstetric->pregnancy ?></td>
                      <td><?= $gynecoObstetric->caesarian ?></td>
                      <td><?= $gynecoObstetric->child_bearing ?></td>
                      <td><?= $gynecoObstetric->abortion ?></td>
                      <td><?= $gynecoObstetric->menstruation_pain ?></td>
                  </tr>
              <?php endforeach; ?>
          </table>

</div>
