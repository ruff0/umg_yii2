
<div class="col-md-14">

          <?php if (($model->physical) == null) {
          } else {
              echo "<hr><div><h3>Examenes Físicos</h3></div>";
          }?>
              <?php foreach ($model->physical as $physical) :?>
              <div><h4>Examen Físico <?= $physical->date ?></h4></div>
              <table class='table table-striped' >
                    <tr>
                      <th>Edad</th>
                      <td><?= $physical->age ?></td>
                      <th>Cintura</th>
                      <td><?= $physical->waist ?></td>
                      <th>Peso</th>
                      <td><?= $physical->weight ?></td>
                      <th>Estatura</th>
                      <td><?= $physical->height ?></td>
                      <th>Estado Nutricional</th>
                      <td><?= $physical->nutritional ?></td>
                      <th>T/A (mmHg)</th>
                      <td><?= $physical->mmhg ?></td>
                      <th>Pulso</th>
                      <td><?= $physical->pulse ?></td>
                      <th>Temperatura</th>
                      <td><?= $physical->temperature ?></td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                      <th>Atención</th>
                      <td><?= $physical->attention ?></td>
                      <th>Memoria</th>
                      <td><?= $physical->memory ?></td>
                      <th>Ideación</th>
                      <td><?= $physical->ideation ?></td>
                      <th>Psicotécnica</th>
                      <td><?= $physical->psychotechnic ?></td>
                      <th>Sensibilidad</th>
                      <td><?= $physical->sensibility ?></td>
                      <th>Reflejos</th>
                      <td><?= $physical->reflex ?></td>
                      <th>Tono Muscular</th>
                      <td><?= $physical->muscle ?></td>
                      <th>Fuerza</th>
                      <td><?= $physical->force ?></td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                      <th>Condición Visual</th>
                      <td><?= $physical->eye_condition ?></td>
                      <th>Ojo Izquierdo</th>
                      <td><?= $physical->eye_left ?></td>
                      <th>Ojo Derecho</th>
                      <td><?= $physical->eye_right ?></td>
                      <th>Condición Auditiva</th>
                      <td><?= $physical->hear_condition ?></td>
                      <th>Oído Izquierdo</th>
                      <td><?= $physical->hear_left ?></td>
                      <th>Oído Derecho</th>
                      <td><?= $physical->hear_right ?></td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                      <th>Respiratorio</th>
                      <td><?= $physical->breath ?></td>
                      <th>Cardiovascular</th>
                      <td><?= $physical->vascular ?></td>
                      <th>Piel</th>
                      <td><?= $physical->skin ?></td>
                      <th>Digestivo</th>
                      <td><?= $physical->digestive ?></td>
                      <th>Genito-Urinario</th>
                      <td><?= $physical->genital ?></td>
                      <th>Neurológico</th>
                      <td><?= $physical->neurologic ?></td>
                      <th>Endocrinológico</th>
                      <td><?= $physical->endocrinologic ?></td>
                      <th>Psicosocial</th>
                      <td><?= $physical->psychosocial ?></td>
                    </tr>
                    <tr>
                  </tr>
                </table>
              <?php endforeach; ?>

</div>
