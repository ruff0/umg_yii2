<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Parental;
use app\models\Patient;
use app\models\Personal;
use app\models\Diet;
use app\models\Addiction;
use app\models\System;
use app\models\Physical;
use app\models\Labtest;
use app\models\PersonalHistory;
use app\models\QuirurgicalIntervention;
use app\models\Hospitalization;
use app\models\GynecoObstetric;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\db\Expression;
?>
<?php $this->registerJs("
    $('.delete-button').click(function() {
        var detail = $(this).closest('.parental');
        var updateType = detail.find('.update-type');
        if (updateType.val() === " . json_encode(Parental::UPDATE_TYPE_UPDATE) . ") {
            //marking the row for deletion
            updateType.val(" . json_encode(Parental::UPDATE_TYPE_DELETE) . ");
            detail.hide();
        } else {
            //if the row is a new row, delete the row
            detail.remove();
        }
    });

    $('.delete-button-personal').click(function() {
      var detailpersonal = $(this).closest('.personal');
      var updateTypePersonal = detailpersonal.find('.update-type-personal');
      if (updateTypePersonal.val() === " . json_encode(Personal::UPDATE_TYPE_PERSONAL_UPDATE) . ") {
        updateTypePersonal.val(" . json_encode(Personal::UPDATE_TYPE_PERSONAL_DELETE) . ");
        detailpersonal.hide();
      } else {
      detailpersonal.remove();
      }
    });

    $('.delete-button-personalhistory').click(function() {
      var detailpersonalhistory = $(this).closest('.personalhistory');
      var updateTypePersonalHistory = detailpersonalhistory.find('.update-type-personalhistory');
      if (updateTypePersonalHistory.val() === " . json_encode(PersonalHistory::UPDATE_TYPE_PERSONAL_HISTORY_UPDATE) . ") {
        updateTypePersonalHistory.val(" . json_encode(PersonalHistory::UPDATE_TYPE_PERSONAL_HISTORY_DELETE) . ");
        detailpersonalhistory.hide();
      } else {
        detailpersonalhistory.remove();
      }
    });

    $('.delete-button-quirurgicalintervention').click(function() {
      var detailquirurgicalintervention = $(this).closest('.quirurgicalintervention');
      var updateTypeQuirurgicalIntervention = detailquirurgicalintervention.find('.update-type-quirurgicalintervention');
      if (updateTypeQuirurgicalIntervention.val() === " . json_encode(QuirurgicalIntervention::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_UPDATE) . ") {
        updateTypeQuirurgicalIntervention.val(" . json_encode(QuirurgicalIntervention::UPDATE_TYPE_QUIRURGICAL_INTERVENTION_DELETE) . ");
        detailquirurgicalintervention.hide();
      } else {
        detailquirurgicalintervention.remove();
      }
    });

    $('.delete-button-hospitalization').click(function() {
      var detailhospitalization = $(this).closest('.hospitalization');
      var updateTypeHospitalization = detailhospitalization.find('.update-type-hospitalization');
      if (updateTypeHospitalization.val() === " . json_encode(Hospitalization::UPDATE_TYPE_HOSPITALIZATION_UPDATE) . ") {
        updateTypeHospitalization.val(" . json_encode(Hospitalization::UPDATE_TYPE_HOSPITALIZATION_DELETE) . ");
        detailhospitalization.hide();
      } else {
        detailhospitalization.remove();
      }
    });

   $('.delete-button-system').click(function() {
     var detailsystem = $(this).closest('.system');
     var updateTypeSystem = detailsystem.find('.update-type-system');
     if (updateTypeSystem.val() === " . json_encode(System::UPDATE_TYPE_SYSTEM_UPDATE) . ") {
       updateTypeSystem.val(" . json_encode(System::UPDATE_TYPE_SYSTEM_DELETE) . ");
       detailsystem.hide();
     } else {
     detailsystem.remove();
     }
   });


");
?>
<?= DetailView::widget([
  'model' => $model,
  "options" => ['class' => 'bg-gray-light   table table-striped ', ],
  'attributes' => [
    // 'id',    // 'on_emergency',     // 'emergency_phone',     // 'modified_date',
    'name',
    // 'sex',    // 'birth_date',    // 'birth_place',    'age',
    // 'status',    // 'scholarity',    // 'work_area',    // 'profession',    // 'experience',
    // 'address',    // 'city',    // 'postal_code',    // 'phone',    // 'created_date',
  ],
  ]) ?>

<div class="patient-form">
  <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
      ]); ?>

          <div class="col-md-5" style="display:none">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>

          <?php if(  $model->parental == false ){
              }
              else {echo "<div>
              <h4>Antecedentes Hereditarios y Familiares</h4>
              </div>";
          }?>
                <?php foreach ($modelDetails as $i => $modelDetail) : ?>
                    <div class="row parental parental-<?= $i ?>">
                        <div class="col-md-11">
                            <?= Html::activeHiddenInput($modelDetail, "[$i]id") ?>
                            <?= Html::activeHiddenInput($modelDetail, "[$i]updateType", ['class' => 'update-type']) ?>
                            <?php
                            $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10')
                            ?><table>
                                <tr>
                                  <td class="col-md-3">
                                    <?= $form->field($modelDetail, "[$i]desease_case")->label('Enfermedad')->widget(TypeaheadBasic::classname(), [
                                      'data' => $cie10Data,
                                      'scrollable' => true,
                                      'options' => ['placeholder' => 'Enfermedad'],
                                      'pluginOptions' => ['highlight'=>true],
                                    ]);
                                    ?>
                                  </td>
                                  <td class="col-md-2">
                                    <?= $form->field($modelDetail, "[$i]parental_relation")->label('Parentesco')->textInput(['maxlength' => 255]) ?>
                                  </td>

                                </tr>
                              </table>
                        </div>
                        <div class="col-md-1">
                            <?= Html::button('x', ['class' => 'delete-button btn btn-danger', 'data-target' => "parental-$i"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php if( $model->personal == false ){
                    }
                    else {echo "<div>
                    <h4>Antecedentes Personales Patológicos</h4>
                    </div>";
                }?>
                <?php foreach ($modelPersonals as $j => $modelPersonal) : ?>
                    <div class="row warning personal personal-<?= $j ?>">
                        <div class="col-md-11 warning">
                            <?= Html::activeHiddenInput($modelPersonal, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelPersonal, "[$j]updateTypePersonal", ['class' => 'update-type-personal']) ?>
                            <?php
                            $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10')
                            ?><table>
                                <tr>
                                  <td class="col-md-3 warning">
                                    <?= $form->field($modelPersonal, "[$j]desease")->label('Enfermedad')->widget(TypeaheadBasic::classname(), [
                                      'data' => $cie10Data,
                                      'scrollable' => true,
                                      'options' => ['placeholder' => 'Enfermedad'],
                                      'pluginOptions' => ['highlight'=>true],
                                    ]);
                                    ?>
                                  </td>
                                  <td class="col-md-2 warning">
                                    <?= $form->field($modelPersonal, "[$j]medication")->label('Medicamento')->textInput(['maxlength' => 255]) ?>
                                  </td>

                                </tr>
                              </table>
                        </div>
                        <div class="col-md-1 warning">
                            <?= Html::button('x', ['class' => 'delete-button-personal btn btn-danger', 'data-target' => "personal-$j"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php if( $model->quirurgicalIntervention == false ){
                          }
                          else {echo "<div>
                          <h4>Intervenciones Quirúrgicas</h4>
                          </div>";
                      }?>
                <?php foreach ($modelQuirurgicalInterventions as $j => $modelQuirurgicalIntervention) : ?>
                  <div class="row warning quirurgicalintervention quirurgicalintervention-<?= $j ?>">
                    <div class="col-md-11 warning">
                      <?= Html::activeHiddenInput($modelQuirurgicalIntervention, "[$j]id") ?>
                      <?= Html::activeHiddenInput($modelQuirurgicalIntervention, "[$j]updateTypeQuirurgicalIntervention", ['class' => 'update-type-quirurgicalintervention']) ?>
                      <?php
                      $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10')
                      ?><table>
                        <tr>
                          <td class="col-md-3 warning">
                            <?= $form->field($modelQuirurgicalIntervention, "[$j]cause")->label('Cirugía')->widget(TypeaheadBasic::classname(), [
                              'data' => $cie10Data,
                              'scrollable' => true,
                              'options' => ['placeholder' => 'Causa'],
                              'pluginOptions' => ['highlight'=>true],
                            ]);
                            ?>
                          </td>
                          <td class="col-md-2 warning">
                            <?= $form->field($modelQuirurgicalIntervention, "[$j]at_age")->label('Edad')->textInput(['maxlength' => 255]) ?>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="col-md-1 warning">
                      <?= Html::button('x', ['class' => 'delete-button-quirurgicalintervention btn btn-danger', 'data-target' => "quirurgicalintervention-$j"]) ?>
                    </div>
                  </div>
                <?php endforeach; ?>

                <?php if( $model->hospitalization == false ){
                    }
                    else {echo "<div>
                    <h4>Hospitalizaciones</h4>
                    </div>";
                }?>
                <?php foreach ($modelHospitalizations as $j => $modelHospitalization) : ?>
                  <div class="row warning hospitalization hospitalization-<?= $j ?>">
                    <div class="col-md-11 warning">
                      <?= Html::activeHiddenInput($modelHospitalization, "[$j]id") ?>
                      <?= Html::activeHiddenInput($modelHospitalization, "[$j]updateTypeHospitalization", ['class' => 'update-type-hospitalization']) ?>
                      <?php
                      $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10')
                      ?><table>
                        <tr>
                          <td class="col-md-3 warning">
                            <?= $form->field($modelHospitalization, "[$j]cause")->label('Hospitalización')->widget(TypeaheadBasic::classname(), [
                              'data' => $cie10Data,
                              'scrollable' => true,
                              'options' => ['placeholder' => 'Causa'],
                              'pluginOptions' => ['highlight'=>true],
                            ]);
                            ?>
                          </td>
                          <td class="col-md-2 warning">
                            <?= $form->field($modelHospitalization, "[$j]at_age")->label('Edad')->textInput(['maxlength' => 255]) ?>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div class="col-md-1 warning">
                      <?= Html::button('x', ['class' => 'delete-button-hospitalization btn btn-danger', 'data-target' => "hospitalization-$j"]) ?>
                    </div>
                  </div>
                <?php endforeach; ?>

                <?php if( $model->personalHistory == false ){}
                else {
                  echo "<div>
                  <h4>Enfermedades Padecidas</h4>
                  </div>";
                  }?>
                <?php foreach ($modelPersonalHistorys as $j => $modelPersonalHistory) : ?>
                    <div class="row warning personalhistory personalhistory-<?= $j ?>">
                        <div class="col-md-11 warning">
                            <?= Html::activeHiddenInput($modelPersonalHistory, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelPersonalHistory, "[$j]updateTypePersonalHistory", ['class' => 'update-type-personalhistory']) ?>
                            <?php
                            $cie10Data = ArrayHelper::map(Cie10::find()->all(), 'id10', 'dec10')
                            ?><table>
                                <tr>
                                  <td class="col-md-3 warning">
                                    <?= $form->field($modelPersonalHistory, "[$j]desease_suffered")->label('Enfermedad Padecida')->widget(TypeaheadBasic::classname(), [
                                      'data' => $cie10Data,
                                      'scrollable' => true,
                                      'options' => ['placeholder' => 'Enfermedad Padecida'],
                                      'pluginOptions' => ['highlight'=>true],
                                    ]);
                                    ?>
                                  </td>
                                  <td class="col-md-2 warning">
                                    <?= $form->field($modelPersonalHistory, "[$j]desease_suffered_age")->label('Edad')->textInput(['maxlength' => 255]) ?>
                                  </td>

                                </tr>
                              </table>
                        </div>
                        <div class="col-md-1">
                            <?= Html::button('x', ['class' => 'delete-button-personalhistory btn btn-danger', 'data-target' => "personalhistory-$j"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php if(  $model->system == false ){
                    }
                    else {echo "<div>
                    <h4>Cuestionario por Aparato y Sistemas</h4>
                    </div>";
                }?>
                <?php foreach ($modelSystems as $j => $modelSystem) : ?>
                    <div class="row warning system system-<?= $j ?>">
                        <div class="col-md-11 warning">
                            <?= Html::activeHiddenInput($modelSystem, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelSystem, "[$j]updateTypeSystem", ['class' => 'update-type-system']) ?>
                          <table>
                            <tr>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]double_vision")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]blurred_vision")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]colored_vision")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]view_problem_frequency")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]noise_exposure")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]ear_pain")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]tinnitus")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]difficulty_hearing")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]ear_infection")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]hearing_aid")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]nosebleed")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]dry_nose")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]cant_smell")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]frequent_flu")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                            </tr>
                            <tr>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]allergy")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]persistent_cough")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]phlegm")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]breathless")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]chest_pain")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]chest_pain")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]chest_nuisance")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]high_pressure")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]heartblow")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]varicose")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]swelling")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]pain_urinating")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]urine_count")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]urine_blood")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                            </tr>
                            <tr>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]urine_infection")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]excesive_bleeding")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]bruise")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]anemia")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]acne")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]itch")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]feet_ulceration")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]mole_change")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]hair_loss")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]skin_color_change")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]nail_problems")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                            </tr>
                            <tr>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]gout")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]sciatica")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]arthritis")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]lumbar_desease")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]pain_paralysis")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                              <td class="col-md-2 ">
                              <?= $form->field($modelSystem, "[$j]flatfoot")->widget(SwitchInput::classname(), ['pluginOptions' => ['onText' => 'Si','offText' => 'No','onColor' => 'primary',]]);?>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div class="col-md-1">
                            <?= Html::button('x', ['class' => 'delete-button-system btn btn-danger', 'data-target' => "system-$j"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>


                <div class="form-group col-md-18">

                  <div class="form-group col-md-3">
                    <?= Html::submitButton(' Antecedentes Hereditariso y Familiares', ['name' => 'addRow', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                  </div>
&nbsp;
                  <div class="form-group col-md-3">
                    <?= Html::submitButton(' Antecedentes Patológicos Personales', ['name' => 'addRowPersonal', 'value' => 'true', 'class' => 'btn btn-info secondary']) ?>
                  </div>
                  &nbsp;

                  <div class="form-group col-md-2">
                    <?= Html::submitButton(' Padecimientos Previos', ['name' => 'addRowPersonalHistory', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                  </div>
                  &nbsp;

                  <div class="form-group col-md-3">
                    <?= Html::submitButton(' Historia de Intervenciones Quirúrgicas', ['name' => 'addRowQuirurgicalIntervention', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                  </div>
                </div>
                <div class="form-group col-md-13">
                  &nbsp;
                  <div class="form-group col-md-3">
                    <?= Html::submitButton('<i class="fa fa-hospital-o"></i> Historia de Hospitalizaciones', ['name' => 'addRowHospitalization', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                  </div>


                  </div>
                  <div class="form-group col-md-14">
                    <div class="form-group col-md-3">
                      <?= Html::submitButton('Aparatos y Sistemas', ['name' => 'addRowSystem', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                    </div>

                  </div>
    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
  </div>
