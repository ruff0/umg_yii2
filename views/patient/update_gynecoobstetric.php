<?php
use yii\helpers\Html;
$this->title = 'Update Patient Gyneco-Obstetric: ' . $model->name;
?>
<div class="patient-update-gynecoobstetric">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?= $this->render('_update_form_gynecoobstetric', [
        'model' => $model,
        'modelDetails' => $modelDetails,
        'modelPersonals' => $modelPersonals,
        'modelPersonalHistorys' => $modelPersonalHistorys,
        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
        'modelHospitalizations' => $modelHospitalizations,
        'modelGynecoObstetrics' => $modelGynecoObstetrics,
        'modelDiets' => $modelDiets,
        'modelAddictions' => $modelAddictions,
        'modelSystems' => $modelSystems,
        'modelPhysicals' => $modelPhysicals,
        'modelLabtests' => $modelLabtests,
    ]) ?>
</div>
