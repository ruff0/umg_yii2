<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Patient;

use app\models\Allergic;

use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\db\Expression;

?>
<?php $this->registerJs("
   $('.delete-button-allergic').click(function() {
     var detailallergic = $(this).closest('.allergic');
     var updateTypeAllergic = detailallergic.find('.update-type-allergic');
     if (updateTypeAllergic.val() === " . json_encode(Allergic::UPDATE_TYPE_ALLERGIC_UPDATE) . ") {
       updateTypeAllergic.val(" . json_encode(Allergic::UPDATE_TYPE_ALLERGIC_DELETE) . ");
       detailallergic.hide();
     } else {
     detailallergic.remove();
     }
   });
");
?>
<?= DetailView::widget([
  'model' => $model,
  "options" => ['class' => 'bg-gray-light   table table-striped ', ],
  'attributes' => [
    // 'id',    // 'on_emergency',     // 'emergency_phone',     // 'modified_date',
    'name',
    // 'sex',    // 'birth_date',    // 'birth_place',    'age',
    // 'status',    // 'scholarity',    // 'work_area',    // 'profession',    // 'experience',
    // 'address',    // 'city',    // 'postal_code',    // 'phone',    // 'created_date',
  ],
  ]) ?>

<div class="patient-form-allergic">
  <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
      ]); ?>

          <div class="col-md-5" style="display:none">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>
                <?php
                if ($model->allergic == false) {
                } else {
                    echo "<div><h4>Alergías</h4></div>";
                }?>
                <?php foreach ($modelAllergics as $j => $modelAllergic) : ?>
                    <div class="row warning allergic allergic-<?= $j ?>">
                        <div class="col-md-11 warning">
                            <?= Html::activeHiddenInput($modelAllergic, "[$j]id") ?>
                            <?= Html::activeHiddenInput($modelAllergic, "[$j]updateTypeAllergic", ['class' => 'update-type-allergic']) ?>
                              <table>
                                <tr>
                                  <td class="col-md-2 warning">
                                  <?= $form->field($modelAllergic, "[$j]allergy")->label('Alergia')->textInput(['maxlength' => true]) ?>
                                </td>
                                </tr>
                              </table>
                        </div>
                        <div class="col-md-1 warning">
                            <?= Html::button('x', ['class' => 'delete-button-allergic btn btn-danger', 'data-target' => "allergic-$j"]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="form-group col-md-18">

                    <div class="form-group col-md-3">
                      <?= Html::submitButton('Alérgias', ['name' => 'addRowAllergicAlone', 'value' => 'true', 'class' => 'btn btn-info']) ?>
                    </div>
                  </div>
    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
  </div>
