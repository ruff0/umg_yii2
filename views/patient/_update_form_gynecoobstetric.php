<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\GynecoObstetric;
use yii\helpers\ArrayHelper;
use app\models\Cie10;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use kartik\switchinput\SwitchInput;
// use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\DetailView;
use yii\db\Expression;
?>
<?php Pjax::begin(); ?>
<?php $this->registerJs("
   $('.delete-button-gynecoObstetric').click(function() {
     var detailgynecoObstetric = $(this).closest('.gynecoObstetric');
     var updateTypeGynecoObstetric = detailgynecoObstetric.find('.update-type-gynecoObstetric');
     if (updateTypeGynecoObstetric.val() === " . json_encode(GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_UPDATE) . ") {
       updateTypeGynecoObstetric.val(" . json_encode(GynecoObstetric::UPDATE_TYPE_GYNECO_OBSTETRIC_DELETE) . ");
       detailgynecoObstetric.hide();
     } else {
       detailgynecoObstetric.remove();
     }
   });
");
?>
<?= DetailView::widget([
  'model' => $model,
  "options" => ['class' => 'bg-gray-light   table table-striped ', ],
  'attributes' => [
    // 'id',    // 'on_emergency',     // 'emergency_phone',     // 'modified_date',
    'name',
    // 'sex',    // 'birth_date',    // 'birth_place',    'age',
    // 'status',    // 'scholarity',    // 'work_area',    // 'profession',    // 'experience',
    // 'address',    // 'city',    // 'postal_code',    // 'phone',    // 'created_date',
  ],
  ]) ?>

<div class="patient-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        // 'enableAjaxValidation' => true,
      ]); ?>

          <div class="col-md-5" style="display:none">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
          </div>

                <?php if(  $model->gynecoObstetric == false ){
                    }
                    else {echo "<div>
                      <h4>Antecedentes Gineco-Obstétricos</h4>
                    </div>";
                }?>
                <?php foreach ($modelGynecoObstetrics as $j => $modelGynecoObstetric) : ?>
                  <div class="row warning gynecoObstetric gynecoObstetric-<?= $j ?>">
                    <div class="col-md-12 ">
                        <?= Html::activeHiddenInput($modelGynecoObstetric, "[$j]id") ?>
                        <?= Html::activeHiddenInput($modelGynecoObstetric, "[$j]updateTypeGynecoObstetric", ['class' => 'update-type-gynecoObstetric']) ?>
                          <table>
                            <tr>
                              <td class="col-md-1">
                                  <?= $form->field($modelGynecoObstetric, "[$j]menstruation_age")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-1">
                                  <?= $form->field($modelGynecoObstetric, "[$j]menstruation_frequency")->textInput(['maxlength' => true]) ?>
                              </td>

                              <td class="col-md-2">
                                <?= $form->field($modelGynecoObstetric, "[$j]last_menstruation")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-1">
                                <?= $form->field($modelGynecoObstetric, "[$j]ivsa_age")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-2">
                                  <?= $form->field($modelGynecoObstetric, "[$j]last_pap")->textInput(['maxlength' => true]) ?>
                              </td>
                            </tr>
                            <tr>

                          </table>
                        </div>

                          <div class="col-md-12 ">
                          <table>
                            <tr>
                              <td class="col-md-1 warning">
                                  <?= $form->field($modelGynecoObstetric, "[$j]pregnancy")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-1 warning">
                                  <?= $form->field($modelGynecoObstetric, "[$j]caesarian")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-1 warning">
                                  <?= $form->field($modelGynecoObstetric, "[$j]child_bearing")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-1 warning">
                                  <?= $form->field($modelGynecoObstetric, "[$j]abortion")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-1 warning">
                                  <?= $form->field($modelGynecoObstetric, "[$j]menstruation_pain")->textInput(['maxlength' => true]) ?>
                              </td>
                              <td class="col-md-5 ">
                                  <?= $form->field($modelGynecoObstetric, "[$j]contraceptive")->textInput(['maxlength' => true]) ?>
                              </td>
                            </tr>
                      </table>
                    </div>
                    <div class="col-md-11 warning ">
                      <div class='col-md-2 pull-right'>
                      <?= Html::button('Eliminar los Datos Gineco-Obstetricos', ['class' => 'delete-button-gynecoObstetric btn btn-danger', 'data-target' => "gynecoObstetric-$j"]) ?>
                      </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <div class="form-group col-md-18">

    <div class="form-group col-md-12">
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>
  </div>
