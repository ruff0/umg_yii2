<?php
use yii\helpers\Html;
$this->title = 'Modify Patient: ' . $model->name;
?>
<div class="patient-modify">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?= $this->render('_modify_form', [
        'model' => $model,
        'modelDetails' => $modelDetails,
        'modelPersonals' => $modelPersonals,
        'modelPersonalHistorys' => $modelPersonalHistorys,
        'modelQuirurgicalInterventions' => $modelQuirurgicalInterventions,
        'modelHospitalizations' => $modelHospitalizations,
        'modelGynecoObstetrics' => $modelGynecoObstetrics,
        'modelDiets' => $modelDiets,
        'modelAddictions' => $modelAddictions,
        'modelSystems' => $modelSystems,
        'modelPhysicals' => $modelPhysicals,
        'modelLabtests' => $modelLabtests,
    ]) ?>
</div>
