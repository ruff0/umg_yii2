<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;

$this->title = 'Pacientes';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-12">
        <div class="pull-left"><p>
            <?= Html::a('<i class="fa fa-user-plus"></i> Nuevo Paciente', ['create'], ['class' => 'btn btn-success']) ?>
        </p></div>
    </div>

  <?php $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        [
            'class'=>'kartik\grid\SerialColumn',
            'contentOptions'=>['class'=>'kartik-sheet-style'],
            'width'=>'36px',
            'header'=>'',
            'headerOptions'=>['class'=>'kartik-sheet-style']
        ],
        [
            'class'=>'kartik\grid\ExpandRowColumn',
            'width'=>'50px',
            'value'=>function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail'=>function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_view', ['model'=>$model]);
            },
            'headerOptions'=>['class'=>'kartik-sheet-style'],
            'expandOneOnly'=>true,
            'hiddenFromExport' => false,
        ],
        // 'id',
        // 'created_date',
        // 'modified_date',
        'name',
        // 'sex',
        // 'birth_date',
        // 'birth_place',
        // 'age',
        // 'status',
        // 'scholarity',
        // 'work_area',
        // 'profession',
        // 'experience',
        // 'address',
        // 'city',
        // 'postal_code',
        'phone',
        'on_emergency',
        'emergency_phone',

        ['class' => 'yii\grid\ActionColumn'],
    ]?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'resizableColumns'=>true,
        'pjax'=>true,
        'bordered'=>true,
        'striped'=>true,
        'condensed'=>true,
        'showPageSummary'=>false,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>false,
            ],
        'persistResize'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
              ],
        'responsive'=>true,
        'hover'=>true,
        'toolbar'=> [
              '{export}',
              '{toggleData}',
              ],
        'columns' => $gridColumns,
        'export'=>[
              // 'fontAwesome' => true,
              'PDF' => [
                      'options' => [
                          // 'title' => $tituloexport,
                          //  'subject' => $tituloexport,
                          // 'author' => 'NYCSPrep CIMS',
                          // 'keywords' => 'NYCSPrep, preceptors, pdf'
                      ]
                  ],
              ],
        'exportConfig' => [
              GridView::EXCEL => [
                'label' => 'Guardar en XLS',
                'showHeader' => true,
                // 'filename' => $tituloexport,


              ],
              GridView::PDF => [
                'label' => 'Guardar en PDF',
                'showHeader' => true,
                'showCaption' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                // 'title' => $tituloexport,
                // 'options' => ['title' => $tituloexport],
                // 'config' => ['options' => ['title' => $tituloexport],],
                // 'filename' => $tituloexport,
              ],
        ]

    ]); ?>
    <?php Pjax::end(); ?>
</div>
