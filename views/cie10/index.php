<?php
use miloschuman\highcharts\Highcharts;

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Cie10;
use app\models\Prescription;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\Cie10Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'CIE10';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="cie10-index">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Cie10', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php
    $data = ArrayHelper::map(Prescription::find()->all(), 'id', 'cie10_code');
    ?>
<!-- <?php $datacie= new Cie10();?>
        <?= Highcharts::widget([
          'options' => [
           'title' => ['text' => 'Sample title - pie chart'],
           'plotOptions' => [
               'pie' => [
                   'cursor' => 'pointer',
               ],
           ],
           'series' => [
               [ // new opening bracket
                   'type' => 'pie',
                   'name' => 'Elements',
                   'data' => [
                       ['Firefox', 45.0],
                       ['IE', 26.8],
                       ['Safari', 8.5],
                       ['Opera', 6.2],
                       ['Others', 0.7]
                   ],
               ] // new closing bracket
           ],
       ],
   ]);?> -->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id10',
            'dec10',
            // 'grp10',

            // ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>



    <?php Pjax::end(); ?>
</div>
