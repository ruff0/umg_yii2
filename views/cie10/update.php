<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cie10 */

$this->title = 'Update Cie10: ' . $model->id10;
$this->params['breadcrumbs'][] = ['label' => 'Cie10s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id10, 'url' => ['view', 'id' => $model->id10]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cie10-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
