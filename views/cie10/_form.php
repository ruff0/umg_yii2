<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cie10 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cie10-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dec10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grp10')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
