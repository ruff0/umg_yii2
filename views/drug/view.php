<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Drug */

$this->title = $model->nombre;
// $this->params['breadcrumbs'][] = ['label' => 'Medicamentos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="drug-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          'descripcion',
            // 'id',
            'cbarras',
            // 'nombre',
            'tipo',
            // 'linea',
            'precio',
            'costo',
            'anotaciones',
            // 'activo',
        ],
    ]) ?>
    <p>
      <?= Html::a('<i class="fa fa-pencil"></i> Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('<i class="fa fa-remove"></i> Eliminar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Are you sure you want to delete this medicine?',
          'method' => 'post',
        ],
        ]) ?>
      </p>

</div>
