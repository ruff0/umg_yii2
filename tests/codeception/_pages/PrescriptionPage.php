<?php

namespace tests\codeception\_pages;


use yii\codeception\BasePage;

class PrescriptionPage extends BasePage
{
    public $route = 'prescription/index';
}